![Logo](logo.png)


Unity implementation of Almost Intelligent Machines game

Project in the scope of the "Computer Games Laboratory" at Technical University of Munich, Summer Semester 2019.

https://wiki.tum.de/display/gameslab2019/Almost+Intelligent+Machines 

[![IMAGE ALT TEXT](http://img.youtube.com/vi/nlUHbQ3jJR0/0.jpg)](http://www.youtube.com/watch?v=nlUHbQ3jJR0 "Watch the trailer")

