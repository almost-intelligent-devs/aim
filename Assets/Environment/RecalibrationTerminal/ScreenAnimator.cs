﻿using System.Collections;
using UnityEngine;

namespace AIM
{
    /// <summary>
    /// Used to animate the recalibration terminal screen
    /// via the material's texture offset
    /// </summary>
    public class ScreenAnimator : MonoBehaviour
    {
        private readonly int albedoId = Shader.PropertyToID("_MainTex");

        [SerializeField] private float timePerFrame = 0.15f;

        private Material material;
        private Vector2 offset = new Vector2();

        void Start() { material = GetComponent<MeshRenderer>().material; }

        private void OnEnable() { StartCoroutine(AnimateScreen()); }

        private IEnumerator AnimateScreen()
        {
            int x = 0, y = 0;

            while (true)
            {
                yield return new WaitForSeconds(timePerFrame);
                x = (x + 1) % 4;
                if (x == 0)
                    y = (y + 1) % 4;

                offset.Set(x / 4f, y / 4f);
                material.SetTextureOffset(albedoId, offset);
            }
        }
    }
}

