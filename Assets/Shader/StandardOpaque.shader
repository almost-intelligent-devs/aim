﻿Shader "Highlight/StandardOpaque"
{
	Properties
	{
		_Color("Color", Color) = (1,1,1,1)
		_MainTex("Albedo", 2D) = "white" {}
		[NoScaleOffset][Normal] _Normal("Normal Map", 2D) = "" {}
		//[NoScaleOffset]_MeAoSmo("MetalAOSmooth", 2D) = "grey" {}
		_Metalness("Metalness",  Range(0.0, 1.0)) = 0.5
		_Smoothness("Smoothness",  Range(0.0, 1.0)) = 0.5
		_OccludedAlpha("Occlusion Transparency",  Range(0.0, 1.0)) = 0.2
	}

	CGINCLUDE
	#include "UnityCG.cginc"

	uniform fixed4 _Color;
	uniform float _Metalness;
	uniform float _Smoothness;
	uniform float _OccludedAlpha;
	
	uniform sampler2D _MainTex;
	uniform sampler2D _Normal;
	//uniform sampler2D _MeAoSmo;

	ENDCG

	SubShader
	{
		// pass for solid color when occluded
		Pass
		{
			Name "OCCLUSION"
			Tags{ "Queue" = "Transparent" "RenderType" = "Transparent" }
			Blend SrcAlpha OneMinusSrcAlpha
			ZTest Greater
			ZWrite Off
			Cull Back

			CGPROGRAM

			#pragma vertex vert
			#pragma fragment frag
		
			struct v2f
			{ 
				float4 pos : SV_POSITION;
				float2 uv : TEXCOORD0;
			};

			v2f vert(float4 vertex : POSITION, float2 uv : TEXCOORD0) {
				v2f o;
				o.pos = UnityObjectToClipPos(vertex);
				o.uv = uv;
				return o;
			}

			half4 frag(v2f i) : COLOR{
				fixed4 c = tex2D(_MainTex, i.uv) * _Color;
				c.a =_OccludedAlpha;
				return c;
			}

			ENDCG
		}

		// passthrough to standard shader
		CGPROGRAM
		#pragma surface surf Standard fullforwardshadows
		#pragma target 3.0

		struct Input { float2 uv_MainTex; };

		void surf(Input IN, inout SurfaceOutputStandard o)
		{
			fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
			o.Albedo = c.rgb;
			o.Alpha = c.a;

			o.Normal = UnpackNormal(tex2D(_Normal, IN.uv_MainTex));

			//fixed4 meAoSmo = tex2D(_MeAoSmo, IN.uv_MainTex);
			//o.Metallic = meAoSmo.r;
			//o.Occlusion = meAoSmo.g;
			//o.Smoothness = meAoSmo.a;

			o.Metallic = _Metalness;
			o.Smoothness = _Smoothness;
		}
		ENDCG
	}
	Fallback "Diffuse"
}