﻿Shader "Highlight/StandardOpaqueMAS" // With Metal, AO, Smoothness texture
{
	Properties
	{
		_Color("Color", Color) = (1,1,1,1)
		_MainTex("Albedo", 2D) = "white" {}
		[NoScaleOffset][Normal] _Normal("Normal Map", 2D) = "" {}
		[NoScaleOffset]_MeAoSmo("MetalAOSmooth", 2D) = "grey" {}
		_OccludedAlpha("Occlusion Transparency",  Range(0.0, 1.0)) = 0.2
	}

	CGINCLUDE
	#include "UnityCG.cginc"

	uniform fixed4 _Color;
	uniform float _OccludedAlpha;
	
	uniform sampler2D _MainTex;
	uniform sampler2D _Normal;
	uniform sampler2D _MeAoSmo;

	ENDCG

	SubShader
	{
		// pass for solid color when occluded
		UsePass "Highlight/StandardOpaque/OCCLUSION"

		// passthrough to standard shader
		CGPROGRAM
		#pragma surface surf Standard fullforwardshadows
		#pragma target 3.0

		struct Input { float2 uv_MainTex; };

		void surf(Input IN, inout SurfaceOutputStandard o)
		{
			fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
			o.Albedo = c.rgb;
			o.Alpha = c.a;

			o.Normal = UnpackNormal(tex2D(_Normal, IN.uv_MainTex));

			fixed4 meAoSmo = tex2D(_MeAoSmo, IN.uv_MainTex);
			o.Metallic = meAoSmo.r;
			o.Occlusion = meAoSmo.g;
			o.Smoothness = meAoSmo.a;
		}
		ENDCG
	}
	Fallback "Diffuse"
}