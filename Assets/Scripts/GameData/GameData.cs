﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AIM.Boss;
using System;
using System.Linq;

namespace AIM {

    public class GameData : Singleton<GameData>
    {
        private float startTime = 0;
        public float Duration { get; private set; }
        public int BotsAlive {get; private set; }
        public float RemainingBotHealth { get; private set; }

        private bool gameFinished = false;

        void Start()
        {
            BossController boss = Gamemaster.Instance.GetBoss();
            boss.BossPhaseChanged += OnBossPhaseChanged;
            boss.BossDied += OnBossOrBotsDied;
            Gamemaster.Instance.OnAllBotsDied += OnBossOrBotsDied;
        }

        private void OnBossPhaseChanged(object obj, EventArgs args)
        {
            if(((BossPhaseChangedEventArgs) args).Phase == 1)
            {
                startTime = Time.time;
            }
        }

        private void OnBossOrBotsDied(object obj, EventArgs args)
        {
            if (gameFinished) { return; }

            Duration = Time.time - startTime;
            BotsAlive = Gamemaster.Instance.GetAIMBots().Where(bot => bot.Alive).Count();
            RemainingBotHealth = Gamemaster.Instance.GetAIMBots().Sum(bot => bot.HitPoints);
            Debug.Log(RemainingBotHealth + " - " + BotsAlive + " - " + Duration);

            gameFinished = true;
        }

    }
}