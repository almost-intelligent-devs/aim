﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace AIM
{
    public static class Extensions
    {
        public static void CheckForNullElements<T>(this ICollection<T> collection, string collectionName) where T : class
        {
            foreach (var element in collection)
            {
                if (element == null)
                {
                    throw new InvalidOperationException($"There is an empty element in collection {collectionName}!");
                }
            }
        }

        public static T GetRandomElement<T>(this IList<T> list)
        {
            int index = UnityEngine.Random.Range(0, list.Count);
            return list[index];
        }

        public static List<string> GetConstantsStrings(this Type type)
        {
            System.Reflection.FieldInfo[] fieldInfos = type.GetFields(System.Reflection.BindingFlags.Public |
                 System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.FlattenHierarchy);

            return fieldInfos.Where(fi => fi.FieldType == typeof(string) && fi.IsLiteral && !fi.IsInitOnly).Select(info => (string)info.GetRawConstantValue()).ToList();
        }
    }
}
