﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class LayerDictionary {
    public const string Boss = "Boss";
    public const string InvisibleSurface = "InvisibleSurface";
    public const string NonTargetable = "NonTargetable";

}
