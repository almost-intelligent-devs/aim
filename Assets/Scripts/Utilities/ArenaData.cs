﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ArenaData
{
    public const float GroundHeight = -0.14f;
    public const float WallHeight = 5.5f;
    public const float ArenaRadius = 21f;
    public const float BossHeight = 6f;
    public const float BotHeight = 2.7f;
    public const float MaxBounceHeight = 3f;
    public const float LoSHeight = 1f;
    public const float AgentRadius = 1f;
    public const float DiggingTime = 3f;

    public const float MediumRagdollForce = 50f;
    public const float HardRagDollForce = MediumRagdollForce * 2f;
    public const float MegaRagdollForce = MediumRagdollForce * 5f;
}
