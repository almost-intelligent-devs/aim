﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AIM
{
    public static class Scenes
    {
        public const string Game = "Game";
        public const string Tutorial = "Tutorial";
    }
}

