﻿using System.Collections.Generic;
using UnityEngine;

namespace AIM.Animation
{
    public abstract class SmartParameter
    {
        protected readonly MonoBehaviour owner;
        protected readonly Animator animator;
        protected readonly int nameHash;
        protected readonly int tagHash;
        protected readonly int layerIndex;

        /// <param name="owner">MonoBehaviour that manages the smart parameter</param>
        /// <param name="animator">Animator which holds the parameter</param>
        /// <param name="nameHash">Hash for the parameter's name (use <see cref="Animator.StringToHash(string)"/>)</param>
        /// <param name="tagHash">Hash for the tag name of the state that this parameter controls (use <see cref="Animator.StringToHash(string)"/>)</param>
        public SmartParameter(MonoBehaviour owner, Animator animator, int nameHash, int tagHash, int layerIndex)
        {
            this.owner = owner;
            this.animator = animator;
            this.nameHash = nameHash;
            this.tagHash = tagHash;
            this.layerIndex = layerIndex;
        }

        public bool IsControlledStateActive { get => animator.IsCurrentStateTagHash(tagHash, layerIndex); }
    }
}
