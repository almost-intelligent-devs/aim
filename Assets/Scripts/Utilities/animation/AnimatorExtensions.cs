﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class AnimatorExtensions
{
    public static bool IsCurrentStateTagHash(this Animator animator, int tagHash, int layerIndex = 0)
        => animator.GetCurrentAnimatorStateInfo(layerIndex).tagHash == tagHash;

    public static bool IsNextStateTagHash(this Animator animator, int tagHash, int layerIndex = 0)
        => animator.GetNextAnimatorStateInfo(layerIndex).tagHash == tagHash;
}
