﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace AIM.Animation
{
    public class SmartTrigger : SmartParameter
    {
        #region Events
        public event EventHandler OnReset;
        #endregion

        public bool BlockUntilFinished { get; set; } = false;
        private bool isFinished = true;
        private CoroutineCancellationToken waitUntilFinishedToken;

        public List<int> AnyStateExceptions { get; set; }

        /// <param name="owner">MonoBehaviour that manages the smart trigger</param>
        /// <param name="animator">Animator which holds the trigger parameter</param>
        /// <param name="nameHash">Hash for the trigger's name (use <see cref="Animator.StringToHash(string)"/>)</param>
        /// <param name="tagHash">Hash for the tag name of the states that this trigger controls (use <see cref="Animator.StringToHash(string)"/>)</param>
        /// <param name="anyStateExceptions">Tag hash of animator states this trigger cannot transition from (use <see cref="Animator.StringToHash(string)"/>)</param>
        public SmartTrigger(MonoBehaviour owner, Animator animator, int nameHash, int tagHash, int layerIndex = 0) 
            : base(owner, animator, nameHash, tagHash, layerIndex) {}

        /// <summary>
        /// Sets the value of the given trigger parameter. If BlockUntilFinished is true, 
        /// the trigger is only set if the respective animation has finished yet.
        /// </summary>
        /// <returns>Indicates whether the trigger has been set</returns>
        public bool Trigger()
        {
            if (AnyStateExceptions?.Any(tagHash => animator.IsCurrentStateTagHash(tagHash) || animator.IsNextStateTagHash(tagHash)) ?? false)
            {
                return false;
            }

            if (BlockUntilFinished)
            {
                if (isFinished)
                {
                    waitUntilFinishedToken?.Cancel();
                    waitUntilFinishedToken = new CoroutineCancellationToken();

                    animator.SetTrigger(nameHash);
                    owner.StartCoroutine(WaitUntilFinished(waitUntilFinishedToken));
                    return true;
                }
            }
            else
            {
                animator.SetTrigger(nameHash);
                return true;
            }

            return false;
        }
        /// <summary> Resets the value of the given trigger parameter. </summary>
        public void Reset()
        {
            waitUntilFinishedToken?.Cancel();
            waitUntilFinishedToken = null;
            animator.ResetTrigger(nameHash);
            OnReset?.Invoke(this, EventArgs.Empty);
        }

        private IEnumerator WaitUntilFinished(CoroutineCancellationToken ct)
        {
            isFinished = false;
            yield return new WaitUntil(() => animator.GetCurrentAnimatorStateInfo(layerIndex).tagHash == tagHash || ct.CancellationRequested);
            yield return new WaitWhile(() => animator.GetCurrentAnimatorStateInfo(layerIndex).tagHash == tagHash && !ct.CancellationRequested);
            isFinished = true;
            waitUntilFinishedToken = null;
        }
    }
}
