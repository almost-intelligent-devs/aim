﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class TagDictionary
{
    public const string Bot = "Bot";
    public const string Interactable = "Interactable";
    public const string DiggingSpot = "DiggingSpot";
    public const string Boss = "Boss";
    public const string BossWeakPoint = "BossWeakPoint";
    public const string BossJumpTrigger = "BossJumpTrigger";
    public const string BotWeapon = "BotWeapon";
}
