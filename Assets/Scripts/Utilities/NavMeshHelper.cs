﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public static class NavMeshHelper
{
    public static Vector3 RandomPositionOnNavMesh()
    {
        NavMeshHit hit;
        Vector3 currentPosition;
        float x, z;

        int count = 0;

        do
        {
            x = Random.Range(-20.0f, 20.0f);
            z = Random.Range(-15.0f, 15.0f);
            currentPosition = new Vector3(x, 0, z);
            count++;
        }
        while (!NavMesh.SamplePosition(currentPosition, out hit, 0.5f, NavMesh.AllAreas) && count <= 1000);

        return currentPosition;
    }
}
