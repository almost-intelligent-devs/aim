﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AIM
{
    public static class Geometry
    {
        public static bool LinePlaneIntersection(out Vector3 intersection, Vector3 linePoint, Vector3 lineVec, Vector3 planeNormal, Vector3 planePoint)
        {

            float dotNumerator = Vector3.Dot(planePoint - linePoint, planeNormal);
            float dotDenominator = Vector3.Dot(lineVec, planeNormal);

            if (dotDenominator != 0.0f)
            {

                intersection = linePoint + lineVec.normalized * (dotNumerator / dotDenominator);
                return true;
            }
            else
            {

                intersection = Vector3.zero;
                return false;
            }
        }
        public static float ClampAngle(float ang, float min, float max)
        {

            if (ang > 180) ang -= 360;
            if (max > 180) max -= 360;
            if (min > 180) min -= 360;

            ang = Mathf.Clamp(ang, min, max);

            return ang < 0 ? ang + 360 : ang;
        }
    }
}
