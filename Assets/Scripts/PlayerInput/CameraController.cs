﻿using System;
using System.Collections;
using UnityEngine;

namespace AIM.PlayerInput
{
    /// <summary>
    /// Simple camera controller that follows the currently selected bot and zooms out when the player is in selection mode
    /// </summary>
    [RequireComponent(typeof(Camera))]
    public class CameraController : MonoBehaviour
    {
        [SerializeField] private float currentDistance = 20f;
        [SerializeField] private float currentTilt = 80f;
        [SerializeField] private float smoothTime = .5f;
        [SerializeField] private InputHandler input;

        [Header("Control Mode")]
        [SerializeField] private float controlModeDistance = 15f;
        [SerializeField] private float controlModeTilt = 45f;

        [Header("Selection Mode")]
        [SerializeField] private float selectionModeDistance = 20f;
        [SerializeField] private float selectionModeTilt = 80f;

        new private Camera camera;
        private Transform transformToFollow;

        private Vector3 positionVelocity = Vector3.zero;
        private Vector3 rotationVelocity = Vector3.zero;

        private CoroutineCancellationToken cancelRotationToken;

        /// <summary> If set, camera matches position of the transform (Note: overrides <see cref="transformToFollow"/>) </summary>
        public Transform TransformToMatch { get; set; }
        public bool MatchRotation { get; set; }

        private void Awake()
        {
            camera = GetComponent<Camera>();
            if (camera == null)
            {
                throw new InvalidOperationException($"{nameof(camera)} is not specified!");
            }
            if (input == null)
            {
                throw new InvalidOperationException($"{nameof(input)} is not specified!");
            }

            Gamemaster.Instance.Register(this);
        }

        private void Start()
        {
            currentDistance = selectionModeDistance;
            currentTilt = selectionModeTilt;
        }

        private void OnEnable()
        {
            input.ControlModeActivated += OnBotControlled;
            input.SelectionModeActivated += OnSelectModeActivated;
            input.SelectionChanged += OnSelectionChanged;
        }

        private void OnDisable()
        {
            input.ControlModeActivated -= OnBotControlled;
            input.SelectionModeActivated -= OnSelectModeActivated;
        }

        private void Update()
        {
            if (TransformToMatch)
            {
                SmoothDampToPosition(TransformToMatch.position);
                if (MatchRotation)
                {
                    SmoothDampToRotation(TransformToMatch.rotation);
                }
                else
                {
                    SmoothDampToRotation(0f);
                }
            }
            else if (transformToFollow != null)
            {
                var dir = transform.forward;
                dir.y = 0f;
                var rot = Quaternion.Euler(currentTilt, 0f, 0f);
                dir = rot * dir.normalized;
                SmoothDampToPosition(transformToFollow.position - dir * currentDistance);
                SmoothDampToRotation(currentTilt, true);
            }
        }

        private void SmoothDampToPosition(Vector3 position)
        {
            transform.position = Vector3.SmoothDamp(transform.position, position, ref positionVelocity, smoothTime);
        }


        private void SmoothDampToRotation(float tiltAngle, bool resetYZ = false)
        {
            Vector3 rotation = transform.localEulerAngles;
            rotation.x = tiltAngle;
            if (resetYZ)
            {
                rotation.y = rotation.z = 0f;
            }
            transform.localRotation = Quaternion.Euler(Vector3.SmoothDamp(transform.localEulerAngles, rotation, ref rotationVelocity, smoothTime));
        }

        private void SmoothDampToRotation(Quaternion rotation)
        {
            transform.localRotation = Quaternion.Euler(Vector3.SmoothDamp(transform.localEulerAngles, rotation.eulerAngles, ref rotationVelocity, smoothTime));
        }

        private void OnSelectModeActivated(object sender, EventArgs args)
        {
            transformToFollow = ((AIMBotEventArgs)args).Bot.transform;
            currentDistance = selectionModeDistance;
            currentTilt = selectionModeTilt;
        }

        private void OnSelectionChanged(object sender, EventArgs args)
        {
            transformToFollow = ((AIMBotEventArgs)args).Bot.transform;
        }

        private void OnBotControlled(object sender, EventArgs args)
        {
            transformToFollow = ((AIMBotEventArgs)args).Bot.transform;
            currentDistance = controlModeDistance;
            currentTilt = controlModeTilt;
        }
    }
}
