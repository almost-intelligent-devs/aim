﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using AIM.Bots;
using UnityEngine;
using AIM.Environment;
using AIM.Sound;

namespace AIM.PlayerInput
{
    /// <summary>
    /// Keyboard + Mouse / Xbox input for the player input.
    /// </summary>
    [RequireComponent(typeof(MouseAndKeyboardInput), typeof(XboxInput))]
    public class InputHandler : MonoBehaviour
    {
        /// <summary>
        /// Triggered when the direct control over the bot is assumed. Passes <see cref="AIMBotEventArgs"/> as EventArgs.
        /// </summary>
        public event EventHandler ControlModeActivated;
        /// <summary>
        /// Triggered when the bot Selection mode is activated. Has empty EventArgs.
        /// </summary>
        public event EventHandler SelectionModeActivated;
        /// <summary>
        /// Triggered when the current selection in Selection mode changed
        /// </summary>
        public event EventHandler SelectionChanged;

        /// <summary>
        /// Bot that is directly controlled at the moment. <see langword="null"/> when in Selection mode.
        /// </summary>
        [SerializeField]
        private AIMBotController currentBot;
        /// <summary>
        /// Bot that is currently selected at the moment. <see langword="null"/> when in Direct control mode.
        /// </summary>
        private AIMBotController selectedBot;
        /// <summary>
        /// Stores reference to Camera.main
        /// </summary>
        new private Camera camera;

        /// <summary>
        /// The length of the pause between jumping to another bot in selection mode.
        /// Without this pause we jump to the last bot available in direction of input
        /// </summary>
        private const float SelectionChangeCooldown = .5f;
        /// <summary>
        /// Timer used for the pause between selection jumps in selection mode
        /// </summary>
        private float timeSinceLastSelection = 0f;
        /// <summary>
        /// The first game object from the viewport during the previous frame.
        /// Only set when holding down the 'dig' button. Used for determining, whether said object
        /// is dug upon for a longer period.
        /// </summary>
        private GameObject dugAtPrevFrame;
        private MouseAndKeyboardInput keyboardMouseInput;
        private XboxInput xboxInput;
        private InputMethod currentInput;

        private AudioListener audioListener;

        private void Awake()
        {
            camera = Camera.main;

            keyboardMouseInput = this.GetComponent<MouseAndKeyboardInput>();
            xboxInput = this.GetComponent<XboxInput>();

            Destroy(camera.GetComponent<AudioListener>());
            var listener = new GameObject("AudioListener");
            audioListener = listener.AddComponent<AudioListener>();

            Gamemaster.Instance.Register(this);
        }

        private void Start()
        {
            SelectionChanged += OnSelectionChanged;
            if (Input.GetJoystickNames().Count() > 0 && Input.GetJoystickNames()[0] != "")
            {
                currentInput = xboxInput;
            }
            else
            {
                currentInput = keyboardMouseInput;
            }
        }


        private void Update()
        {
            if (currentBot != null)
            {
                ResolveControlBot();
            }
            else
            {
                ResolveChoosingBot();
            }
        }

        /// <summary>
        /// Control logic for Direct control mode
        /// </summary>
        private void ResolveControlBot()
        {

            //Switch to selection mode when corresponding key is pressed
            if (currentInput.GoToSelectModePressed())
            {
                GoToSelectMode();
                return;
            }

            //Orient bot towards the mouse
            RotateBot();
            //Move bot accordingly to the player's input
            Vector3 movementDirection = currentInput.GetDirection();
            if (currentInput.SlowdownPressed())
            {
                currentBot.WalkTowards(movementDirection);
            }
            else
            {
                currentBot.RunTowards(movementDirection);
            }

            if (currentInput.DashPressed())
            {
                currentBot.PerformDash();
            }
            if (currentInput.AttackPressed())
            {
                currentBot.UseHeldItem();
            }
            // if (Input.GetButton(InputDictionary.Attack))
            // {
            //     currentBot.UseHeldItemWithKeyHeldDown();
            // }
            if (currentInput.ThrowPressed())
            {
                currentBot.ThrowItem();
            }
            if (currentInput.DigPressed())
            {
                currentBot.TryToDig();
            }
        }

        /// <summary>
        /// Control logic for Selection mode.
        /// </summary>
        private void ResolveChoosingBot()
        {
            //Get all the bots that are still alive
            var bots = Gamemaster.Instance.GetAIMBots().Where(bot => bot.Alive);
            if (!bots.Any())
            {
                //All bots are dead! Nothing to do here
                return;
            }
            //Select the first bot available if no bot is selected or previously selected is no longer available
            if (selectedBot == null || !bots.Contains(selectedBot))
            {
                selectedBot = bots.FirstOrDefault();
                SelectionChanged?.Invoke(this, new AIMBotEventArgs(selectedBot));
            }
            //Switch to the direct control when the corresponding key is pressed
            if (currentInput.ConfirmSelectionPressed())
            {
                if (selectedBot.ControlBlocked)
                {
                    selectedBot.Sounds.PlayOrQueue(selectedBot.SoundsProvider.GetClip(BotSounds.BotBlocked), 0);
                    return;
                }
                currentBot = selectedBot;
                selectedBot = null;
                timeSinceLastSelection = 0f;
                ControlModeActivated?.Invoke(this, new AIMBotEventArgs(currentBot));
                currentBot.AssumeDirectControl();
                return;
            }
            //Timeout between jumps
            if (!Mathf.Approximately(timeSinceLastSelection, 0))
            {
                timeSinceLastSelection -= Time.deltaTime;
                if (timeSinceLastSelection < 0f)
                {
                    timeSinceLastSelection = 0f;
                }
                else
                {
                    return;
                }
            }

            var choosingDirection = currentInput.GetDirection();
            if (choosingDirection != Vector3.zero && bots.Count() > 1)
            {
                //Iterate through available bots to find the "closest" bot in the direction of the input
                float minValue = float.MaxValue;
                AIMBotController closestBot = null;
                foreach (var bot in bots)
                {
                    //skip the currently selected bot
                    if (bot == selectedBot)
                    {
                        continue;
                    }
                    //We look in 2 angles: if the bot is close, we search in a wider cone, if the bot is far, we look in
                    //a more narrow cone. Then for all "visible" bots inside those cones we just calculate the distance
                    //and pick the one that is closest to our current bot
                    var angle = Vector3.Angle(choosingDirection, (bot.transform.position - selectedBot.transform.position));
                    const float AngleOfSearch = 90f;
                    const float ProximityAllowance = 3f;
                    const float ProximityMultiplier = 1.5f;
                    var distance = Vector3.Distance(bot.transform.position, selectedBot.transform.position);
                    if (angle < AngleOfSearch / 2f || (distance < ProximityAllowance && angle < AngleOfSearch / 2f * ProximityMultiplier))
                    {
                        //We look for the distance from the bot to potential bot
                        if (distance < minValue)
                        {
                            closestBot = bot;
                            minValue = distance;
                        }
                    }
                }
                //If bot found, change the selection
                if (closestBot != null)
                {
                    selectedBot = closestBot;
                    timeSinceLastSelection = SelectionChangeCooldown;
                    SelectionChanged?.Invoke(this, new AIMBotEventArgs(selectedBot));
                    selectedBot.Sounds.PlayOrQueue(selectedBot.SoundsProvider.GetClip(Sound.BotSounds.BotSelected), 0);
                }
            }
        }

        public void GoToSelectMode()
        {
            selectedBot = currentBot;
            currentBot.ReleaseDirectControl();
            currentBot = null;
            SelectionModeActivated?.Invoke(this, new AIMBotEventArgs(selectedBot));
        }

        /// <summary>
        /// Orients <see cref="currentBot"/> towards the mouse position
        /// </summary>
        private void RotateBot()
        {
            float angle = currentInput.GetRotationAngle(currentBot);
            currentBot.RotateAroundY(angle);
        }

        private void OnSelectionChanged(object sender, EventArgs e)
        {
            var args = (AIMBotEventArgs)e;
            var bot = args.Bot;
            audioListener.transform.SetParent(bot.transform, false);
        }
    }
}
