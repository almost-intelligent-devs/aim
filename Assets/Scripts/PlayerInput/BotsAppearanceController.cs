﻿using System;
using UnityEngine;
using AIM.Bots;

namespace AIM.PlayerInput
{
    public class BotsAppearanceController : MonoBehaviour
    {
        [SerializeField]
        private Color selectedColor = Color.blue;
        [SerializeField]
        private Color controlledColor = Color.yellow;
        [SerializeField]
        private Color notControllable = Color.red;
        [SerializeField]
        private InputHandler input;

        private PlaceholderBotAppearance currentTarget;

        private void Awake()
        {
            if (input == null)
            {
                throw new InvalidOperationException($"{nameof(input)} is not specified!");
            }
        }

        private void OnEnable()
        {
            input.SelectionChanged += OnSelectionChanged;
            input.ControlModeActivated += OnControlModeActivated;
            input.SelectionModeActivated += OnSelectionModeActivated;
        }

        private void OnDisable()
        {
            input.SelectionChanged -= OnSelectionChanged;
            input.ControlModeActivated -= OnControlModeActivated;
            input.SelectionModeActivated -= OnSelectionModeActivated;
        }

        private void OnSelectionModeActivated(object sender, EventArgs e)
        {
            currentState = CurrentState.Selection;
        }

        private void OnControlModeActivated(object sender, EventArgs e)
        {
            var controlled = ((AIMBotEventArgs)e).Bot.GetComponent<PlaceholderBotAppearance>();
            if (controlled != currentTarget)
            {
                currentTarget?.SetIndicatorEnabled(false);
                currentTarget = controlled;
            }
            currentTarget?.SetIndicatorEnabled(true);
            currentTarget?.SetIndicatorColor(controlledColor);
            currentState = CurrentState.Controlling;
        }

        private void OnSelectionChanged(object sender, EventArgs e)
        {
            currentTarget?.SetIndicatorEnabled(false);
            currentTarget = ((AIMBotEventArgs)e).Bot.GetComponent<PlaceholderBotAppearance>();
            currentTarget?.SetIndicatorEnabled(true);
        }

        private void Update()
        {
            if (currentState == CurrentState.Selection)
            {
                currentTarget?.SetIndicatorColor(currentTarget.CanBeControlled() ? selectedColor : notControllable);
            }
        }

        private CurrentState currentState = CurrentState.Selection;

        private enum CurrentState
        {
            Selection,
            Controlling
        }
    }
}
