﻿using System;
using System.Collections;
using System.Collections.Generic;
using AIM.Bots;
using UnityEngine;

namespace AIM.PlayerInput
{
    public class AIMBotEventArgs : EventArgs
    {
        public AIMBotController Bot { get; private set; }
        public AIMBotEventArgs(AIMBotController bot)
        {
            Bot = bot;
        }
    }
}
