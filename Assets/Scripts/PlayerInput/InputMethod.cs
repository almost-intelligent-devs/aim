﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AIM.Bots;

/// <summary>
/// Determines if button for specific method was pressed or not
/// </summary>
namespace AIM.PlayerInput
{
    public abstract class InputMethod : MonoBehaviour
    {
        public abstract bool GoToSelectModePressed();
        public abstract bool SlowdownPressed();
        public abstract bool DashPressed();
        public abstract bool AttackPressed();
        public abstract bool ThrowPressed();
        public abstract bool DigPressed();
        public abstract bool ConfirmSelectionPressed();
        public abstract Vector3 GetDirection();
        public abstract float GetRotationAngle(AIMBotController bot);
    }
}