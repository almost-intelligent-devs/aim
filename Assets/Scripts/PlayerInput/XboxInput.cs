﻿using AIM.Bots;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
namespace AIM.PlayerInput
{
    /// <summary>
    /// Current Input Scheme:
    /// A to select bot and perform dash
    /// B to deselect bot
    /// X to Attack
    /// Right Bumper to Dig
    /// Left Bumper to Throw Item
    /// Left Stick to Move
    /// Right Stick to Rotate
    /// Left/Right Trigger to fine adjust rotation
    /// </summary>
    public class XboxInput : InputMethod
    {
        [SerializeField]
        private float rotationSpeed;
        [SerializeField]
        private float RightStickThresholdPercentage = 0.3f;
        [SerializeField]
        private float LeftStickThresholdPercentage = 0.7f;

        private const float maxAxisValue = 0.07537688f;

        private bool speedLinkController = false;
        private void Start()
        {
            if (Input.GetJoystickNames().Count() > 0 && Input.GetJoystickNames()[0] == "SPEEDLINK TORID")
            {
                speedLinkController = true;
                SetSpeedLinkMapping();
            }
        }
        private void SetSpeedLinkMapping()
        {
            InputDictionary.XboxRightJoystickHorizontal = "XboxRightJoystickHorizontalMAC";
            InputDictionary.XboxRightJoystickVertical = "XboxRightJoystickVerticalMAC";
            InputDictionary.XboxAButton = KeyCode.Joystick1Button2;
            InputDictionary.XboxBButton = KeyCode.Joystick1Button1;
            InputDictionary.XboxXButton = KeyCode.Joystick1Button3;
            InputDictionary.XboxLeftBumper = KeyCode.Joystick1Button4;
            InputDictionary.XboxRightBumper = KeyCode.Joystick1Button5;
        }

        public override bool AttackPressed()
        {
            if (speedLinkController)
                return Input.GetKey(InputDictionary.XboxSpeedLinkRightTrigger);

            return Input.GetAxis(InputDictionary.XboxRightTrigger) > 0;
        }

        public override bool ConfirmSelectionPressed()
        {
            return Input.GetKeyDown(InputDictionary.XboxAButton);
        }

        public override bool DashPressed()
        {
            return Input.GetKeyDown(InputDictionary.XboxAButton);
        }

        public override bool DigPressed()
        {
            return Input.GetKey(InputDictionary.XboxXButton);
        }

        public override Vector3 GetDirection()
        {
            return new Vector3(Input.GetAxis(InputDictionary.XboxLeftJoystickHorizontal), 0f, -Input.GetAxis(InputDictionary.XboxLeftJoystickVertical)).normalized;
        }

        public override float GetRotationAngle(AIMBotController bot)
        {
            float horizontal = Input.GetAxis(InputDictionary.XboxRightJoystickHorizontal);
            float vertical = -Input.GetAxis(InputDictionary.XboxRightJoystickVertical);

            if (StickInThreshold(horizontal, vertical, RightStickThresholdPercentage))
            {
                return Mathf.Atan2(vertical, horizontal) * Mathf.Rad2Deg - 90f;
            }
            else
            {
                float currentAngle = -bot.transform.rotation.eulerAngles.y;
                float rotationDirection = 0;
                if (Input.GetKey(InputDictionary.XboxLeftBumper)) { rotationDirection += 1; }
                if (Input.GetKey(InputDictionary.XboxRightBumper)) { rotationDirection -= 1; }
                return currentAngle + rotationDirection * Time.deltaTime * rotationSpeed;
            }
        }

        public override bool GoToSelectModePressed()
        {
            return Input.GetKeyDown(InputDictionary.XboxBButton);
        }

        public override bool SlowdownPressed()
        {
            return !StickInThreshold(Input.GetAxis(InputDictionary.XboxLeftJoystickHorizontal), Input.GetAxis(InputDictionary.XboxLeftJoystickVertical), LeftStickThresholdPercentage);
        }

        public override bool ThrowPressed()
        {
            if (speedLinkController)
                return Input.GetKeyDown(InputDictionary.XboxSpeedLinkLeftTrigger);

            return Input.GetAxis(InputDictionary.XboxLeftTrigger) > 0;
        }

        private bool StickInThreshold(float horizontal, float vertical, float threshold)
        {
            return Mathf.Max(Mathf.Abs(horizontal) / maxAxisValue, Mathf.Abs(vertical) / maxAxisValue) > threshold;
        }
    }
}
