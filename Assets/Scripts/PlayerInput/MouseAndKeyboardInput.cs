﻿using AIM.Bots;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AIM.PlayerInput
{
    public class MouseAndKeyboardInput : InputMethod
    {
        private const KeyCode GoToSelectModeKey = KeyCode.Tab;
        private const KeyCode ConfirmSelectionKey = KeyCode.Space;

        public override bool AttackPressed()
        {
            return Input.GetButton(InputDictionary.Attack);
        }

        public override bool ConfirmSelectionPressed()
        {
            return Input.GetKeyDown(ConfirmSelectionKey);
        }

        public override bool DashPressed()
        {
            return Input.GetButtonDown(InputDictionary.Jump);
        }

        public override bool DigPressed()
        {
            return Input.GetButton(InputDictionary.Dig);
        }

        public override Vector3 GetDirection()
        {
            return new Vector3(Input.GetAxis(InputDictionary.Horizontal), 0f, Input.GetAxis(InputDictionary.Vertical)).normalized;
        }

        public override float GetRotationAngle(AIMBotController bot)
        {
            var oldRotation = bot.transform.rotation;
            if (RotateBotToMouse(bot))
            {
                var resultAngle = bot.transform.rotation.eulerAngles.y;
                bot.transform.rotation = oldRotation;
                return -resultAngle;
            }
            else
            {
                Vector3 playerPos = Camera.main.WorldToScreenPoint(bot.transform.position + bot.transform.forward * 0.01f);
                var dir = Input.mousePosition - playerPos;
                return Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg - 90f;
            }
        }

        Vector3 GunRelativePos = new Vector3(0.2f, 1.4f, 1.1f);
        Vector3 GunRelativeDir = new Vector3(-0.2f, 0.1f, 1.0f);

        private bool RotateBotToMouse(AIMBotController controller)
        {
            Transform s = controller.transform;

            float distance = float.PositiveInfinity;
            float minDistance = 0.01f;
            int iteration = 30;

            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, float.MaxValue, 1 << LayerMask.NameToLayer(LayerDictionary.InvisibleSurface)))
            {
                var bulletSpawn = s.TransformPoint(GunRelativePos);

                var relativeHit = s.InverseTransformPoint(hit.point);
                if(relativeHit.sqrMagnitude < 2f * GunRelativePos.sqrMagnitude)
                {
                    relativeHit = GunRelativePos;
                    relativeHit.x = 0f;
                    relativeHit.z = 0f;
                    bulletSpawn = s.TransformPoint(relativeHit);
                }

                var botPos = s.position;

                var diff = bulletSpawn - botPos;
                Vector3 targetPos = hit.point;
                targetPos.y = bulletSpawn.y;
                while (distance > minDistance && iteration > 0)
                {
                    Vector3 aimOrigin = bulletSpawn;
                    Vector3 origin;
                    var weaponForward = s.TransformDirection(GunRelativeDir);
                    weaponForward.Normalize();
                    var sForw = s.forward;
                    sForw.y = 0f;
                    sForw.Normalize();
                    var sPos = s.position;
                    sPos.y = 0f;
                    if (!Geometry.LinePlaneIntersection(out origin, aimOrigin, -weaponForward, s.forward, s.position))
                    {

                        //if for some readon the weapon cannot intersect with spine forward plane, just use the aim origin
                        //hopefully this never happens as weapon should always be in front of spine
                        origin = aimOrigin;
                    }
                    origin.y = 0f;
                    Vector3 aimForward = targetPos - origin;
                    Quaternion rot = Quaternion.FromToRotation(weaponForward, aimForward);
                    distance = rot.eulerAngles.sqrMagnitude;
                    iteration--;
                    s.rotation = rot * s.rotation;
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        public override bool GoToSelectModePressed()
        {
            return Input.GetKeyDown(GoToSelectModeKey);
        }

        public override bool SlowdownPressed()
        {
            return Input.GetButton(InputDictionary.Slowdown);
        }

        public override bool ThrowPressed()
        {
            return Input.GetButtonDown(InputDictionary.Throw);
        }
    }
}
