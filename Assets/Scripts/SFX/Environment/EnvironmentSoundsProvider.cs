﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AIM.Sound
{
    public class EnvironmentSoundsProvider : SoundsProvider
    {
        protected override void CheckAudioClips() => CheckAudioClips(typeof(EnvironmentSounds));
    }
}
