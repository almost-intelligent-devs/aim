﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AIM.Sound
{
    public static class EnvironmentSounds
    {
        public const string RigidBodyCollided = "rigidbodyContact";
        public const string ButtonActivated = "buttonActivated";
        public const string ButtonDeactivated = "buttonDeactivated";
        public const string DoorOpened = "doorOpened";
        public const string DoorClosed = "doorClosed";
        public const string RecalibrationStarted = "recalibrationStarted";
        public const string RecalibrationComplete = "recalibrationComplete";
    }
}
