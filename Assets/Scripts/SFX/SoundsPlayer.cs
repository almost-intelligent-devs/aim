﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AIM.Sound
{
    public class SoundsPlayer : MonoBehaviour
    {
        [SerializeField] private List<AudioSource> audioSources;
        [SerializeField] private bool mute = false;
        private List<AudioClip> clipsQueue;

        private void Awake()
        {
            if (audioSources == null || audioSources.Count < 1)
            {
                throw new InvalidOperationException($"{nameof(audioSources)} collection is not specified!");
            }
            audioSources.CheckForNullElements(nameof(audioSources));
            clipsQueue = new List<AudioClip>(audioSources.Count);
            foreach (var track in audioSources)
            {
                clipsQueue.Add(null);
            }
        }

        /// <summary>
        /// Plays specified clip on the specified track with optional looping.
        /// If the same clip is already playing with the same looping flag, the call
        /// is ignored.
        /// </summary>
        /// <param name="clip"><see cref="AuidoClip"/> to play</param>
        /// <param name="loop"><see langword="true"/> if the playback should loop</param>
        /// <param name="track">index of the track to use</param>
        public void PlaySound(AudioClip clip, bool loop, int track = 0)
        {
            if (mute) { return; }
            TrackIndexCheck(track);
            if (audioSources[track].clip == clip && audioSources[track].isPlaying && audioSources[track].loop == loop)
            {
                //already playing what we want and how we want, return
                return;
            }
            audioSources[track].Stop();
            audioSources[track].clip = clip;
            audioSources[track].loop = loop;
            audioSources[track].Play();
        }
        /// <summary>
        /// Stop the playback of sounds on the specified track
        /// </summary>
        /// <param name="track">index of the track</param>
        public void StopTrack(int track)
        {
            if (mute) { return; }
            TrackIndexCheck(track);
            audioSources[track].Stop();
        }

        /// <summary>
        /// Plays specified clip on the specified track with optional looping.
        /// If the same clip is already playing with the same looping flag,
        /// the playback will start form 0s.
        /// </summary>
        /// <param name="clip"><see cref="AuidoClip"/> to play</param>
        /// <param name="loop"><see langword="true"/> if the playback should loop</param>
        /// <param name="track">index of the track to use</param>
        public void PlaySoundWithStartingOver(AudioClip clip, bool loop, int track = 0)
        {
            if (mute) { return; }
            TrackIndexCheck(track);
            audioSources[track].Stop();
            audioSources[track].clip = clip;
            audioSources[track].loop = loop;
            audioSources[track].Play();
        }

        /// <summary>
        /// Plays the <see cref="AudioClip"/> using <see cref="AudioSource.PlayOneShot(AudioClip)"/> call.
        /// Can be used to play multiple short sounds in parallel
        /// </summary>
        /// <param name="clip"><see cref="AuidoClip"/> to play</param>
        /// <param name="track">index of the track to use</param>
        public void PlayOneShot(AudioClip clip, int track = 0)
        {
            if (mute) { return; }
            TrackIndexCheck(track);
            audioSources[track].PlayOneShot(clip);
        }

        /// <summary>
        /// Plays the <see cref="AudioClip"/> using <see cref="AudioSource.PlayOneShot(AudioClip)"/> call after a specified delay.
        /// Can be used to play multiple short sounds in parallel
        /// </summary>
        /// <param name="clip"><see cref="AuidoClip"/> to play</param>
        /// <param name="delay">delay before playing the clip in seconds</param>
        /// <param name="track">index of the track to use</param>
        public void PlayOneShotDelayed(AudioClip clip, float delay, int track = 0)
        {
            if (mute) { return; }
            TrackIndexCheck(track);
            StartCoroutine(DelayedOneShot(clip, delay, track));
        }

        /// <summary>
        /// Stop playing clips on all the tracks.
        /// Doesn't stop playback by PlayOneShot.
        /// </summary>
        public void StopAll()
        {
            if (mute) { return; }
            audioSources.ForEach(source => source.Stop());
            clipsQueue.ForEach(clip => clip = null);
        }

        /// <summary>
        /// Plays the track or puts it to be played next.
        /// The queue of the next tracks is only 1 element long,
        /// the latest call always overrides previously queued tracks.
        /// </summary>
        /// <param name="clip"><see cref="AuidoClip"/> to play</param>
        /// <param name="track">index of the track to use</param>
        public void PlayOrQueue(AudioClip clip, int track = 0)
        {
            if (mute) { return; }
            if ((clip == audioSources[track].clip && audioSources[track].isPlaying) || clip == clipsQueue[track])
            {
                return;
            }
            if (!audioSources[track].isPlaying)
            {
                audioSources[track].clip = clip;
                audioSources[track].Play();
            }
            else
            {
                clipsQueue[track] = clip;
            }
        }

        /// <summary>
        /// Plays the audio clip if the track is empty
        /// </summary>
        /// <param name="clip"><see cref="AuidoClip"/> to play</param>
        /// <param name="track">index of the track to use</param>
        public void PlayIfEmpty(AudioClip clip, int track = 0)
        {
            if (mute) { return; }
            if (!audioSources[track].isPlaying)
            {
                audioSources[track].clip = clip;
                audioSources[track].Play();
            }
        }

        private void Update()
        {
            //resolve queued clips
            for (int i = 0; i < clipsQueue.Count; ++i)
            {
                if (clipsQueue[i] != null && !audioSources[i].isPlaying)
                {
                    PlaySound(clipsQueue[i], false, i);
                    clipsQueue[i] = null;
                }
            }
        }

        private IEnumerator DelayedOneShot(AudioClip clip, float delay, int track)
        {
            if (mute) { yield break; }
            yield return new WaitForSeconds(delay);
            audioSources[track].PlayOneShot(clip);
        }

        /// <summary>
        /// Check that the provided track index isn't out of range of the collection.
        /// For easier error discovery during the development.
        /// </summary>
        /// <param name="track"></param>
        [System.Diagnostics.Conditional("UNITY_EDITOR")]
        private void TrackIndexCheck(int track)
        {
            if (track >= audioSources.Count)
            {
                throw new IndexOutOfRangeException($"Trying to access track #{track} when there's only {audioSources.Count} tracks in the player of {gameObject.name}!");
            }
        }
    }
}
