﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace AIM.Sound
{
    public static class BossSounds
    {
        public const string BouncyBall = "bouncyBall";
        public const string DestructionDisk = "destructionDisk";
        public const string DoNothingBeam = "doNothingBeam";
        public const string EvilBeam = "evilBeam";
        public const string ElectricFieldEnd = "electricFieldEnd";
        public const string ElectricFieldStart = "electricFieldStart";
        public const string EyeSlam = "eyeSlam";
        public const string Stomp = "stomp";
        public const string Jump = "jump";
        public const string Lala = "lala";
        public const string LalaCountdown = "lalaCountdown";
        public const string LalaStun = "lalaStun";
        public const string ProtectionLiftUp = "protectionLiftUp";
        public const string ProtectionStart = "protectionStart";
        public const string Snipe = "snipe";
        public const string SpreadShot = "spreadShot";
        public const string Startup = "startup";
    }
}