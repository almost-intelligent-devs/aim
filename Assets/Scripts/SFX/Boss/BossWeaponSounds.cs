﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AIM.Sound
{
    public static class BossWeaponSounds
    {
        public const string BallBounce = "ballBounce";
        public const string BallDestroyed = "ballDestroyed";
        public const string BallBackground = "ballBackground";
        public const string SawBlades = "sawBlades";
        public const string EyeSlam = "eyeSlam";
        public const string JumpLand = "jumpLand";
        public const string EyeBeam = "eyeBeam";
        public const string ElectricField = "electricField";
    }
}
