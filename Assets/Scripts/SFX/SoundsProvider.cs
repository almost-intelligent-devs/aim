﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace AIM.Sound
{
    public abstract class SoundsProvider : MonoBehaviour
    {
        [SerializeField] protected List<AudioClip> clipsList;
        protected Dictionary<string, List<AudioClip>> clips;

        protected virtual void Awake()
        {
            if (clipsList == null || clipsList.Count < 1)
            {
                throw new InvalidOperationException($"{nameof(clipsList)} collection is not specified!");
            }
            clipsList.CheckForNullElements(nameof(clipsList));

            CheckAudioClips();

            clips = new Dictionary<string, List<AudioClip>>();
            foreach (var clip in clipsList)
            {
                var name = clip.name.Contains("_") ? clip.name.Split('_')[0] : clip.name;
                if (!clips.ContainsKey(name))
                {
                    clips.Add(name, new List<AudioClip>());
                }
                clips[name].Add(clip);
            }
        }

        public virtual AudioClip GetClip(string name)
        {
            if (name.Contains("_"))
            {
                name = name.Split('_')[0];
            }
            return clips[name].Count == 1 ? clips[name][0] : clips[name].GetRandomElement();
        }
        protected abstract void CheckAudioClips();

        protected void CheckAudioClips(Type type)
        {
            foreach (var name in type.GetConstantsStrings())
            {
                if (!clipsList.Any(clip => clip.name == name || clip.name.Split('_')[0] == name))
                {
                    throw new InvalidOperationException($"{nameof(SoundsProvider)} on object {gameObject.name} is missing sound clip for {name}!");
                }
            }
        }
    }
}
