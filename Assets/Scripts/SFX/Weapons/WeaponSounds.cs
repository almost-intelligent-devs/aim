﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AIM.Sound
{
    public static class WeaponSounds
    {
        public const string RiflePew = "riflePewPew";
        public const string WrenchClang = "wrenchClang";
        public const string SniperPew = "sniperPew";
        public const string LaserZzz = "laserZzz";
        public const string RocketLaunched = "rocketLaunched";
        public const string RocketFly = "rocketFly";
        public const string Explosion = "explosion";
        public const string BombTicking = "bombTick";
        public const string ProjectileContact = "projectileContact";
    }
}
