﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AIM.Sound
{
    public class WeaponSoundsProvider : SoundsProvider
    {
        protected override void CheckAudioClips() => CheckAudioClips(typeof(WeaponSounds));
    }
}
