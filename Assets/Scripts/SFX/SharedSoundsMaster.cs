﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AIM.Sound
{
    public class SharedSoundsMaster : Singleton<SharedSoundsMaster>
    {
        private const string WeaponsProviderPrefabPath = "WeaponSounds";
        private const string EnvironmentProviderPrefabPath = "EnvironmentSounds";
        private const string BossWeaponsSoundsProviderPrefabPath = "BossWeaponSounds";

        public WeaponSoundsProvider Weapons { get; private set; }
        public EnvironmentSoundsProvider Environment { get; private set; }
        public BossWeaponsSoundsProvider BossWeapons { get; private set; }

        protected override void Awake()
        {
            base.Awake();
            //Get sound providers!
            Weapons = Instantiate(Resources.Load<WeaponSoundsProvider>(WeaponsProviderPrefabPath), Vector3.zero, Quaternion.identity);
            Weapons.transform.SetParent(this.transform);
            Environment = Instantiate(Resources.Load<EnvironmentSoundsProvider>(EnvironmentProviderPrefabPath), Vector3.zero, Quaternion.identity);
            Environment.transform.SetParent(this.transform);
            BossWeapons = Instantiate(Resources.Load<BossWeaponsSoundsProvider>(BossWeaponsSoundsProviderPrefabPath), Vector3.zero, Quaternion.identity);
            BossWeapons.transform.SetParent(this.transform);

            if (Weapons == null)
            {
                throw new InvalidOperationException($"{nameof(Weapons)} is not specified!");
            }
            if (Environment == null)
            {
                throw new InvalidOperationException($"{nameof(Environment)} is not specified!");
            }
            if (BossWeapons == null)
            {
                throw new InvalidOperationException($"{nameof(BossWeapons)} is not specified!");
            }
        }
    }
}
