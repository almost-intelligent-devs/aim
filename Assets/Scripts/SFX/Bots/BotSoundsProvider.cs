﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using AIM.Bots.AI;

namespace AIM.Sound
{
    public class BotSoundsProvider : SoundsProvider
    {
        protected override void CheckAudioClips() => CheckAudioClips(typeof(BotSounds));
    }
}
