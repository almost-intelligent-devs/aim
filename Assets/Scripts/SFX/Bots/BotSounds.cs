﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AIM.Sound
{
    public static class BotSounds
    {
        public const string Damaged = "damaged";
        public const string Throw = "throw";
        public const string Dig = "dig";
        public const string Dash = "dash";
        public const string MeleeAttack = "meleeAttack";
        public const string EatHealthpack = "eatHealthPack";
        public const string BehaviourChange = "behaviourChange";
        // public const string RandomExclamation = "randomExclamation";
        public const string ItemPickup = "itemPickup";
        public const string KnockBack = "knockBack";
        public const string GettingUp = "getUp";
        public const string BotSelected = "botSelected";
        public const string BotControlled = "botControlled";
        public const string LALAPaninc = "lalaPanic";
        public const string Death = "death";
        public const string DamagedByBot = "damagedByBot";
        public const string BotBlocked = "botBlocked";
    }
}
