﻿using UnityEngine;
using System.Collections;

using AIM.Boss;
using AIM.Boss.Weakpoints;
using AIM.Bots;

namespace AIM.Projectile
{

    public class Laser : FireArmProjectile
    {
        [SerializeField]
        private float damage = 0.2f;
        [SerializeField]
        private GameObject laserBeamPrefab;

        public override float GetForwardSpeed() { return float.PositiveInfinity; }
        public override float GetRange() { return 40.0f; }

        protected override void Start()
        {
            // Project orientation onto xz-plane.
            var forwardDirectionWorldView = transform.localToWorldMatrix * GetForwardDirection();
            transform.rotation = Quaternion.FromToRotation(forwardDirectionWorldView, Vector3.ProjectOnPlane(forwardDirectionWorldView, Vector3.up)) * transform.rotation;

            RaycastHit hit;
            int layerMask = 1 << LayerMask.NameToLayer(LayerDictionary.NonTargetable);
            layerMask = ~layerMask;
            if (Physics.Raycast(transform.position, transform.localToWorldMatrix * GetForwardDirection(), out hit, GetRange(), layerMask))
            {
                base.Start();
                var hitPosition = hit.point;
                var beam = Instantiate(laserBeamPrefab, (hitPosition + startingPosition) / 2.0f, transform.rotation * Quaternion.FromToRotation(laserBeamPrefab.GetComponent<LaserBeam>().GetForwardDirection(), GetForwardDirection()));
                beam.transform.localScale = new Vector3(beam.transform.localScale.x, hit.distance / 2.0f, beam.transform.localScale.z);

                var tag = hit.transform.gameObject.tag;

                if (tag == TagDictionary.Boss)
                    hit.transform.GetComponentInParent<BossController>().ChangeHP(-damage);

                if (tag == TagDictionary.Bot)
                    hit.transform.GetComponent<AIMBotController>().ChangeHP(-damage, shooter);

                if (tag == TagDictionary.BossWeakPoint)
                    hit.transform.GetComponent<BossWeakPoint>().DealCriticalDamage(damage);
            }
            else
            {
                Debug.Log("No hit!");
            }
        }

        protected override void Update()
        {
            Destroy(gameObject);
        }

        public override void ProcessHit(Collider other)
        {

        }

    }

}
