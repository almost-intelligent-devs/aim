﻿using AIM.Bots;
using AIM.Sound;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AIM.Projectile
{
    public class BouncyProjectile : MonoBehaviour
    {
        Rigidbody rigidbody;

        //baseSpeed
        [SerializeField]
        private float speed = 5;
        [SerializeField]
        private float maxSpeed;
        [SerializeField]
        private float speedIncreasePerSecond;

        //baseDamage
        [SerializeField]
        private float damage;
        [SerializeField]
        private float maxDamage;
        [SerializeField]
        private float damageIncreasePerSecond;

        private float maxYSpeed = 7f;

        private float rotationSpeed;
        private float xRotation = 0;

        [SerializeField]
        private float size;
        [SerializeField]
        private float sizeIncreasePerSecond;
        [SerializeField]
        private float maxSize;

        [SerializeField] protected SoundsPlayer soundsPlayer;
        void Start()
        {
            maxDamage *= 2f;//30 => 60(90 through bot damageMultiplier)
            maxSpeed *= 1.5f; //9 => 13.5
            this.transform.localScale = new Vector3(size, size, size);
            rigidbody = this.transform.GetComponent<Rigidbody>();
        }

        void Update()
        {
            soundsPlayer.PlayIfEmpty(SharedSoundsMaster.Instance.BossWeapons.GetClip(BossWeaponSounds.BallBackground));
        }

        /// <summary>
        /// Physics updates(Acceleration)
        /// </summary>
        void FixedUpdate()
        {
            //Increase damage and speed
            if (speed < maxSpeed)
                speed += speedIncreasePerSecond * Time.fixedDeltaTime;
            else
                speed = maxSpeed;

            rotationSpeed = 50 * speed;

            if (damage < maxDamage)
                damage += damageIncreasePerSecond * Time.fixedDeltaTime;
            else
                damage = maxDamage;

            if (size < maxSize)
                size += sizeIncreasePerSecond * Time.fixedDeltaTime;
            else
                size = maxSize;

            //SpeedIncrease
            Vector3 currentVelocity = rigidbody.velocity;
            float yVelocity = Mathf.Sign(currentVelocity.y) * Mathf.Min(Mathf.Abs(currentVelocity.y), maxYSpeed);
            if(this.transform.position.y > ArenaData.WallHeight)
            {
                yVelocity = -1f;
            }
            currentVelocity.y = 0;
            currentVelocity = currentVelocity.normalized;
            rigidbody.velocity = new Vector3(speed * currentVelocity.x, yVelocity, speed * currentVelocity.z);

            //Rotation
            xRotation += rotationSpeed * Time.fixedDeltaTime;
            float yRotDirection = Quaternion.LookRotation(new Vector3(rigidbody.velocity.x, 0, rigidbody.velocity.z)).eulerAngles.y;
            this.transform.rotation = Quaternion.Euler(xRotation, yRotDirection, 0);

            //Size Increase
            this.transform.localScale = new Vector3(size, size, size);
        }

        private void OnCollisionEnter(Collision collision)
        {

            if (collision.gameObject.tag == TagDictionary.Bot)
            {
                var forceDir = rigidbody.velocity;
                collision.gameObject.GetComponent<AIMBotController>().ChangeHP(-damage, this.gameObject, forceDir.normalized * ArenaData.HardRagDollForce);
                soundsPlayer.transform.SetParent(null, true);
                var clip = SharedSoundsMaster.Instance.BossWeapons.GetClip(BossWeaponSounds.BallDestroyed);
                soundsPlayer.PlayOneShot(clip);
                Destroy(this.gameObject);
                Destroy(soundsPlayer.gameObject, clip.length);
            }
            else
            {
                soundsPlayer.PlayOneShot(SharedSoundsMaster.Instance.BossWeapons.GetClip(BossWeaponSounds.BallBounce));
            }
        }
    }
}
