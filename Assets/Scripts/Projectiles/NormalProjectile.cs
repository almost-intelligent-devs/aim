﻿using AIM.Boss;
using AIM.Boss.Weakpoints;
using AIM.Bots;
using AIM.Sound;
using System.Collections;
using UnityEngine;

namespace AIM.Projectile
{
    public class NormalProjectile : MonoBehaviour
    {
        private enum HIT { None, Normal, WeakPoint };
        [SerializeField]
        private int damage = 10;

        [SerializeField]
        private float range = 10;

        [SerializeField]
        private float speed = 10;

        [SerializeField] protected SoundsPlayer soundsPlayer;

        public GameObject Source { get; set; }
        private HIT hit = HIT.None;
        #region Unity lifecycle
        void Start()
        {
            StartCoroutine(InitiateProjectileDestruction());
        }

        void Update()
        {
            float distance = Time.deltaTime * speed;
            if (speed > 50)
                TestFastCollision(distance);

            transform.Translate(Vector3.forward * distance);
        }
        #endregion

        #region CollisionResolution

        private void TestFastCollision(float distance)
        {
            RaycastHit hit;
            if (Physics.Raycast(this.transform.position, this.transform.forward, out hit, distance))
            {
                OnTriggerEnter(hit.collider);
            }
        }
        private static int counter = 0;
        private void OnTriggerEnter(Collider other)
        {
            //prevents shooter from shooting themselves
            if (other.transform.IsChildOf(Source.transform))
                return;

            if (other.gameObject.tag == TagDictionary.Bot)
            {
                var force = other.transform.position - Source.transform.position;
                force.y = 0f;
                other.GetComponent<AIMBotController>().ChangeHP(-damage, Source, force.normalized * ArenaData.MediumRagdollForce);
            }
            else if (other.gameObject.tag == TagDictionary.BossWeakPoint)
            {
                if (hit == HIT.Normal)
                {
                    other.GetComponent<BossWeakPoint>().DealBonusDamage(damage);//deal only weakPoint damage
                }
                else if (hit == HIT.None)
                {
                    other.GetComponent<BossWeakPoint>().DealCriticalDamage(damage);//deal normal + bonus weakPoint damage
                }
                hit = HIT.WeakPoint;
            }
            else if (other.gameObject.tag == TagDictionary.Boss && hit == HIT.None)
            {
                other.GetComponentInParent<BossController>().ChangeHP(-damage);
                hit = HIT.Normal;
            }
            soundsPlayer.transform.SetParent(null, true);
            var clip = SharedSoundsMaster.Instance.Weapons.GetClip(WeaponSounds.ProjectileContact);
            soundsPlayer.PlayOneShot(clip);
            Destroy(soundsPlayer.gameObject, clip.length);
            ProjectileDestruction();
        }
        #endregion

        private IEnumerator InitiateProjectileDestruction()
        {
            //Wait Lifetime of projectile and then get destroyed
            yield return new WaitForSeconds(range / speed);
            ProjectileDestruction();
        }

        private void ProjectileDestruction()
        {

            //add destruction animation
            Destroy(this.gameObject);
        }

        public float GetRange()
        {
            return range;
        }

        public float GetSpeed()
        {
            return speed;
        }
    }
}
