﻿using AIM.Effects;
using AIM.Sound;
using System;
using UnityEngine;

namespace AIM.Projectile
{

    public class Rocket : FireArmProjectile
    {
        [SerializeField]
        private GameObject explosionPrefab;
        [SerializeField]
        private GameObject smokeTrailPrefab;

        [SerializeField]
        protected SoundsPlayer soundsPlayer;

        // Velocity of Rocket in world space.
        private Vector3 velocity;
        private int smoketailEmissionDelay = 3;
        public override float GetForwardSpeed() { return 40.0f; }
        public override float GetRange() { return 30.0f; }
        public override Vector3 GetForwardDirection() { return Vector3.forward; }

        private SmokeTrail smokeTrail;

        protected override void Start()
        {
            base.Start();
            velocity = transform.localToWorldMatrix * GetForwardDirection() * GetForwardSpeed();
            velocity.y = 0.0f;

            smokeTrail = Instantiate(smokeTrailPrefab, transform.position, transform.rotation).GetComponent<SmokeTrail>();
            if (smokeTrail == null)
            {
                throw new InvalidOperationException($"{nameof(smokeTrailPrefab)} does not have a {nameof(SmokeTrail)} component");
            }
            smokeTrail.StartSpeed = GetForwardSpeed();
        }

        protected override void Update()
        {
            base.Update();
            soundsPlayer.PlayIfEmpty(SharedSoundsMaster.Instance.Weapons.GetClip(WeaponSounds.RocketFly));
        }

        public override void ProcessHit(Collider other)
        {
            var expl = Instantiate(explosionPrefab, transform.position, Quaternion.identity);
            var explScript = expl.GetComponent<Explosion>();
            explScript.cause = shooter;
            explScript.DoExplosion();
        }

        private void OnDestroy()
        {
            smokeTrail.Kill(transform.position);
        }
    }
}
