﻿using UnityEngine;
using System.Collections;

using AIM.Boss;
using AIM.Bots;

namespace AIM.Projectile
{

    public class BulletProjectile : FireArmProjectile
    {
        [SerializeField]
        private float damage = 25.0f;

        [SerializeField]
        private float range = 30.0f;
        public override float GetRange() { return range; }

        [SerializeField]
        private float forwardSpeed = 15.0f;
        public override float GetForwardSpeed() { return forwardSpeed; }

        public override void ProcessHit(Collider other)
        {
            if (other.gameObject.tag == TagDictionary.Boss)
                other.GetComponentInParent<BossController>().ChangeHP(-damage);

            if (other.gameObject.tag == TagDictionary.Bot)
                other.GetComponent<AIMBotController>().ChangeHP(-damage, shooter);
        }
    }

}
