﻿using UnityEngine;
using System.Collections;

public class LaserBeam : MonoBehaviour
{
    [SerializeField]
    private float lifetime = 0.05f;

    public Vector3 GetForwardDirection() { return Vector3.up; }

    private void Start()
    {
        StartCoroutine(LifeCycle());
    }

    private IEnumerator LifeCycle()
    {
        yield return new WaitForSeconds(lifetime);
        Destroy(gameObject);
    }
}
