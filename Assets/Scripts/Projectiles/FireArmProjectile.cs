﻿using UnityEngine;
using System.Collections;

namespace AIM.Projectile
{

    public abstract class FireArmProjectile : MonoBehaviour
    {
        [SerializeField]
        public GameObject shooter { get; set; }
        public abstract float GetRange();
        public abstract float GetForwardSpeed();
        public abstract void ProcessHit(Collider other);

        protected Vector3 startingPosition;

        public virtual Vector3 GetForwardDirection() { return Vector3.forward; }

        protected virtual void Start()
        {
            startingPosition = transform.position;
        }

        protected virtual void Update()
        {
            Move();
            if (Vector3.Distance(transform.position, startingPosition) > GetRange())
                Destroy(gameObject);
        }

        protected virtual void Move()
        {
            transform.Translate(GetForwardDirection() * Time.deltaTime * GetForwardSpeed());
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject == shooter)
                return;

            ProcessHit(other);
            Destroy(gameObject);
        }
        
    }

}
