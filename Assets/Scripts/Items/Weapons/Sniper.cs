﻿using UnityEngine;
using System.Collections;

using AIM.Projectile;
using AIM.Sound;

namespace AIM.Items
{

    public class Sniper : FireArm
    {
        [SerializeField]
        private int maxAmmunitionCount = 10;
        public override int GetMaxAmmunitionCount() { return maxAmmunitionCount; }

        [SerializeField]
        private float cooldown = 1.0f;
        public override float GetCooldown() { return cooldown; }

        [SerializeField]
        private NormalProjectile projectilePrefab;
        [SerializeField] private ParticleSystem psMuzzleFlash;


        public override void Shoot()
        {
            var bullet = Instantiate(projectilePrefab, front.position, gameObject.transform.rotation);
            bullet.Source = gameObject.transform.parent.gameObject;
            psMuzzleFlash.Play();
            soundsPlayer.PlayOneShot(SharedSoundsMaster.Instance.Weapons.GetClip(WeaponSounds.SniperPew));
        }

        public override float GetProjectileVelocity()
        {
            return projectilePrefab.GetSpeed();
        }
    }

}
