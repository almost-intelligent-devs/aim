﻿using UnityEngine;
using System.Collections;

using AIM.Bots;
using AIM.Effects;
using System;
using AIM.Sound;

namespace AIM.Items
{
    public class Bomb : Item
    {
        [SerializeField]
        private GameObject explosionPrefab;

        [SerializeField]
        private float fuse = 5.0f;

        [SerializeField]
        BombDisplay bombDisplay;

        protected override void Awake()
        {
            base.Awake();

            if (explosionPrefab == null)
            {
                throw new InvalidOperationException($"{nameof(explosionPrefab)} is not specified!");
            }
            if (bombDisplay == null)
            {
                throw new InvalidOperationException($"{nameof(bombDisplay)} is not specified!");
            }
        }

        protected override void Start()
        {
            base.Start();
            StartCoroutine(Fuse());
        }

        public override void Use(AIMBotController user)
        {

        }

        private IEnumerator Fuse()
        {
            const float fastTickStart = 1f;
            var currentTime = fuse;
            var seconds = 0f;
            while (currentTime > 0f)
            {
                bombDisplay.SetTime(Mathf.FloorToInt(currentTime * 1000f));
                if(currentTime < fastTickStart)
                {
                    soundsPlayer.PlayOneShot(SharedSoundsMaster.Instance.Weapons.GetClip(WeaponSounds.BombTicking));
                }
                else
                {
                    if(seconds > 1f)
                    {
                        seconds -= 1f;
                        soundsPlayer.PlayIfEmpty(SharedSoundsMaster.Instance.Weapons.GetClip(WeaponSounds.BombTicking));
                    }
                }
                yield return null;
                currentTime -= Time.deltaTime;
                seconds += Time.deltaTime;
            }
            Explode();
        }

        public void Explode()
        {
            var expl = Instantiate(explosionPrefab, transform.position, Quaternion.identity);
            expl.GetComponent<Explosion>().DoExplosion();
            Destroy(gameObject);
        }
    }
}
