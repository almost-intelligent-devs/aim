﻿using System;
using System.Collections;
using System.Collections.Generic;
using AIM.Bots;
using AIM.Projectile;
using AIM.Sound;
using UnityEngine;

namespace AIM.Items
{
    public class AssaultRifle : FireArm
    {
        [SerializeField]
        private int maxAmmunitionCount = 30;
        public override int GetMaxAmmunitionCount() { return maxAmmunitionCount; }

        [SerializeField]
        private float cooldown = .2f;
        public override float GetCooldown() { return cooldown; }

        [SerializeField]
        private NormalProjectile projectilePrefab;
        [SerializeField] private ParticleSystem psMuzzleFlash;

        public override void Shoot()
        {
            var bullet = Instantiate(projectilePrefab, front.position, gameObject.transform.rotation);
            bullet.Source = gameObject.transform.parent.gameObject;
            psMuzzleFlash.Play();
            soundsPlayer.PlayOneShot(SharedSoundsMaster.Instance.Weapons.GetClip(WeaponSounds.RiflePew));
        }

        public override float GetProjectileVelocity()
        {
            return projectilePrefab.GetSpeed();
        }
    }
}
