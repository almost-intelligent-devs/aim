﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AIM.Items
{
    public abstract class Weapon : Item
    {
        /// <summary>
        /// Get effective range of the weapon
        /// </summary>
        /// <returns>Effective range of the weapon</returns>
        public abstract float GetRange();
        /// <summary>
        /// Get the point from which the bullets are spwaned
        /// </summary>
        /// <returns>World space position of the bullets origin</returns>
        public abstract Vector3 GetBulletSpawnPoint();
        /// <summary>
        /// Get the direction in which the gun is facing (where the bullets will fly)
        /// </summary>
        /// <returns>World space direction vector for the bullet</returns>
        public abstract Vector3 GetGunDirection();
        /// <summary>
        /// Get the velocity magnitude of the projectiles the weapon shoots
        /// </summary>
        /// <returns>projectile velocity</returns>
        public abstract float GetProjectileVelocity();
    }
}
