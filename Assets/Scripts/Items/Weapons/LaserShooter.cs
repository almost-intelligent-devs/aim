﻿using UnityEngine;
using System.Collections;

using AIM.Bots;
using AIM.Projectile;
using AIM.Sound;

namespace AIM.Items
{

    public class LaserShooter : FireArm
    {
        [SerializeField]
        private int maxAmmunitionCount = 500;
        public override int GetMaxAmmunitionCount() { return maxAmmunitionCount; }

        [SerializeField]
        private float cooldown = 0.0f;
        public override float GetCooldown() { return cooldown; }

        [SerializeField]
        private Laser laserPrefab;

        public override void Shoot()
        {
            var laser = Instantiate(laserPrefab, front.position, gameObject.transform.rotation * Quaternion.FromToRotation(laserPrefab.GetForwardDirection(), Vector3.forward));
            laser.shooter = gameObject.transform.parent.gameObject;
            soundsPlayer.PlayIfEmpty(SharedSoundsMaster.Instance.Weapons.GetClip(WeaponSounds.LaserZzz));
        }

        public override float GetProjectileVelocity()
        {
            return laserPrefab.GetForwardSpeed();
        }
    }

}
