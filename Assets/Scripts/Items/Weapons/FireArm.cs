﻿using UnityEngine;
using System.Collections;

using AIM.Projectile;
using AIM.Bots;

namespace AIM.Items
{

    public abstract class FireArm : Weapon
    {
        [SerializeField]
        protected Transform front;

        protected int currentAmmunition;

        public abstract int GetMaxAmmunitionCount();
        public abstract float GetCooldown();
        public abstract void Shoot();

        protected bool loaded = true;

        protected override void Start()
        {
            base.Start();
            currentAmmunition = GetMaxAmmunitionCount();
        }

        public override float GetRange()
        {
            return 20.0f;
        }

        public override void Use(AIMBotController user)
        {
            if (loaded)
            {
                Shoot();
                NotifySuccessfulUsage();
                loaded = false;
                StartCoroutine(Reload());
                currentAmmunition--;
                if (currentAmmunition <= 0)
                    Destroy(gameObject);
            }
        }

        private IEnumerator Reload()
        {
            yield return new WaitForSeconds(GetCooldown());
            loaded = true;
        }

        public override Vector3 GetBulletSpawnPoint()
        {
            return front.position;
        }

        public override Vector3 GetGunDirection()
        {
            return front.parent.forward;
        }
    }

}
