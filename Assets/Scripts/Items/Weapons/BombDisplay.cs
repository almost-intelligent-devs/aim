﻿using UnityEngine;
using System;
using TMPro;

public class BombDisplay : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI txtSeconds;
    [SerializeField] private TextMeshProUGUI txtMillis;

    void Awake()
    {
        if (txtSeconds == null)
        {
            throw new InvalidOperationException($"{nameof(txtSeconds)} is not specified!");
        }
        if (txtMillis == null)
        {
            throw new InvalidOperationException($"{nameof(txtMillis)} is not specified!");
        }
    }

    public void SetTime(int milliseconds)
    {
        const int MAX_TIMER_VALUE = 99999;
        if (milliseconds > MAX_TIMER_VALUE || milliseconds < 0)
        {
            throw new ArgumentOutOfRangeException($"Milliseconds must be larger than zero and smaller equal than {MAX_TIMER_VALUE}");
        }

        TimeSpan timeSpan = TimeSpan.FromMilliseconds(milliseconds);
        txtSeconds.text = ((int)timeSpan.TotalSeconds).ToString("00");
        txtMillis.text = timeSpan.Milliseconds.ToString("000");
    }

    // void Start()
    // {
    //     currentTime = 10000;
    // }

    // private int currentTime = 0;

    // private float timePassed = 0f;
    // public void Update()
    // {
    //     if (currentTime > 0)
    //     {
    //         timePassed += Time.deltaTime;
    //         int milisecondPassed = Mathf.FloorToInt(timePassed * 1000f);
    //         currentTime -= milisecondPassed;
    //         SetTime(Mathf.Max(0, currentTime));
    //         timePassed -= milisecondPassed / 1000f;
    //     }
    // }

}
