﻿using AIM.Projectile;
using AIM.Sound;
using UnityEngine;

namespace AIM.Items
{

    public class RocketLauncher : FireArm
    {
        [SerializeField]
        private int maxAmmunitionCount = 3;
        public override int GetMaxAmmunitionCount() { return maxAmmunitionCount; }

        [SerializeField]
        private float cooldown = 1.0f;
        public override float GetCooldown() { return cooldown; }

        [SerializeField]
        private Rocket rocketPrefab;

        public override void Shoot()
        {
            var rocket = Instantiate(rocketPrefab, front.position, Quaternion.LookRotation(front.forward));
            rocket.shooter = gameObject.transform.parent.gameObject;
            soundsPlayer.PlayOneShot(SharedSoundsMaster.Instance.Weapons.GetClip(WeaponSounds.RocketLaunched));
        }

        public override float GetProjectileVelocity()
        {
            return rocketPrefab.GetForwardSpeed();
        }
    }

}
