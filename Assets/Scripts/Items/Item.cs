﻿using AIM.Bots;
using AIM.Sound;
using System;
using System.Collections;
using UnityEngine;

namespace AIM.Items
{
    /// <summary>
    /// Base class for pick-up-able items in the game
    /// </summary>
    [RequireComponent(typeof(Collider), typeof(Rigidbody))]
    public abstract class Item : MonoBehaviour
    {
        public event EventHandler OnItemDestroyed;
        public event EventHandler ItemUsed;

        private GameObject objToIgnore;

        protected SoundsPlayer soundsPlayer;

        /// <summary>
        /// Is the item equipped?
        /// </summary>
        /// <value><see langword="true"/> if it is currently equipped by the bot</value>
        public bool Equipped
        {
            get
            {
                return equipped;
            }
            set
            {
                equipped = value;
                collider.enabled = !value;
                rigidbody.detectCollisions = !value;
                rigidbody.isKinematic = value;
            }
        }

        private bool equipped;

        new protected Collider collider;
        new protected Rigidbody rigidbody;

        /// <summary>
        /// All the logic that governs the item being used goes here
        /// </summary>
        public abstract void Use(AIMBotController user);

        /// throw the item with the impulse
        /// </summary>
        /// <param name="force">impulse vector</param>
        public void Throw(Vector3 force, GameObject thrower)
        {
            StartCoroutine(PickupTimeout(thrower, CoroutineCancellationToken.Empty));
            rigidbody.AddForce(force, ForceMode.Impulse);
        }

        protected virtual void Awake()
        {
            collider = GetComponent<Collider>();
            if (collider == null)
            {
                throw new InvalidOperationException($"{nameof(collider)} is not specified!");
            }
            rigidbody = GetComponent<Rigidbody>();
            if (rigidbody == null)
            {
                throw new InvalidOperationException($"{nameof(rigidbody)} is not specified!");
            }
            soundsPlayer = GetComponentInChildren<SoundsPlayer>();
            if (soundsPlayer == null)
            {
                throw new InvalidOperationException($"{nameof(soundsPlayer)} is not specified!");
            }
        }

        protected virtual void Start()
        {
            Gamemaster.Instance.Register(this);
        }

        protected virtual void OnDestroy()
        {
            if (!Gamemaster.Instance?.ApplicationQuit ?? false)
                OnItemDestroyed?.Invoke(this, null);
            Gamemaster.Instance?.Unregister(this);
        }

        /// <summary>
        /// If the bot walks over, the item tries to be picked up
        /// </summary>
        /// <param name="other"></param>
        protected virtual void OnCollisionEnter(Collision collision)
        {
            var other = collision.collider;
            if (other.gameObject != objToIgnore)
            {
                if (other.tag == TagDictionary.Bot)
                {
                    var botController = other.GetComponent<AIMBotController>();
                    if (botController.EquipItem(this))
                    {
                        Equipped = true;
                    }
                }
                else
                {
                    soundsPlayer.PlayOneShot(SharedSoundsMaster.Instance.Environment.GetClip(EnvironmentSounds.RigidBodyCollided));
                }
            }
        }

        protected void NotifySuccessfulUsage()
        {
            ItemUsed?.Invoke(this, EventArgs.Empty);
        }

        private IEnumerator PickupTimeout(GameObject target, CoroutineCancellationToken cancellationToken)
        {
            const float Timeout = .5f;
            var timeElapsed = 0f;
            objToIgnore = target;
            while (!cancellationToken.CancellationRequested && timeElapsed < Timeout)
            {
                yield return null;
                timeElapsed += Time.deltaTime;
            }
            objToIgnore = null;
        }
    }
}
