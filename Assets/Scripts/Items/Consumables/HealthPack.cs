﻿using UnityEngine;
using System.Collections;
using AIM.Bots;

namespace AIM.Items
{

    public class HealthPack : Item
    {
        [SerializeField]
        private int restoredHealth = 25;

        public override void Use(AIMBotController holder)
        {
            holder.ChangeHP(restoredHealth, this.gameObject);
            NotifySuccessfulUsage();
            Destroy(gameObject);
        }
    }
}
