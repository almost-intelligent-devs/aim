﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AIM.Interactables
{
    public class SimpleInteractable : Interactable
    {
        public override void Interact(GameObject interactor)
        {
            Debug.Log($"Gameobject {interactor.name} interacted with {gameObject.name}!");
        }
    }
}
