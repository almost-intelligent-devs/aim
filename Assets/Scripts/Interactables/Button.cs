﻿using AIM.Controllables;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AIM.Bots.AI;
using AIM.Sound;
using System;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace AIM.Interactables
{
    [RequireComponent(typeof(Animator), typeof(Renderer))]
    public class Button : Interactable, IPunchable
    {
        #region Constants
        private readonly int TPush = Animator.StringToHash("Push");
        private readonly int EmissionId = Shader.PropertyToID("_EmissionColor");
        private readonly Color ColorNotInteractable = Color.black;
        private readonly Color ColorActivated = new Color(0f, 2f, 0f);
        private readonly Color ColorDeactivated = new Color(2f, 0f, 0f);
        #endregion

        #region Inspector Fields
        [Tooltip("Which objects in the game world does this button control?")]
        [SerializeField] private List<Controllable> controllables;

        [Tooltip("Can the button be interacted with?")]
        [SerializeField] protected bool isInteractable = false;

        [Tooltip("Is the button currently activated?")]
        [SerializeField] protected bool isActivated = false;

        [Tooltip("How often must the button be hit to activate?")]
        [SerializeField] private int hitsToActivate = 1;

        [HideInInspector]
        [SerializeField]
        private ButtonMode buttonMode = ButtonMode.Toggle;
        [HideInInspector]
        [SerializeField]
        private float autoResetTime = 2f;
        #endregion

        private SoundsPlayer soundsPlayer;

        private Animator animator;
        private Material material;
        private CoroutineCancellationToken cancelSetActiveToken;
        private int currentHitCount = 0;

        protected virtual void Awake()
        {
            animator = GetComponent<Animator>();
            material = GetComponent<Renderer>().material;

            soundsPlayer = GetComponentInChildren<SoundsPlayer>();
            if (soundsPlayer == null)
            {
                throw new InvalidOperationException($"{nameof(soundsPlayer)} is not specified!");
            }
        }

        protected override void Start()
        {
            base.Start();

            if (isInteractable)
            {
                SetEmissiveColor(isActivated ? ColorActivated : ColorDeactivated);
            }
            else
            {
                SetEmissiveColor(ColorNotInteractable);
            }

            foreach (Controllable c in controllables)
            {
                c.Init(isActivated);
            }
        }

        #region Setter

        private void SetEmissiveColor(Color color) => material.SetColor(EmissionId, color);

        protected void SetActivated(bool isActivated)
        {
            if (this.isActivated != isActivated)
            {
                this.isActivated = isActivated;

                if (isActivated)
                {
                    SetEmissiveColor(ColorActivated);
                    foreach (Controllable c in controllables) { c.Activate(this); }
                    soundsPlayer.PlayOneShot(SharedSoundsMaster.Instance.Environment.GetClip(EnvironmentSounds.ButtonActivated));
                }
                else
                {
                    SetEmissiveColor(ColorDeactivated);
                    foreach (Controllable c in controllables) { c.Deactivate(this); }
                    soundsPlayer.PlayOneShot(SharedSoundsMaster.Instance.Environment.GetClip(EnvironmentSounds.ButtonDeactivated));
                }
            }
        }

        public void SetInteractable(bool isInteractable)
        {
            if (this.isInteractable != isInteractable)
            {
                this.isInteractable = isInteractable;

                cancelSetActiveToken?.Cancel();
                cancelSetActiveToken = null;

                if (isInteractable)
                {
                    SetEmissiveColor(isActivated ? ColorActivated : ColorDeactivated);
                }
                else
                {
                    SetEmissiveColor(ColorNotInteractable);
                }
            }
        }

        #endregion

        public override void Interact(GameObject interactor)
        {
            animator.SetTrigger(TPush);

            if (isInteractable)
            {
                currentHitCount++;
                if (currentHitCount >= hitsToActivate)
                {
                    currentHitCount = 0;

                    switch (buttonMode)
                    {
                        case ButtonMode.Toggle:
                            SetActivated(!isActivated);
                            break;
                        case ButtonMode.AutoDeactivate:
                            cancelSetActiveToken?.Cancel();
                            cancelSetActiveToken = new CoroutineCancellationToken();
                            StartCoroutine(SetActiveAndReset(cancelSetActiveToken));
                            break;
                    }
                }
            }
        }

        private IEnumerator SetActiveAndReset(CoroutineCancellationToken ct)
        {
            SetActivated(true);
            SetEmissiveColor(ColorActivated);

            yield return new WaitForSeconds(autoResetTime);

            if (!ct.CancellationRequested)
            {
                SetEmissiveColor(ColorDeactivated);
                SetActivated(false);
            }
        }

        private void RefreshActiveAndInteractable()
        {


            isInteractable = !isInteractable;
            SetInteractable(!isInteractable);
        }


        #region Bots AI Utils
        private List<AttackToken> attackTokens;
        public List<AttackToken> GetAttackTokens()
        {
            if (attackTokens == null)
            {
                attackTokens = AttackToken.GenerateTokensAroundCollider(this.transform, GetComponent<Collider>(), ArenaData.AgentRadius / 2f, .9f, -50, 50);
            }
            return attackTokens;
        }


        private void OnDrawGizmos()
        {
            var tokens = GetAttackTokens();
            foreach (var token in tokens)
            {
                if (token.State == TokenState.Open)
                {
                    Gizmos.DrawWireSphere(token.Position, .1f);
                }
                else
                {
                    Gizmos.DrawSphere(token.Position, .1f);
                }
            }
        }

        #endregion

        #region Editor
#if UNITY_EDITOR
        void OnValidate()
        {
            if (Application.isPlaying && !Gamemaster.Instance.ApplicationQuit)
            {
                try { RefreshActiveAndInteractable(); }
                catch { }
            }
        }

        [CustomEditor(typeof(Button), true)]
        [CanEditMultipleObjects]
        private class MyScriptEditor : UnityEditor.Editor
        {
            SerializedProperty buttonModeProp;
            SerializedProperty autoResetTimeProp;
            SerializedProperty isInteractableProp;
            SerializedProperty isActivatedProp;

            void OnEnable()
            {
                buttonModeProp = serializedObject.FindProperty(nameof(buttonMode));
                autoResetTimeProp = serializedObject.FindProperty(nameof(autoResetTime));
                isInteractableProp = serializedObject.FindProperty(nameof(isInteractable));
                isActivatedProp = serializedObject.FindProperty(nameof(isActivated));
            }

            public override void OnInspectorGUI()
            {
                var button = target as Button;
                bool isActivatedOld = button.isActivated;
                bool isInteractableOld = button.isInteractable;

                serializedObject.Update();
                DrawDefaultInspector();

                EditorGUILayout.PropertyField(buttonModeProp,
                    new GUIContent(ObjectNames.NicifyVariableName(nameof(buttonMode)),
                    $"{nameof(ButtonMode.Toggle)}: (de-)activate on hit | {nameof(ButtonMode.AutoDeactivate)}: hit only activates, deactivated after certain time"));

                if ((ButtonMode)buttonModeProp.enumValueIndex != button.buttonMode)
                {
                    button.cancelSetActiveToken?.Cancel();
                    button.cancelSetActiveToken = null;
                }
                using (new EditorGUI.DisabledScope(button.buttonMode != ButtonMode.AutoDeactivate))
                {
                    EditorGUI.indentLevel++;
                    EditorGUILayout.PropertyField(autoResetTimeProp,
                        new GUIContent(ObjectNames.NicifyVariableName(nameof(button.autoResetTime)),
                        "Time after which the button is automatically deactivated"));
                    EditorGUI.indentLevel--;
                }

                if (Application.isPlaying && !Gamemaster.Instance.ApplicationQuit)
                {
                    try
                    {
                        if (isInteractableProp.boolValue != isInteractableOld)
                        {
                            button.isInteractable = isInteractableOld;
                            button.SetInteractable(isInteractableProp.boolValue);
                        }
                    }
                    catch { }
                    try
                    {
                        if (isActivatedProp.boolValue != isActivatedOld)
                        {
                            button.isActivated = isActivatedOld;
                            if (isActivatedProp.boolValue && (ButtonMode)buttonModeProp.enumValueIndex == ButtonMode.AutoDeactivate)
                            {
                                button.cancelSetActiveToken?.Cancel();
                                button.cancelSetActiveToken = new CoroutineCancellationToken();
                                button.StartCoroutine(button.SetActiveAndReset(button.cancelSetActiveToken));
                            }
                            else
                            {
                                button.SetActivated(isActivatedProp.boolValue);
                            }
                        }
                    }
                    catch { }
                }

                serializedObject.ApplyModifiedProperties();
            }
        }
#endif
        #endregion
    }

    public enum ButtonMode { Toggle, AutoDeactivate }
}
