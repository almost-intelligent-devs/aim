﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AIM.Interactables
{
    /// <summary>
    /// Base class for the interactables objects within the game.
    /// Each interactable objects implements the interaction logic
    /// within the <see cref="Interactable.Interact(GameObject)"/> method.
    /// </summary>
    public abstract class Interactable : MonoBehaviour
    {
        protected virtual void Start()
        {
            if (this.gameObject.tag != TagDictionary.Interactable)
            {
                throw new InvalidOperationException($"{nameof(Interactable)} under the name of {gameObject.name} doesn't have the proper tag = \"{TagDictionary.Interactable}\" on it! Interactions will not work for this item!");
            }
            Gamemaster.Instance.Register(this);
        }

        private void OnDestroy()
        {
            Gamemaster.Instance?.Unregister(this);
        }

        /// <summary>
        /// Interaction logic
        /// </summary>
        /// <param name="interactor">Game object that initiated the interaction</param>
        public abstract void Interact(GameObject interactor);
    }
}
