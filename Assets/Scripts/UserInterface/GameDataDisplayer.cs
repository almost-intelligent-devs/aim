﻿using System;
using TMPro;
using UnityEngine;

namespace AIM.Menu
{
    public class GameDataDisplayer : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI txtResult;
        [SerializeField] private TextMeshProUGUI txtTime, txtBots, txtHealth;

        private void Awake()
        {

            if (!txtResult)
            {
                throw new InvalidOperationException($"{nameof(txtResult)} is not specified!");
            }
            if (!txtTime)
            {
                throw new InvalidOperationException($"{nameof(txtTime)} is not specified!");
            }
            if (!txtBots)
            {
                throw new InvalidOperationException($"{nameof(txtBots)} is not specified!");
            }
            if (!txtHealth)
            {
                throw new InvalidOperationException($"{nameof(txtHealth)} is not specified!");
            }
        }

        public void UpdateData()
        {
            GameData gd = GameData.Instance;

            txtResult.text = (gd.BotsAlive > 0) ? "Victory" : "Defeat";
            TimeSpan timeSpan = TimeSpan.FromSeconds(gd.Duration);
            txtTime.text = $"{((int)timeSpan.TotalMinutes).ToString("00")} min {timeSpan.Seconds.ToString("00")} sec";
            txtBots.text = gd.BotsAlive.ToString().PadLeft(3);
            txtHealth.text = ((int)gd.RemainingBotHealth).ToString().PadLeft(3);
        }
    }
}


