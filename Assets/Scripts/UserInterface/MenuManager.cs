﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace AIM.Menu
{
    public class MenuManager : MonoBehaviour
    {
        [SerializeField] private GameDataDisplayer gameDataDisplayer;
        [SerializeField] private GameObject backToMenuButton;
        [SerializeField] private GameObject mainMenu;

        void Awake()
        {
            if (!gameDataDisplayer)
            {
                throw new InvalidOperationException($"{nameof(gameDataDisplayer)} is not specified!");
            }
            if (!backToMenuButton)
            {
                throw new InvalidOperationException($"{nameof(backToMenuButton)} is not specified!");
            }
            if (!mainMenu)
            {
                throw new InvalidOperationException($"{nameof(mainMenu)} is not specified!");
            }
        }

        private void Start()
        {
            Gamemaster.Instance.OnGameFinished += OnGameFinished;
        }

        private void OnGameFinished(object sender, EventArgs e)
        {
            backToMenuButton.SetActive(false);
            gameDataDisplayer.gameObject.SetActive(true);
            gameDataDisplayer.UpdateData();
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape) && !mainMenu.activeInHierarchy
                && !gameDataDisplayer.gameObject.activeInHierarchy)
            {
                backToMenuButton.SetActive(!backToMenuButton.activeInHierarchy);
            }
        }

        public void LoadGameScene() => SceneManager.LoadScene(Scenes.Game);
    }
}