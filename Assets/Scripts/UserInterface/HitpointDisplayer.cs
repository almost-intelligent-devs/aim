﻿using AIM.Boss;
using AIM.Bots;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AIM
{
    /// <summary>
    /// Instantiates and assigns a HitpointText for each Bot and the boss
    /// </summary>
    public class HitpointDisplayer : MonoBehaviour
    {
        [SerializeField]
        private GameObject hitPointCanvasPrefab;

        void Start()
        {
            BossController boss = Gamemaster.Instance.GetBoss();
            if (boss != null)
            {
                AddHitpointCanvas(boss.transform, ArenaData.BossHeight).Init(boss);
            }
            List<AIMBotController> bots = Gamemaster.Instance.GetAIMBots();

            foreach (AIMBotController bot in bots)
            {
                AddHitpointCanvas(bot.transform, ArenaData.BotHeight).Init(bot);
            }
        }

        /// <summary>
        /// Instantiates HitpointCanvas, assigns it to the respective parent and puts it above their origin
        /// </summary>
        /// <param name="transform"></param>
        /// <param name="height"></param>
        /// <returns>HitpointText of the instantiated Canvas</returns>
        HitpointText AddHitpointCanvas(Transform transform, float height)
        {
            Transform canvasTransform = Instantiate(hitPointCanvasPrefab).transform;
            canvasTransform.SetParent(transform, true);
            canvasTransform.localPosition = new Vector3(0, height / transform.localScale.y, 0);
            return canvasTransform.GetComponentInChildren<HitpointText>();
        }
    }
}
