﻿using System;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace AIM.Menu
{
    public class MainMenu : MonoBehaviour
    {
        private const string PlayerPrefWidth = "width";
        private const string PlayerPrefHeight = "height";
        private const string PlayerPrefVolume = "volume";

        private const int ResetTime = 10;

        [SerializeField] private Slider sldResolution;
        [SerializeField] private Button btnResetResolution;
        [SerializeField] private TextMeshProUGUI txtResolution;
        [SerializeField] private TextMeshProUGUI txtCountdown;
        [SerializeField] private UnityEngine.Animation slideOutAnim;

        [SerializeField] private Transform cameraTransformToMatch;

        [SerializeField] private GameObject[] activateOnStartGame;

        [SerializeField] private Slider sldVolume;

        Resolution[] resolutions;
        Resolution previousResolution;

        float resetTimer = ResetTime;
        CoroutineCancellationToken cancelReset;

        void Start()
        {
            var camController = Gamemaster.Instance.CameraController;
            camController.TransformToMatch = cameraTransformToMatch;
            camController.MatchRotation = true;


            if (PlayerPrefs.HasKey(PlayerPrefWidth) && PlayerPrefs.HasKey(PlayerPrefHeight))
            {
                if (Screen.currentResolution.width != PlayerPrefs.GetInt(PlayerPrefWidth) || Screen.currentResolution.height != PlayerPrefs.GetInt(PlayerPrefHeight))
                {
                    Screen.SetResolution(PlayerPrefs.GetInt(PlayerPrefWidth), PlayerPrefs.GetInt(PlayerPrefHeight), true);
                }
            }
            if (PlayerPrefs.HasKey(PlayerPrefVolume))
            {
                AudioListener.volume = PlayerPrefs.GetFloat(PlayerPrefVolume);
                sldVolume.value = PlayerPrefs.GetFloat(PlayerPrefVolume) * sldVolume.maxValue;
            }

            resolutions = Screen.resolutions;
            sldResolution.maxValue = resolutions.Length - 1;
            txtResolution.text = Screen.currentResolution.ToString();
            sldResolution.value = Array.IndexOf(resolutions, Screen.currentResolution);
        }

        public void LoadTutorial()
        {
            Gamemaster.Instance.HardMode = false;
            SceneManager.LoadScene("Tutorial_1");
        }

        public void StartGame(bool hardMode)
        {
            Gamemaster.Instance.HardMode = hardMode;
            slideOutAnim.Play();
            var camController = Gamemaster.Instance.CameraController;
            camController.TransformToMatch = null;
            camController.MatchRotation = false;

            foreach (GameObject go in activateOnStartGame)
                go.SetActive(true);
        }

        public void QuitGame() => Application.Quit();


        #region Options

        public void SetVolume(float volume)
        {
            AudioListener.volume = volume / sldVolume.maxValue;
            PlayerPrefs.SetFloat(PlayerPrefVolume, volume / sldVolume.maxValue);
        }

        public void SetResolutionText(float resolutionIndex) => txtResolution.text = resolutions[(int)resolutionIndex].ToString();

        public void ApplyOptionsWithReset()
        {
            previousResolution = Screen.currentResolution;
            Screen.SetResolution(resolutions[(int)sldResolution.value].width, resolutions[(int)sldResolution.value].height, true);
            PlayerPrefs.SetInt(PlayerPrefWidth, resolutions[(int)sldResolution.value].width);
            PlayerPrefs.SetInt(PlayerPrefHeight, resolutions[(int)sldResolution.value].height);

            resetTimer = ResetTime;
            cancelReset?.Cancel();
            cancelReset = new CoroutineCancellationToken();
            StartCoroutine(ApplyOrReset(cancelReset));
        }

        public void ResetResolution()
        {
            cancelReset?.Cancel();
            Screen.SetResolution(previousResolution.width, previousResolution.height, true);
            PlayerPrefs.SetInt(PlayerPrefWidth, previousResolution.width);
            PlayerPrefs.SetInt(PlayerPrefHeight, previousResolution.height);
        }

        public void IncrementResolution() => sldResolution.value++;
        public void DecrementResolution() => sldResolution.value--;

        public IEnumerator ApplyOrReset(CoroutineCancellationToken ct)
        {
            while (!ct.CancellationRequested && resetTimer > 0)
            {
                resetTimer -= Time.deltaTime;
                txtCountdown.text = "Resolution will reset in " + (int)resetTimer + " sec.";
                yield return 0;
            }
            if (resetTimer <= 0)
            {
                btnResetResolution.onClick.Invoke();
            }
        }

        public void CancelReset() => cancelReset?.Cancel();

        private void DisableSelf() => gameObject.SetActive(false);

        #endregion
    }
}
