﻿using AIM.Boss;
using AIM.Bots;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace AIM
{
    /// <summary>
    /// Displays the current hp of bots and the boss above their heads
    /// Text will rotate towards the camera and update on each HpChangedEvent
    /// </summary>
    [RequireComponent(typeof(RectTransform), typeof(Text))]
    public class HitpointText : MonoBehaviour
    {

        private RectTransform rect;
        private Text text;
        [SerializeField]
        private int bossFontSize, botFontSize;

        void Awake()
        {
            rect = this.GetComponent<RectTransform>();
            text = this.GetComponent<Text>();
        }

        void Update()
        {
            Vector3 diff = this.transform.position - Camera.main.transform.position;
            float rot = Quaternion.LookRotation(diff).eulerAngles.y;
            this.transform.rotation = Quaternion.Euler(0, rot, 0);
        }

        public void Init(BossController boss)
        {
            boss.BossHpChanged += BossUpdateHitpoints;
            text.text = "" + Mathf.Ceil(boss.HitPoints);
            text.fontSize = bossFontSize;
        }

        public void Init(AIMBotController bot)
        {
            bot.BotHpChanged += BotUpdateHitpoints;
            text.text = "" + Mathf.Ceil(bot.HitPoints);
            text.fontSize = botFontSize;
        }

        private void BossUpdateHitpoints(object sender, EventArgs args)
        {
            if (sender is BossController) {
                
                text.text = "" + Mathf.Ceil((sender as BossController).HitPoints);
            }
            else
            {
                throw new InvalidOperationException($"{nameof(HitpointText.BossUpdateHitpoints)} called from an object of type {sender.GetType()}, should be type {nameof(BossController)} instead.");
            }
        }

        private void BotUpdateHitpoints(object sender, EventArgs args)
        {
            if (sender is AIMBotController)
            {

                text.text = "" + Mathf.Ceil((sender as AIMBotController).HitPoints);
            }
            else
            {
                throw new InvalidOperationException($"{nameof(HitpointText.BotUpdateHitpoints)} called from an object of type {sender.GetType()}, should be type {nameof(AIMBotController)} instead.");
            }

        }
        
    }
}