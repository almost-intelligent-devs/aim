﻿using UnityEngine;

namespace AIM.Boss
{
    [RequireComponent(typeof(Renderer))]
    public class BouncyBallAnim : MonoBehaviour
    {
        [SerializeField] private float cycleDuration = 2;
        [SerializeField] private AnimationCurve cutoffCurve;

        private float timer = 0;
        private new Renderer renderer;

        int shaderProperty;

        void Awake()
        {
            shaderProperty = Shader.PropertyToID("_cutoff");
            renderer = GetComponent<Renderer>();
        }

        void Update()
        {
            if (timer < cycleDuration)
            {
                timer += Time.deltaTime;
            }
            else
            {
                timer = 0;
            }

            renderer.material.SetFloat(shaderProperty, cutoffCurve.Evaluate(Mathf.InverseLerp(0, cycleDuration, timer)));
        }
    }
}
