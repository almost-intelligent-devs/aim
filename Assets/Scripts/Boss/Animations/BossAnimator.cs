using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;
using AIM.PlayerInput;

namespace AIM.Boss
{
    /// <summary>
    /// Handles all animation logic of the boss
    /// </summary>
    [RequireComponent(typeof(Animator))]
    public class BossAnimator : MonoBehaviour
    {
        private Animator animator;

        [SerializeField] private Transform protectionParent;
        [SerializeField] private Transform protectionCamParent;
        [SerializeField] private Animator[] protectionAnimators = new Animator[3];

        private void Awake()
        {
            animator = GetComponent<Animator>();

            if (animator == null)
            {
                throw new InvalidOperationException($"{nameof(animator)} is not specified!");
            }
            if (protectionParent == null)
            {
                throw new InvalidOperationException($"{nameof(protectionParent)} is not specified!");
            }
            if (protectionCamParent == null)
            {
                throw new InvalidOperationException($"{nameof(protectionCamParent)} is not specified!");
            }
        }

        /// <summary> 
        /// false => standard pose; 
        /// true => head centered over base for eye beam attacks
        ///         (Note: cannot perform any other animations from this position) 
        /// </summary>
        public bool CenterHeadOverBase { set => animator.SetBool(BossAnimParameter.BEyeBeam, value); }

        public void PerformEyeSlam() => animator.SetTrigger(BossAnimParameter.TEyeSlam);
        public void PerformStomp() => animator.SetTrigger(BossAnimParameter.TStomp);

        #region Protection

        public void LowerProtection(int side) => protectionAnimators[side].SetBool(BossAnimParameter.BProtection, false);
        public void LiftProtection(int side) => protectionAnimators[side].SetBool(BossAnimParameter.BProtection, true);

        /// <summary> Plays animation that adds an additional layer of protection to boss. </summary>
        public IEnumerator PerformUpgradeAnimation()
        {
            animator.SetTrigger(BossAnimParameter.TUpgradeProtection);
            yield return new WaitUntil(() => animator.GetCurrentAnimatorStateInfo(BossAnimParameter.LBase).IsName(BossAnimParameter.NUpgradeProtection));

            protectionCamParent.gameObject.SetActive(true);

            CameraController camController = Gamemaster.Instance.CameraController;
            camController.transform.SetParent(protectionCamParent, true);
            camController.TransformToMatch = protectionCamParent;
            camController.MatchRotation = false;

            for (int i = 0; i < protectionParent.childCount; ++i)
                protectionParent.GetChild(i).gameObject.SetActive(true);
            yield return new WaitWhile(() => animator.GetCurrentAnimatorStateInfo(BossAnimParameter.LBase).IsName(BossAnimParameter.NUpgradeProtection));

            camController.transform.SetParent(null, true);
            camController.TransformToMatch = null;
        }

        #endregion

        #region LALA

        /// <summary> Lowers the boss eye for LALA </summary>
        public void StartLala() => animator.SetTrigger(BossAnimParameter.TLalaIn);
        /// <summary> Drops the boss eye to the ground to expose weak eye to attacks (Note: requires <see cref="StartLala"/> to be called prior to this) </summary>
        public void StunLala() => animator.SetTrigger(BossAnimParameter.TLalaStun);
        /// <summary> Returns boss to idle state after LALA (Note: requires <see cref="StartLala"/> to be called prior to this) </summary>
        public void StopLala() => animator.SetTrigger(BossAnimParameter.TLalaOut);

        #endregion

        #region Animation Events
        public event EventHandler OnEyeStunned;
        private void InvokeOnEyeStunned() => OnEyeStunned?.Invoke(this, null);

        /// <summary> Invoked when the eye hits the ground after calling <see cref="PerformEyeSlam"/> </summary>
        public event EventHandler OnEyeSlamHitGround;
        private void InvokeOnEyeSlamHitGround() => OnEyeSlamHitGround?.Invoke(this, null);

        /// <summary> Invoked when the boss hits the ground after calling <see cref="PerformStomp"/> </summary>
        public event EventHandler OnStompHitGround;
        private void InvokeOnStompHitGround() => OnStompHitGround?.Invoke(this, null);
        #endregion

        public IEnumerator IdleHeadUp(float idleTime)
        {
            float initialWaitTime = Random.Range(3, 5f);
            yield return new WaitForSeconds(initialWaitTime);
            CenterHeadOverBase = true;
            yield return new WaitForSeconds(Random.Range(5, idleTime - 2 * initialWaitTime));
            CenterHeadOverBase = false;
        }
    }
}
