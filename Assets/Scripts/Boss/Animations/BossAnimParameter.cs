﻿using UnityEngine;

namespace AIM.Boss
{
    /// <summary>
    /// Contains hashes for accessing parameters of the boss animation controller
    /// </summary>
    public static class BossAnimParameter
    {
        // Bools
        public static readonly int BEyeBeam = Animator.StringToHash("EyeBeam");
        public static readonly int BProtection = Animator.StringToHash("Protection");

        // Triggers
        public static readonly int TEyeSlam = Animator.StringToHash("EyeSlam");
        public static readonly int TStomp = Animator.StringToHash("Stomp");
        public static readonly int TUpgradeProtection = Animator.StringToHash("UpgradeProtection");
        public static readonly int TLalaIn = Animator.StringToHash("LalaIn");
        public static readonly int TLalaStun = Animator.StringToHash("LalaStun");
        public static readonly int TLalaOut = Animator.StringToHash("LalaOut");

        // State Names
        public const string NUpgradeProtection = "UpgradeProtection";

        // Layer Index
        public const int LBase = 0;
        public const int LProtection = 1;
    }
}
