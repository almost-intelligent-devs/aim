﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace AIM.Boss
{
    public class BouncyBallAttack : BossAttack
    {
        public override float Cooldown => 15;
        protected override float idleDelay => 5f;

        private float[] usefullness = new float[] { 30, 20, 5, 0.1f };
        private float[] hardUsefullness = new float[] { 30, 20, 10, 5, 5, 5, 1 };
        private float projectileRadius = 2f;
        private int maxAttempts = 10;
        private BossEyeAttackPerformer attackPerformer;

        public BouncyBallAttack(BossEyeAttackPerformer attackPerformer)
        {
            this.attackPerformer = attackPerformer;
        }

        /// <summary>
        /// Looks for Direction for Attack; 
        /// Usefullness depends on how many balls are currently on the field
        /// </summary>
        /// <returns></returns>
        public override float GetAttackUsefullness()
        {
            bool foundDirection = attackPerformer.TryFindBouncyBallAttackDirection(2 * projectileRadius, maxAttempts);
            if (!foundDirection)
                return 0;
            int numberBalls = attackPerformer.GetNumberOfBallsOnField();
            if (Gamemaster.Instance.HardMode)
            {
                return hardUsefullness[Math.Min(numberBalls, hardUsefullness.Length - 1)];
            }
            else
            {
                return usefullness[Math.Min(numberBalls, usefullness.Length - 1)];
            }
        }

        public override void PerformAttack()
        {
            attackPerformer.StartCoroutine(attackPerformer.PerformBouncyBallAttack(idleDelay));
        }

    }
}