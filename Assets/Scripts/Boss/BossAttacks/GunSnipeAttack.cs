﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AIM.Bots;

namespace AIM.Boss
{
    public class GunSnipeAttack : BossAttack
    {
        public override float Cooldown => 12;
        protected override float idleDelay => lowIdle;

        private float aimDuration = 6;
        private float baseUsefullness = 15;
        private float projectileRange = 30f;

        private BossGunAttackPerformer attackPerformer;
        public GunSnipeAttack(BossGunAttackPerformer attackPerformer)
        {
            this.attackPerformer = attackPerformer;
        }


        /// <summary>
        /// Depends on ratio of Current HP to maxHP of weakest bot
        /// </summary>
        /// <returns></returns>
        public override float GetAttackUsefullness()
        {
            float hitpoints = attackPerformer.GetLowestTargetHP(projectileRange);

            if (hitpoints <= 0)//if no bot is found
                return 0;

            float HitpointPercentage = hitpoints / 100f; //to Do change 100f to static Bot.MaxHP value
            return baseUsefullness + (100f - baseUsefullness) * (1 - HitpointPercentage);
        }

        public override void PerformAttack()
        {
            attackPerformer.StartCoroutine(attackPerformer.PerformSnipeAttack(aimDuration, projectileRange, idleDelay));
        }
    }
}