﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AIM.DangerArea
{
    [RequireComponent(typeof(MeshFilter))]
    public class DangerAreaTriangle : MonoBehaviour
    {
        Mesh mesh;
        Vector3[] vertices;
        int[] triangles;

        void Awake()
        {
            mesh = GetComponent<MeshFilter>().mesh;
        }

        #region Meshcreation
        /// <summary>
        /// Creates triangle Mesh for danger area plane, for an attack
        /// </summary>
        /// <param name="startingPoint">StartPoint of the attack</param>
        /// <param name="direction">Direction of the attack</param>
        /// <param name="radius">Range of the attack</param>
        /// <param name="angle">Overall angle of the attack(from Startpoint half of angle to the right and to the left)</param>
        public void CreateMesh(Vector3 startingPoint, Vector3 direction, float radius, float angle)
        {
            this.transform.position = new Vector3(startingPoint.x, ArenaData.GroundHeight + 0.01f, startingPoint.z); //
            direction.y = 0;
            direction = direction.normalized * radius;

            Vector3 point0 = Vector3.zero;
            Vector3 point1 = Quaternion.Euler(0, -angle / 2f, 0) * direction;
            Vector3 point2 = Quaternion.Euler(0, angle / 2f, 0) * direction;
            vertices = new Vector3[] { point0, point1, point2 };
            triangles = new int[] { 0, 1, 2 };

            UpdateMesh();
        }

        void UpdateMesh()
        {
            mesh.Clear();
            mesh.vertices = vertices;
            mesh.triangles = triangles;
        }
        #endregion

    }
}