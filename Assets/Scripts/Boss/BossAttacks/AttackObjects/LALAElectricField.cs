﻿using AIM.Bots;
using AIM.PlayerInput;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AIM.Bots.AI;
using static UnityEngine.ParticleSystem;

namespace AIM.Boss
{
    [RequireComponent(typeof(Renderer), typeof(Collider))]
    public class LALAElectricField : MonoBehaviour
    {
        [SerializeField]
        private GameObject electricParticleSystem;
        public InputHandler MyInput;
        private List<ParticleSystem> particleSystems = new List<ParticleSystem>();
        private const float defaultXScale = 0.2f;

        void Start()
        {
            GenerateParticleSystems();
        }

        private void GenerateParticleSystems()
        {
            Quaternion rot = this.transform.rotation;
            this.transform.rotation = Quaternion.identity;
            float x = this.transform.localScale.x;
            float z = this.transform.localScale.z;
            float steps = (z / x);
            float pos = -steps / z + 1 / z;
            steps *= 2;
            while (steps > 0)
            {
                GameObject particleSystem = Instantiate(electricParticleSystem);

                particleSystem.transform.parent = this.transform;
                particleSystem.transform.localPosition = new Vector3(0, 0, pos * x / defaultXScale);
                particleSystem.transform.localScale = new Vector3(0.1f / defaultXScale, 0.1f, x / z * 0.1f / defaultXScale);
                particleSystems.Add(particleSystem.GetComponent<ParticleSystem>());
                pos += 1f / z;
                steps--;
            }
            this.transform.rotation = rot;
            this.GetComponent<Renderer>().enabled = false;
        }

        /// <summary>
        /// When a bot touches the electrical field they will stop doing anything an become uncontrolable
        /// </summary>
        /// <param name="other"></param>
        private void OnTriggerEnter(Collider other)
        {
            if (other.tag == TagDictionary.Bot)
            {
                AIMBotController bot = other.GetComponent<AIMBotController>();
                BossController boss = Gamemaster.Instance.GetBoss();
                if (bot.ControlledByPlayer)
                {
                    Gamemaster.Instance.GetInputHandler().GoToSelectMode();
                }
                if (!bot.ControlBlocked)
                {
                    boss.ReleaseControlBlock += bot.OnReleaseControlBlock;
                    bot.ControlBlocked = true;
                    if (!bot.CurrentBehaviour.IsMalicious())
                    {
                        bot.SetAIBehaviour(new StareAtPointBehaviour(bot, this.transform));
                    }
                }
            }
        }

        /// <summary>
        /// Turn off the particle emission for a shutdown effect
        /// </summary>
        /// <returns>Time needed until all particles are gone</returns>
        public float StartDestruction()
        {
            foreach(ParticleSystem ps in particleSystems)
            {
                EmissionModule emission = ps.emission;
                emission.rateOverTime = 0;
            }
            this.GetComponent<Collider>().enabled = false;
            return particleSystems[0].main.startLifetime.constant;
        }

    }
}