﻿using AIM.Bots.AI;
using AIM.Interactables;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AIM.Boss
{
    public class Pillar : Interactable, IPunchable
    {
        [SerializeField]
        private ParticleSystem[] particleSystems;

        private const int TargetHeight = 0;
        private const int AppearSpeed = 2;

        private ElectricAttackPerformer attackPerformer;

        private bool activated;

        private void Awake()
        {
            particleSystems = GetComponentsInChildren<ParticleSystem>();
            if (particleSystems == null || particleSystems.Length == 0)
            {
                throw new InvalidOperationException($"{nameof(Pillar)} is missing particle systems!");
            }
        }

        protected override void Start()
        {
            base.Start();
            attackPerformer = Gamemaster.Instance.GetBoss().GetComponent<ElectricAttackPerformer>();
            StartCoroutine(AppearAnimation());
        }

        /// <summary>
        /// Activate Pillar on Hit
        /// </summary>
        public void Activate()
        {
            if (activated)
                return;
            activated = true;
            foreach(ParticleSystem ps in particleSystems)
            {
                var emission = ps.emission;
                emission.enabled = false;
            }
            attackPerformer.PillarHit();
        }

        /// <summary>
        /// Deactivate after timeout
        /// </summary>
        public void Deactivate()
        {
            if (!activated)
                return;
            activated = false;
            foreach (ParticleSystem ps in particleSystems)
            {
                var emission = ps.emission;
                emission.enabled = true;
            }
        }

        private IEnumerator AppearAnimation()
        {
            while (this.transform.position.y < TargetHeight)
            {
                this.transform.position += new Vector3(0, AppearSpeed * Time.deltaTime, 0);
                yield return new WaitForEndOfFrame();
            }
        }

        public override void Interact(GameObject interactor)
        {
            Activate();
        }


        #region Bots AI utils
        private List<AttackToken> attackTokens;
        public List<AttackToken> GetAttackTokens()
        {
            if (attackTokens == null)
            {
                attackTokens = AttackToken.GenerateTokensAroundCollider(this.transform, GetComponent<Collider>(), ArenaData.AgentRadius / 2f, .2f);
            }
            return attackTokens;
        }


        private void OnDrawGizmos()
        {
            var tokens = GetAttackTokens();
            foreach (var token in tokens)
            {
                if (token.State == TokenState.Open)
                {
                    Gizmos.DrawWireSphere(token.Position, .1f);
                }
                else
                {
                    Gizmos.DrawSphere(token.Position, .1f);
                }
            }
        }

        #endregion
    }
}
