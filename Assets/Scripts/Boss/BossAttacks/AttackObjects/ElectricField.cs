﻿using AIM.Bots;

using System;
using AIM.Sound;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
namespace AIM.Boss
{
    public class ElectricField : MonoBehaviour
    {

        [SerializeField]
        private float slowGrowthSpeed, mediumGrowthSpeed, fastGrowthSpeed;
        [SerializeField]
        private float innerPercentile, innerDamage, mediumDamage, outerDamage, outerPercentile;
        [SerializeField]
        private Transform middleField, innerField;

        [SerializeField]
        protected SoundsPlayer soundsPlayer;

        private float growthSpeed;
        private float currentSize;

        public event EventHandler FieldDestroyed;

        private void Start()
        {
            growthSpeed = slowGrowthSpeed;
            StartCoroutine(UpdateGrowthSpeed());
            currentSize = this.transform.localScale.x;
            if (Gamemaster.Instance.HardMode)
            {
                outerPercentile *= 1.5f;
                innerPercentile *= 1.5f;
            }
                middleField.localScale = new Vector3(outerPercentile, 1, outerPercentile);
            innerField.localScale = new Vector3(innerPercentile, 1, innerPercentile);
        }

        private void Update()
        {
            if (currentSize < 2 * ArenaData.ArenaRadius)
            {
                float growth = growthSpeed * Time.deltaTime;
                currentSize += growth;
                this.transform.localScale = new Vector3(currentSize, 0.1f, currentSize);
            }
            soundsPlayer.PlayIfEmpty(SharedSoundsMaster.Instance.BossWeapons.GetClip(BossWeaponSounds.ElectricField));
        }

        /// <summary>
        /// Increase growthSpeed in 2 steps
        /// </summary>
        /// <returns></returns>
        private IEnumerator UpdateGrowthSpeed()
        {
            while (currentSize < 2 * ArenaData.ArenaRadius)
            {
                if (growthSpeed == slowGrowthSpeed && currentSize > 0.5f * ArenaData.ArenaRadius)
                {
                    growthSpeed = mediumGrowthSpeed;
                }
                else if (growthSpeed == fastGrowthSpeed && currentSize > 1.5f * ArenaData.ArenaRadius)
                {
                    growthSpeed = fastGrowthSpeed;
                    yield break;
                }
                yield return new WaitForEndOfFrame();
            }
        }

        /// <summary>
        /// Bots withing Electricfield take damage depending on how far to the center they are
        /// </summary>
        /// <param name="other"></param>
        private void OnTriggerStay(Collider other)
        {
            if (other.tag == TagDictionary.Bot)
            {
                float distance = Mathf.Sqrt((other.transform.position - this.transform.position).sqrMagnitude);
                float relativeDistance = distance / (currentSize / 2);

                float damage = outerDamage;
                if (relativeDistance < innerPercentile)
                    damage = innerDamage;
                else if (relativeDistance < outerPercentile)
                    damage = mediumDamage;
                other.GetComponent<AIMBotController>().ChangeHP(-damage * Time.fixedDeltaTime, this.gameObject);
            }
        }

        private void OnDestroy()
        {
            FieldDestroyed?.Invoke(this, EventArgs.Empty);
        }


    }
}