﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AIM.DangerArea
{
    [RequireComponent(typeof(MeshFilter))]
    public class DangerAreaLALA : MonoBehaviour
    {
        private Mesh mesh;
        private Vector3[] vertices;
        private int[] mainTriangles, chargedTriangles;

        void Awake()
        {
            mesh = GetComponent<MeshFilter>().mesh;
        }

        #region Meshcreation
        /// <summary>
        /// Creates Mesh for danger area plane, for an attack
        /// </summary>
        /// <param name="startingPoint">StartPoint of the attack</param>
        /// <param name="direction">Direction of the attack</param>
        /// <param name="radius">Range of the attack</param>
        /// <param name="angle">Overall angle of the attack(from Startpoint half of angle to the right and to the left)</param>
        /// <param name="side">The side which the LALA will be fired to - determines corners of DangerArea</param>
        public void CreateMesh(Vector3 startingPoint, Vector3 direction, float radius, float angle, int side)
        {
            this.transform.position = new Vector3(startingPoint.x, ArenaData.GroundHeight + 0.01f, startingPoint.z);
            direction.y = 0;
            direction = direction.normalized * radius;

            float x = ArenaData.ArenaRadius;
            float z = ArenaData.ArenaRadius;
            Vector3 point0 = Vector3.zero;
            Vector3 point1 = Quaternion.Euler(0, -angle / 2f, 0) * direction;
            Vector3 point2 = new Vector3(0, 0, -z);
            Vector3 point3 = Quaternion.Euler(0, angle / 2f, 0) * direction;
            if (side == 0)
            {
                point3 = new Vector3(-x, 0, point3.z / point3.x * -x);
                point1 = new Vector3(x, 0, point1.z / point1.x * x);
            }
            if (side == 1)
            {
                point1 = new Vector3(-x, 0, point1.z / point1.x * -x);
                point2 = new Vector3(-x, 0, z);
                point3 = new Vector3(point3.x / point3.z * z, 0, z);
            }
            else if (side == 2)
            {
                point1 = new Vector3(point1.x / point1.z * z, 0, z);
                point2 = new Vector3(x, 0, z);
                point3 = new Vector3(x, 0, point3.z / point3.x * x);
            }
            //point0=origin; point1-3=complete mesh endpoints; point4-6=charged up mesh endpoints
            vertices = new Vector3[] { point0, point1, point2, point3, point0, point0, point0 };
            mainTriangles = new int[] { 0, 1, 2, 0, 2, 3 };
            chargedTriangles = new int[] { 0, 4, 5, 0, 5, 6 };
            UpdateMesh();
        }

        /// <summary>
        /// Apply Changes to Danger Area Mesh
        /// </summary>
        private void UpdateMesh()
        {
            mesh.Clear();
            mesh.subMeshCount = 2;
            mesh.vertices = vertices;
            mesh.SetTriangles(mainTriangles, 0);
            mesh.SetTriangles(chargedTriangles, 1);
        }

        /// <summary>
        /// Show charging up LALA attack
        /// </summary>
        /// <param name="chargeTime"></param>
        /// <returns></returns>
        public IEnumerator ChargeDangerArea(float chargeTime)
        {
            float time = 0f;
            while (time < chargeTime)
            {
                yield return new WaitForEndOfFrame();
                time += Time.deltaTime;
                SetChargedPoints(time / chargeTime);
            }
            SetChargedPoints(1);
        }

        /// <summary>
        /// Assigns new positions to the charged up mesh points and apply changes to mesh
        /// </summary>
        /// <param name="chargeProgress">Percentage of how much LALA is charged</param>
        private void SetChargedPoints(float chargeProgress)
        {
            for (int i = 1; i < 4; i++)
            {
                vertices[i + 3] = vertices[i] * chargeProgress;
            }
            UpdateMesh();
        }
        #endregion

    }
}