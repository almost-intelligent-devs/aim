﻿using System.Collections;
using System.Collections.Generic;
using AIM.Sound;
using UnityEngine;
namespace AIM.Boss
{
    public class SawbladeSide : MonoBehaviour
    {
        [SerializeField] protected SoundsPlayer soundsPlayer;

        void Start()
        {
            soundsPlayer.PlaySound(SharedSoundsMaster.Instance.BossWeapons.GetClip(BossWeaponSounds.SawBlades), true);
        }
    }
}
