﻿using AIM.Bots;
using System;
using UnityEngine;

namespace AIM.Boss
{
    public class Sawblade : MonoBehaviour
    {
        private const float rotationSpeed = 770f;

        [SerializeField] private float damagePerHit;
        [SerializeField] private Transform mesh;
        [SerializeField] private ParticleSystem psSparks;
        [SerializeField] private Transform sparkCollisionPlane;

        private void Awake()
        {
            if (!mesh)
            {
                throw new InvalidOperationException($"{nameof(mesh)} is not specified!");
            }
            if (!psSparks)
            {
                throw new InvalidOperationException($"{nameof(psSparks)} is not specified!");
            }
            if (!sparkCollisionPlane)
            {
                throw new InvalidOperationException($"{nameof(sparkCollisionPlane)} is not specified!");
            }

            var pos = sparkCollisionPlane.position;
            pos.y = 0f;
            sparkCollisionPlane.position = pos;
        }

        void Update()
        {
            mesh.Rotate(0, 0, rotationSpeed * Time.deltaTime);
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.tag == TagDictionary.Bot)
            {
                AIMBotController bot = other.gameObject.GetComponent<AIMBotController>();
                bot.ChangeHP(-damagePerHit, Gamemaster.Instance.GetBoss().gameObject);

                psSparks.Play();
            }
        }

        private void OnTriggerExit(Collider other)
        {
            psSparks.Stop();
        }
    }
}
