﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AIM.Bots.AI;

namespace AIM.Boss
{
    public class DoNothingBeamAttack : CorruptionBeamAttack
    {
        public override float Cooldown => 40;
        protected override float idleDelay => lowIdle;

        private const float corruptionDuration = 2f;
        private const float EffectDuration = 10f;

        public DoNothingBeamAttack(BossEyeAttackPerformer attackPerformer) : base(attackPerformer) { }

        public override void PerformAttack()
        {
            attackPerformer.StartCoroutine(attackPerformer.PerformCorruptionBeamAttack(corruptionBeamTarget, new StareAtPointBehaviour(corruptionBeamTarget, Gamemaster.Instance.GetBoss().transform, EffectDuration), corruptionDuration, idleDelay));
        }

    }
}
