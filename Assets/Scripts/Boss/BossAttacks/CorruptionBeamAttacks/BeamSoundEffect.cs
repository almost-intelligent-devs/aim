﻿using System.Collections;
using System.Collections.Generic;
using AIM.Sound;
using UnityEngine;

namespace AIM.Boss
{
    public class BeamSoundEffect : MonoBehaviour
    {
        [SerializeField]
        protected SoundsPlayer soundsPlayer;

        void Update()
        {
            soundsPlayer.PlayIfEmpty(SharedSoundsMaster.Instance.BossWeapons.GetClip(BossWeaponSounds.EyeBeam));
        }
    }
}
