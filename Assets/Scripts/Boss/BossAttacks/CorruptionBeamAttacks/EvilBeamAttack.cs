﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AIM.Bots.AI;
using AIM.Bots;
namespace AIM.Boss
{
    public class EvilBeamAttack : CorruptionBeamAttack
    {
        public override float Cooldown
        {
            get
            {
                if (Gamemaster.Instance.HardMode)
                    { return 30; }
                else
                    { return 60; }
            }
        }
        protected override float idleDelay => mediumIdle;

        private const float corruptionDuration = 5f;
        private const float EffectDuration = 30f;
        private const float damageTime = 30;
        private const float maxDamage = 300;

        public EvilBeamAttack(BossEyeAttackPerformer attackPerformer) : base(attackPerformer) { }

        public override float GetAttackUsefullness()
        {
            float baseUsefullness = GetCorruptionBeamBaseUsefullness();
            float damage = Gamemaster.Instance.GetBoss().GetDamageTaken(damageTime);
            float damagePercent = Mathf.Min(damage / maxDamage, 1);
            float damageUsefullness = (1 - baseUsefullness / 100f) * damagePercent * 100f; // 90%
            return baseUsefullness + damageUsefullness;
        }

        public override void PerformAttack()
        {
            attackPerformer.StartCoroutine(attackPerformer.PerformCorruptionBeamAttack(corruptionBeamTarget, new EvilBehaviour(corruptionBeamTarget, EffectDuration), corruptionDuration, idleDelay));
        }
    }
}
