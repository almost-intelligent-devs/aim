﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AIM.Bots;
using System.Linq;

namespace AIM.Boss
{
    public abstract class CorruptionBeamAttack : BossAttack
    {
        protected BossEyeAttackPerformer attackPerformer;
        protected AIMBotController corruptionBeamTarget;
        private float baseUsefullness = 10f;

        public CorruptionBeamAttack(BossEyeAttackPerformer attackPerformer)
        {
            this.attackPerformer = attackPerformer;
        }

        public override float GetAttackUsefullness()
        {
            float usefullness = GetCorruptionBeamBaseUsefullness();
            return usefullness;
        }

        /// <summary>
        /// Chooses a random bot who is not corrupted
        /// </summary>
        /// <returns></returns>
        protected float GetCorruptionBeamBaseUsefullness()
        {
            List<AIMBotController> bots = Gamemaster.Instance.GetAIMBots().Where(bot => bot.Alive && !bot.Corrupted).ToList();
            if (!bots.Any())
            {
                corruptionBeamTarget = null;
                return 0;
            }
            int randomChoice = UnityEngine.Random.Range(0, bots.Count);
            corruptionBeamTarget = bots[randomChoice];
            return baseUsefullness;
        }
    }
}