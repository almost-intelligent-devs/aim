﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AIM.Boss
{
    public class LALAAttack : BossAttack
    {
        public override float Cooldown => 9999f;//only once per game

        protected override float idleDelay => 5f;

        private float damage = 30f;
        private float chargeTime = 10f;
        private float stunDuration = 20f;

        private BossEyeAttackPerformer attackPerformer;

        public LALAAttack(BossEyeAttackPerformer attackPerformer)
        {
            this.attackPerformer = attackPerformer;
        }

        /// <summary>
        /// LALA attackUsefullness depends on the number of bots affected
        /// Usefullness is zero when hp over 50 percent or electric field is active or boss is not in center
        /// </summary>
        /// <returns></returns>
        public override float GetAttackUsefullness()
        {
            if (attackPerformer.ElectricFieldActive() || !attackPerformer.InCenter())
                return 0;
            int botsAffected = attackPerformer.GetMaxBotsPerSide(); 
            float currentHpPercentage = Gamemaster.Instance.GetBoss().HitPoints / BossController.MaxHP;
            if (currentHpPercentage > 0.5f)
                return 0;
            else
                return 30 * botsAffected;
        }

        public override void PerformAttack()
        {
            attackPerformer.StartCoroutine(attackPerformer.PerformLALA(damage, chargeTime, stunDuration, idleDelay));
        }
    }
}