﻿using AIM.Bots;
using AIM.DangerArea;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AIM.Projectile;
using AIM.Sound;

namespace AIM.Boss
{
    public class BossGunAttackPerformer : BossAttackPerformer
    {
        [SerializeField]
        private GameObject gun, projectileSpawn, baseRotator;

        [SerializeField]
        private GameObject triangleDangerAreaPrefab, laserPrefab;

        [SerializeField]
        private GameObject normalProjectilePrefab, sniperProjectilePrefab;

        [SerializeField]
        private float shootCooldownMin, shootCooldownMax;

        private float rotationSpeed = 30;
        private const float baseRotatorSpeed = 100;

        private float currentShootCooldown;

        #region GunSpreadShotAttack
        private AIMBotController spreadShotTarget;

        /// <summary>
        /// Finds target which would include most bot in effect area and returns how many bots will be hit
        /// </summary>
        /// <param name="spreadAngle"></param>
        /// <param name="range"></param>
        /// <returns>Number of bots affected by attack</returns>
        public int GetNumberOfHitBots(float spreadAngle, float range)
        {
            int maxHittableBots = 0;
            AIMBotController targetBot = null;
            List<AIMBotController> targets = FindBotsInRange(projectileSpawn, range);
            foreach (AIMBotController curBot in targets)
            {
                Vector3 targetDirection = curBot.transform.position - projectileSpawn.transform.position;
                targetDirection.y = 0;
                int hittableBots = 0;
                foreach (AIMBotController otherBot in targets)
                {
                    Vector3 botDirection = otherBot.transform.position - projectileSpawn.transform.position;
                    botDirection.y = 0;
                    float angle = Vector3.Angle(targetDirection.normalized, botDirection.normalized);
                    if (angle < spreadAngle / 2f)
                        hittableBots++;
                }

                if (hittableBots > maxHittableBots)
                {
                    maxHittableBots = hittableBots;
                    targetBot = curBot;
                }
            }
            spreadShotTarget = targetBot;
            return maxHittableBots;
        }

        /// <summary>
        /// Does a spreadshoot attack centered around target bot
        /// </summary>
        /// <param name="spreadAngle">Rotation angle of the gun centered around closest AIM bot</param>
        /// <param name="rotationRounds">Number of times gun rotates the full spreadAngle</param>
        /// <param name="range">Distance bullets can fly</param>
        /// <returns></returns>
        ///
        public IEnumerator PerformSpreadShotAttack(float startDelay, float spreadAngle, int rotationRounds, float range, float idleDelay)
        {
            //Rotate gun so it looks at position of the closest bot at the start of the coroutine
            if (spreadShotTarget == null)
            {
                InvokeAttackFinished();
                yield break;
            }

            soundsPlayer.PlaySound(soundsProvider.GetClip(BossSounds.SpreadShot), false);

            yield return StartCoroutine(RotateTo(baseRotator.transform, spreadShotTarget.transform.position - baseRotator.transform.position, baseRotatorSpeed));
            if (attackToken.CancellationRequested) { yield break; }
            yield return StartCoroutine(LookAtBot(gun.transform, projectileSpawn.transform, spreadShotTarget, rotationSpeed));
            if (attackToken.CancellationRequested) { yield break; }

            //Display the dangerArea of the Attack
            GameObject dangerArea = Instantiate(triangleDangerAreaPrefab);
            Vector3 direction = spreadShotTarget.transform.position - projectileSpawn.transform.position;
            direction.y = 0;
            dangerArea.GetComponent<DangerAreaTriangle>().CreateMesh(projectileSpawn.transform.position, direction, range, spreadAngle);
            yield return new WaitForSeconds(startDelay);
            //Do the attack loop
            for (int i = 0; i < rotationRounds; i++)
            {
                float currentAngle = 0;
                while (currentAngle <= spreadAngle / (i == 0 ? 2f : 1f))
                {
                    if (attackToken.CancellationRequested) { yield break; }
                    if (currentShootCooldown < 0)
                        Shoot(normalProjectilePrefab);
                    else
                        currentShootCooldown -= Time.deltaTime;
                    gun.transform.Rotate(0, (i % 2 == 0 ? 1 : -1) * rotationSpeed * Time.deltaTime, 0);
                    currentAngle += rotationSpeed * Time.deltaTime;
                    yield return new WaitForEndOfFrame();
                }
            }

            Destroy(dangerArea.gameObject);
            InvokeAttackFinished(idleDelay);
        }
        #endregion

        #region GunSnipeAttack
        AIMBotController snipeAttackTarget;

        /// <summary>
        /// Finds target with the lowest HP and returns it HP value
        /// </summary>
        /// <param name="range"></param>
        /// <returns>HP of target</returns>
        public float GetLowestTargetHP(float range)
        {
            List<AIMBotController> bots = FindBotsInFOV(gun.transform.position);
            snipeAttackTarget = FindLowestHPBot(bots);
            if (snipeAttackTarget == null)
                return 0;
            return snipeAttackTarget.HitPoints;
        }

        /// <summary>
        /// Aims at lowest HP bot and shoots a single strong and fast bullet at them
        /// </summary>
        /// <param name="aimDuration">Duration boss aims at target bot(Time for player actions)</param>
        /// <param name="range">Range of sniping projectile</param>
        /// <returns></returns>
        public IEnumerator PerformSnipeAttack(float aimDuration, float range, float idleDelay)
        {
            if (snipeAttackTarget == null)
            {
                InvokeAttackFinished();
                yield break;
            }


            yield return StartCoroutine(RotateTo(baseRotator.transform, snipeAttackTarget.transform.position - baseRotator.transform.position, baseRotatorSpeed));
            yield return StartCoroutine(LookAtBot(gun.transform, projectileSpawn.transform, snipeAttackTarget, rotationSpeed));
            soundsPlayer.PlaySound(soundsProvider.GetClip(BossSounds.Snipe), false);
            //Aim at target for aimDuration seconds
            currentShootCooldown = aimDuration;
            GameObject laser = Instantiate(laserPrefab);
            SetLayerToBoss(laser);
            int layermask = ~LayerMask.GetMask(LayerDictionary.Boss);
            while (currentShootCooldown > 0 && !attackToken.CancellationRequested)
            {

                //Display laser which stops at first collision
                Vector3 direction = snipeAttackTarget.transform.position - projectileSpawn.transform.position;
                direction.y = 0;
                RaycastHit rayInfo;
                
                //if target AIM bot moves out of range => cancels attack
                if (!Physics.Raycast(projectileSpawn.transform.position, direction, out rayInfo, range, layermask) || !snipeAttackTarget.Alive)
                {
                    InvokeAttackFinished();
                    Destroy(laser);
                    yield break;
                }

                GameObject hitTarget = rayInfo.collider.gameObject;
                Quaternion lookRotation = Quaternion.LookRotation(direction, Vector3.up);

                //Rotate gun so it looks at target
                baseRotator.transform.rotation = lookRotation;
                gun.transform.rotation = lookRotation;

                float laserDistance = Mathf.Sqrt((hitTarget.transform.position - projectileSpawn.transform.position).sqrMagnitude) / 2;
                laser.transform.position = projectileSpawn.transform.position;
                laser.transform.rotation = lookRotation;
                laser.transform.Rotate(new Vector3(0, 90, 90));//Rotation correction for Cylinder
                laser.transform.GetChild(0).transform.localScale = new Vector3(0.1f, laserDistance, 0.1f);
                laser.transform.GetChild(0).transform.localPosition = new Vector3(0, laserDistance, 0);

                currentShootCooldown -= Time.deltaTime;
                yield return new WaitForEndOfFrame();
            }
            Shoot(sniperProjectilePrefab);
            Destroy(laser);
            InvokeAttackFinished(idleDelay);
        }
        #endregion

        #region Helper
        private void Shoot(GameObject projectilePrefab)
        {
            GameObject projectile = Instantiate(projectilePrefab);
            SetLayerToBoss(projectile);
            projectile.GetComponent<NormalProjectile>().Source = Gamemaster.Instance.GetBoss().GetDamageSource();
            projectile.transform.position = projectileSpawn.transform.position;
            projectile.transform.rotation = gun.transform.rotation;
            currentShootCooldown = Random.Range(shootCooldownMin, shootCooldownMax);
            if (Gamemaster.Instance.HardMode)
            {
                currentShootCooldown *= GunSpreadshotAttack.HardModeShootDelayMultiplier;
            }
            var shootSound = projectilePrefab == sniperProjectilePrefab ? SharedSoundsMaster.Instance.Weapons.GetClip(WeaponSounds.SniperPew) : SharedSoundsMaster.Instance.Weapons.GetClip(WeaponSounds.RiflePew);
            soundsPlayer.PlayOneShot(shootSound, 1);
        }
        #endregion
    }
}
