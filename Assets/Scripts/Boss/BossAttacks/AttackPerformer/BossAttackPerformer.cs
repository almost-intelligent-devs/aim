﻿using AIM.Bots;
using AIM.Sound;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace AIM.Boss
{
    [RequireComponent(typeof(BossAnimator))]
    public abstract class BossAttackPerformer : MonoBehaviour
    {
        protected BossAnimator animator;
        protected SoundsPlayer soundsPlayer;
        protected BossSoundsProvider soundsProvider;
        protected CoroutineCancellationToken attackToken = new CoroutineCancellationToken();
        private const float RotationTimeout = 10f;
        #region events
        public event EventHandler AttackFinished;
        /// <summary>
        /// AttackFinished Event => Choose next Attack
        /// </summary>
        /// <param name="idleDelay">Idle time depends on attack(default 3seconds if attackTarget not found</param>
        protected void InvokeAttackFinished(float idleDelay = 3f)
        {
            AttackFinished?.Invoke(this, new BossAttackFinishedEventArgs(idleDelay));
        }
        #endregion

        virtual protected void Awake()
        {
            soundsProvider = GetComponentInChildren<BossSoundsProvider>();
            soundsPlayer = GetComponentInChildren<SoundsPlayer>();
            if(soundsProvider == null)
            {
                throw new InvalidOperationException($"{nameof(soundsProvider)} component could not be found!");
            }
            if (soundsPlayer == null)
            {
                throw new InvalidOperationException($"{nameof(soundsPlayer)} component could not be found!");
            }
            animator = GetComponent<BossAnimator>();
            GetComponent<BossController>().BossDied += OnBossDied;
        }

        public bool ElectricFieldActive()
        {
            return this.GetComponent<ElectricAttackPerformer>().ElectricField != null;
        }

        public bool InCenter()
        {
            return this.GetComponent<BossController>().GetPositionPoint() == null;
        }

        /// <summary>
        /// Find all bots within Range
        /// </summary>
        /// <param name="startingPoint">Relative point to measure distance to point</param>
        /// <param name="range"></param>
        /// <returns></returns>
        protected List<AIMBotController> FindBotsInRange(GameObject startingPoint, float range)
        {
            var bots = Gamemaster.Instance.GetAIMBots().Where(bot => bot.Alive && Get2DDistance(startingPoint, bot.gameObject) <= range);
            return bots.ToList();
        }

        /// <summary>
        /// Find the closest alive AIM bot to the Boss within range
        /// </summary>
        /// <param name="startingPoint">Relative point to measure distance to point</param>
        /// <param name="range"></param>
        /// <returns>Closest alive AIM Bot</returns>
        protected AIMBotController FindClosestBot(GameObject startingPoint, float range)
        {
            List<AIMBotController> bots = FindBotsInRange(startingPoint, range);
            if (!bots.Any())
                return null;
            var min = bots.Min(bot => Get2DDistance(startingPoint, bot.gameObject));
            AIMBotController closestBot = bots.Where(bot => Get2DDistance(startingPoint, bot.gameObject) == min).FirstOrDefault();
            return closestBot;
        }

        /// <summary>
        /// Find Bots with direct line of sight to attack origin
        /// </summary>
        /// <param name="origin">origin for the raycasts</param>
        /// <returns>List of bots within Line of sight</returns>
        protected List<AIMBotController> FindBotsInFOV(Vector3 origin, bool throughBots = false)
        {
            List<AIMBotController> botsInFOV = new List<AIMBotController>();

            List<AIMBotController> bots = Gamemaster.Instance.GetAIMBots();
            foreach (AIMBotController bot in bots)
            {
                if (RayHit(bot, origin, throughBots))
                {
                    botsInFOV.Add(bot);
                }
            }
            return botsInFOV;
        }

        private bool RayHit(AIMBotController bot, Vector3 origin, bool throughBots)
        {
            RaycastHit hit;
            int layermask = ~LayerMask.GetMask(LayerDictionary.Boss);
            Vector3 dir = (bot.transform.position - origin).normalized;
            dir.y = 0;
            if (Physics.Raycast(origin, dir, out hit, 9999, layermask))
            {
                if (hit.collider.gameObject == bot.gameObject)
                {
                    return true;
                }
                else if (throughBots && hit.collider.gameObject.tag == TagDictionary.Bot)
                {
                    Debug.Log("bot behind bot");
                    return RayHit(bot, origin + 5 * dir, throughBots);
                }
            }
            return false;
        }
        /// <summary>
        /// Find the AIM bot with the lowest HP in the given List of bots
        /// </summary>
        /// <param name="bots">List of bots to choose from</param>
        /// <returns></returns>
        protected AIMBotController FindLowestHPBot(List<AIMBotController> bots)
        {
            if (!bots.Any())
                return null;
            var min = bots.Min(bot => bot.GetComponent<AIMBotController>().HitPoints);
            AIMBotController lowestHPBot = bots.Where(bot => bot.GetComponent<AIMBotController>().HitPoints == min).FirstOrDefault();
            return lowestHPBot;
        }

        /// <summary>
        /// Ignores Y component and returns distance of XZ Plane
        /// </summary>
        /// <param name="obj1"></param>
        /// <param name="obj2"></param>
        /// <returns></returns>
        protected float Get2DDistance(GameObject obj1, GameObject obj2)
        {
            Vector3 difference = obj1.transform.position - obj2.transform.position;
            difference.y = 0;
            return Mathf.Sqrt(difference.sqrMagnitude);
        }

        /// <summary>
        /// Calculates the number of bots within a box Area
        /// </summary>
        /// <param name="cornerPoint">bottom left corner of the box Area</param>
        /// <param name="forward"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <returns></returns>
        protected int GetNumberBotsInBox(Vector3 cornerPoint, Vector3 forward, float width, float height)
        {
            int botNumber = 0;
            cornerPoint.y = 0;
            forward = forward.normalized;
            Vector3 right = Quaternion.Euler(0, 90, 0) * forward;
            List<AIMBotController> bots = Gamemaster.Instance.GetAIMBots();

            foreach (AIMBotController bot in bots)
            {
                Vector3 botPosition = bot.transform.position;
                botPosition.y = 0;
                float forwardDistance = Vector3.Dot((bot.transform.position - cornerPoint), forward);
                float rightDistance = Vector3.Dot((bot.transform.position - cornerPoint), right);
                if (bot.Alive && 0 < forwardDistance && forwardDistance < height
                    && 0 < rightDistance && rightDistance < width)
                {
                    botNumber++;
                }
            }

            return botNumber;
        }

        protected List<AIMBotController> GetBotsInAngle(Vector3 startPosition, Vector3 direction, float angle)
        {
            List<AIMBotController> bots = Gamemaster.Instance.GetAIMBots();
            return bots.Where(bot => Mathf.Abs(Mathf.Acos(Vector3.Dot((bot.transform.position - startPosition).normalized, direction.normalized)) * 180 / Mathf.PI) < angle / 2f).ToList<AIMBotController>();
        }

        /// <summary>
        /// Rotates an object with rotationSpeed so its forward(z)-directions looks at the given direction
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="direction"></param>
        /// <param name="rotationSpeed"></param>
        /// <returns></returns>
        protected IEnumerator RotateTo(Transform obj, Vector3 direction, float rotationSpeed)
        {
            float timer = 0;
            Quaternion lookRotation = Quaternion.Euler(0, Quaternion.LookRotation(direction, Vector3.up).eulerAngles.y, 0);
            do
            {
                yield return new WaitForEndOfFrame();
                obj.rotation = Quaternion.RotateTowards(obj.rotation, lookRotation, rotationSpeed * Time.deltaTime);
                timer += Time.deltaTime;
            } while ((Quaternion.Angle(obj.rotation, lookRotation) > 0.5f) && timer < RotationTimeout && !attackToken.CancellationRequested);
            obj.rotation = lookRotation;
        }

        /// <summary>
        /// Rotates a part of the bot until it looks directly at the target
        /// </summary>
        /// <param name="rotatingObject">Part which will be rotated</param>
        /// <param name="relativePosition">The muzzle where projectiles will spawn</param>
        /// <param name="target">Targeted bot</param>
        /// <param name="rotationSpeed"></param>
        protected IEnumerator LookAtBot(Transform rotatingObject, Transform relativePosition, AIMBotController target, float rotationSpeed)
        {
            Quaternion lookRotation;
            float timer = 0;
            do
            {
                yield return new WaitForEndOfFrame();
                timer += Time.deltaTime;
                Vector3 diff = target.transform.position - relativePosition.transform.position;
                lookRotation = Quaternion.Euler(0, Quaternion.LookRotation(diff, Vector3.up).eulerAngles.y, 0);
                rotatingObject.transform.rotation = Quaternion.RotateTowards(rotatingObject.transform.rotation, lookRotation, 10 * rotationSpeed * Time.deltaTime);
            } while ((Quaternion.Angle(rotatingObject.transform.rotation, lookRotation) > 0.5f) && (timer < RotationTimeout) && !attackToken.CancellationRequested);
            rotatingObject.transform.rotation = lookRotation;
        }

        protected void SetLayerToBoss(GameObject obj)
        {
            obj.layer = LayerMask.NameToLayer(LayerDictionary.Boss);
        }

        protected void OnBossDied(object obj, EventArgs args)
        {
            attackToken.Cancel();
        }
    }
}
