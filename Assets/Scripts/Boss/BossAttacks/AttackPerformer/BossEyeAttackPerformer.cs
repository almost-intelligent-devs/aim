﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using AIM.Bots;
using AIM.Bots.AI;
using System.Linq;
using AIM.DangerArea;
using AIM.VFX;
using AIM.Boss.Weakpoints;
using AIM.Sound;

namespace AIM.Boss
{
    public class BossEyeAttackPerformer : BossAttackPerformer
    {
        [SerializeField]
        private GameObject bouncyProjectilePrefab;

        [SerializeField]
        private GameObject baseRotator, eyeBall;

        #region EyeSlamAttacks
        /// <summary>
        /// Gets number of bots within range of eye
        /// </summary>
        /// <param name="range"></param>
        /// <returns></returns>
        public int GetNumberOfAffectedTargets(float range)
        {
            List<AIMBotController> targets = FindBotsInRange(baseRotator, range);
            return targets.Count;
        }

        /// <summary>
        /// Boss slams its eye down => damages and knocks back nearby robots
        /// </summary>
        /// <param name="effectRange">Effective range of attack from eye</param>
        /// <param name="damage">Damage dealt to affected bots</param>
        /// <returns></returns>
        public IEnumerator PerformEyeSlamAttack(float effectRange, int damage, float idleDelay, float knockBackForce)
        {
            soundsPlayer.PlaySound(soundsProvider.GetClip(BossSounds.EyeSlam), false);
            animator.PerformEyeSlam();

            bool attackFinished = false;

            //Resolve Attack Effect when the eye hits the ground
            EventHandler handler = null;
            animator.OnEyeSlamHitGround += handler = (sender, args) =>
            {
                List<AIMBotController> affectedBots = FindBotsInRange(eyeBall, effectRange);

                foreach (AIMBotController bot in affectedBots)
                {
                    Vector3 difference = bot.transform.position - eyeBall.transform.position;
                    difference.y = 0;
                    float distance = Mathf.Sqrt(difference.sqrMagnitude);
                    var force = knockBackForce * difference.normalized * (effectRange - distance) / effectRange;
                    bot.ChangeHP(-damage, baseRotator, force.normalized * ArenaData.HardRagDollForce);
                    bot.KnockBack(force);
                    if (!bot.CurrentBehaviour.IsMalicious())
                    {
                        bot.SetAIBehaviour(new ConfusedBehaviour(bot));
                    }
                }
                soundsPlayer.PlayOneShot(SharedSoundsMaster.Instance.BossWeapons.GetClip(BossWeaponSounds.EyeSlam), 1);
                animator.OnEyeSlamHitGround -= handler;
                attackFinished = true;
            };

            yield return new WaitUntil(() => attackFinished || attackToken.CancellationRequested);
            InvokeAttackFinished(idleDelay);
        }

        #endregion

        #region BouncyBallAttack
        private Vector3 bouncyBallAttackDirection;
        private List<GameObject> balls = new List<GameObject>();

        /// <summary>
        /// Tries to find a direction in maxNumberAttempts with no bots in the path of the projectile
        /// </summary>
        /// <param name="boxWidth">Relative to size of projectile</param>
        /// <param name="maxNumberAttempts"></param>
        /// <returns>true if it finds direction within number of attempts</returns>
        public bool TryFindBouncyBallAttackDirection(float boxWidth, int maxNumberAttempts)
        {
            Vector3 direction;
            int nmbBots;
            int attempts = 0;
            do
            {
                direction = new Vector3(UnityEngine.Random.Range(-10.0f, 10.0f), 0, UnityEngine.Random.Range(-10.0f, 10.0f));
                direction = direction.normalized; //forward vector in box

                Vector3 left = Quaternion.Euler(0, -90, 0) * direction;
                Vector3 rayCastStart = baseRotator.transform.position + left * boxWidth / 2f;

                nmbBots = GetNumberBotsInBox(rayCastStart, direction, boxWidth, float.MaxValue);
                attempts++;
            } while (nmbBots > 0 && attempts < maxNumberAttempts);
            bouncyBallAttackDirection = direction;
            return attempts < maxNumberAttempts;
        }

        public int GetNumberOfBallsOnField()
        {
            balls = balls.Where(obj => obj != null).ToList();
            return balls.Count;
        }
        /// <summary>
        /// Spawns a bouncy Projectile which increases in speed and damage over time
        /// </summary>
        /// <param name="initialProjectilespeed"></param>
        /// <returns></returns>
        public IEnumerator PerformBouncyBallAttack(float idleDelay)
        {
            animator.CenterHeadOverBase = true;
            yield return StartCoroutine(RotateTo(baseRotator.transform, bouncyBallAttackDirection, 100));
            if (attackToken.CancellationRequested)
            {
                yield break;
            }
            soundsPlayer.PlaySound(soundsProvider.GetClip(BossSounds.BouncyBall), false);
            GameObject bouncyBall = Instantiate(bouncyProjectilePrefab);
            bouncyBall.transform.position = eyeBall.transform.position;
            bouncyBall.transform.GetComponent<Rigidbody>().velocity = bouncyBallAttackDirection.normalized;
            balls.Add(bouncyBall);
            InvokeAttackFinished(idleDelay);
            animator.CenterHeadOverBase = false;
        }

        #endregion

        #region LALA
        [SerializeField]
        private ParticleColorChanger eyeColorChanger;
        [SerializeField]
        new private Light light;
        [SerializeField]
        private GameObject LALADangerAreaPrefab;
        [SerializeField]
        private GameObject[] LALAFields = new GameObject[3];
        [SerializeField]
        private GameObject[] LALAStunTargets = new GameObject[3];

        private Vector3[] eyeDirections = { new Vector3(0, 0, -1), new Vector3(-1, 0, 0.57735f), new Vector3(1, 0, 0.57735f) };//z = tan(30)
        private float LALAangle = 90f;
        private int side = 0;

        private float lightUpDuration = 3f;
        private float lightTime = 0;
        private float startIntensity = 0;
        private float maxIntensity = 150;

        /// <summary>
        /// Chooses one of the 3 sides of the boss, which would lead to most bots affected and returns the maximum number of affected bots
        /// </summary>
        public int GetMaxBotsPerSide()
        {
            int maxBotsPerSide = 0;
            for (int i = 2; i >= 0; i--)
            {
                int botsPerSide = GetBotsInAngle(this.transform.position, eyeDirections[i], LALAangle).Count;
                //if (i == 0)
                //botsPerSide /= 2f; // if implemented change botPerSide to float
                if (botsPerSide > maxBotsPerSide)
                {
                    maxBotsPerSide = botsPerSide;
                    side = i;
                }
            }
            return maxBotsPerSide;
        }
        /// <summary>
        /// Boss starts spinning and looks in direction with most bots
        /// It charges up => releases a blinding light => all bots hit by the attack become uncontrollable and Evil
        /// Spawns also electrical fields which make the bots uncontrollable and doNothing
        /// </summary>
        public IEnumerator PerformLALA(float damage, float chargeTime, float stunDuration, float idleDelay)
        {
            BossController boss = this.GetComponent<BossController>();
            WeakPointController weakPointcontroller = this.GetComponent<WeakPointController>();
            yield return StartCoroutine(PerformLALAStartAnimations());
            if (attackToken.CancellationRequested) { yield break; }
            DangerAreaLALA dangerArea = Instantiate(LALADangerAreaPrefab)?.GetComponent<DangerAreaLALA>();
            dangerArea?.CreateMesh(this.transform.position, eyeDirections[side], 30, LALAangle, side);
            eyeColorChanger.ChangeColorAndIntensity(Color.white, 2f);
            soundsPlayer.PlaySound(soundsProvider.GetClip(BossSounds.LalaCountdown), false);
            yield return dangerArea?.StartCoroutine(dangerArea?.ChargeDangerArea(chargeTime));
            if (attackToken.CancellationRequested) { yield break; }
            soundsPlayer.PlaySound(soundsProvider.GetClip(BossSounds.Lala), false);
            Destroy(dangerArea.gameObject);
            yield return StartCoroutine(LALALightUp(boss));
            if (attackToken.CancellationRequested) { yield break; }

            ResolveLALAEffect(damage);
            GameObject electricFields = Instantiate(LALAFields[side]);
            eyeColorChanger.ChangeColorAndIntensity(new Color(0xFF, 0x61, 0x61), -2f);

            yield return StartCoroutine(LALALightDown(boss));
            if (attackToken.CancellationRequested) { yield break; }
            soundsPlayer.PlaySound(soundsProvider.GetClip(BossSounds.LalaStun), false);
            animator.StunLala();
            EventHandler handler = null;
            GameObject stunTarget = null;
            animator.OnEyeStunned += handler = (sender, args) =>
            {
                weakPointcontroller.ActivateEyeWeakPoint();
                stunTarget = Instantiate(LALAStunTargets[side], Vector3.zero, Quaternion.identity);
                animator.OnEyeStunned -= handler;
            };

            yield return new WaitForSeconds(stunDuration);
            if (attackToken.CancellationRequested) { yield break; }
            Destroy(stunTarget);
            weakPointcontroller.DeactivateEyeWeakPoint();
            boss.InvokeReleaseControlBlock();
            LALAElectricField[] fields = electricFields.GetComponentsInChildren<LALAElectricField>();
            float waitTime = 0;
            foreach (LALAElectricField field in fields)
            {
                waitTime = field.StartDestruction();
            }
            animator.StopLala();

            yield return new WaitForSeconds(waitTime);
            Destroy(electricFields);
            InvokeAttackFinished(idleDelay);
        }

        /// <summary>
        /// Boss spins around and signalizes Start of LALA
        /// </summary>
        private IEnumerator PerformLALAStartAnimations()
        {
            yield return StartCoroutine(RotateTo(baseRotator.transform, eyeDirections[side], 100));
            if (attackToken.CancellationRequested) { yield break; }
            animator.CenterHeadOverBase = true;
            yield return new WaitForSeconds(2f);
            if (attackToken.CancellationRequested) { yield break; }
            animator.CenterHeadOverBase = false;
            yield return new WaitForSeconds(1f);
            if (attackToken.CancellationRequested) { yield break; }
            animator.StartLala();
            yield return new WaitForSeconds(2f);
        }

        /// <summary>
        /// Apply effects and damage to affected bots
        /// </summary>
        /// <param name="damage"></param>
        private void ResolveLALAEffect(float damage)
        {
            List<AIMBotController> botsInArea = GetBotsInAngle(this.transform.position, eyeDirections[side], LALAangle);
            List<AIMBotController> botsInFOV = FindBotsInFOV(eyeBall.transform.position, true);
            List<AIMBotController> bots = Gamemaster.Instance.GetAIMBots();
            foreach (AIMBotController bot in bots)
            {
                if (!bot.Alive)
                    continue;
                if (botsInFOV.Contains(bot) && botsInArea.Contains(bot))
                {
                    var forceDir = bot.transform.position - baseRotator.transform.position;
                    forceDir.y = 0f;
                    bot.ChangeHP(-damage, this.gameObject, forceDir.normalized * ArenaData.MegaRagdollForce);
                    bot.SetAIBehaviour(new EvilBehaviour(bot));
                    Gamemaster.Instance.GetBoss().ReleaseControlBlock += bot.OnReleaseControlBlock;
                    bot.ControlBlocked = true;
                    if (bot.ControlledByPlayer)
                    {
                        Gamemaster.Instance.GetInputHandler().GoToSelectMode();
                    }
                }
                else
                {
                    if (!bot.CurrentBehaviour.IsMalicious())
                    {
                        bot.SetAIBehaviour(new ConfusedBehaviour(bot));
                    }
                }
            }
        }

        private IEnumerator LALALightUp(BossController boss)
        {
            AudioClip clip = soundsProvider.GetClip(BossSounds.Lala);
            soundsPlayer.PlaySound(clip, false);
            bool lalaInvoked = false;
            lightTime = 0;
            while (lightTime < lightUpDuration && !attackToken.CancellationRequested)
            {
                if (!lalaInvoked && lightTime > clip.length)
                {
                    lalaInvoked = true;
                    boss.InvokeLALA();
                }
                yield return new WaitForEndOfFrame();
                lightTime += Time.deltaTime;
                light.intensity = Mathf.Lerp(startIntensity, maxIntensity, lightTime / lightUpDuration);
            }
            yield return new WaitForSeconds(1f);

        }
        private IEnumerator LALALightDown(BossController boss)
        {
            bool endLalaInvoked = false;
            while (lightTime > 0)
            {
                yield return new WaitForEndOfFrame();
                if (light.intensity > 8)
                    lightTime -= Time.deltaTime;
                else
                {
                    if (!endLalaInvoked)
                    {
                        endLalaInvoked = true;
                        boss.InvokeEndLALA();
                    }
                    lightTime -= Time.deltaTime / 5f;
                }
                light.intensity = Mathf.Lerp(startIntensity, maxIntensity / 3f, lightTime / lightUpDuration);
            }
            yield return new WaitForSeconds(1f);
        }

        #endregion

        #region CorruptionBeamAttack
        [SerializeField]
        private GameObject corruptionBeamPrefab;

        /// <summary>
        /// Rotates eye so it looks at target and shoots corruptionBeam for a duration
        /// While corruption is in progress Bot does not move and looks at eye
        /// </summary>
        /// <param name="behaviour">Corrupted behavior which bot adapts after corruption</param>
        /// <param name="corruptionDuration">Time it takes for beam to stay on bot</param>
        /// <returns></returns>
        public IEnumerator PerformCorruptionBeamAttack(AIMBotController target, AIMBotAIBehaviour behaviour, float corruptionDuration, float idleDelay)
        {
            if (target == null)
            {
                InvokeAttackFinished();
                yield break;
            }

            animator.CenterHeadOverBase = true;
            if (behaviour is EvilBehaviour)
            {
                soundsPlayer.PlaySound(soundsProvider.GetClip(BossSounds.EvilBeam), false);
            }
            else if (behaviour is StareAtPointBehaviour)
            {
                soundsPlayer.PlaySound(soundsProvider.GetClip(BossSounds.DoNothingBeam), false);
            }
            yield return new WaitForSeconds(2f);
            yield return StartCoroutine(LookAtBot(baseRotator.transform, eyeBall.transform, target, 10));
            float currentStareTime = 0;

            if (attackToken.CancellationRequested) { yield break; }
            GameObject beam = Instantiate(corruptionBeamPrefab, eyeBall.transform);
            SetLayerToBoss(beam);

            var ptc = beam.GetComponent<ParticleTargetController>();

            if (ptc == null)
            {
                throw new InvalidOperationException($"{nameof(corruptionBeamPrefab)} does not have a {nameof(ParticleTargetController)} component");
            }

            ptc.Target = target.HeadTransform;

            target.SetAIBehaviour(new StareAtPointBehaviour(target, eyeBall.transform));
            target.ControlBlocked = true;
            if (target.ControlledByPlayer)
            {
                Gamemaster.Instance.GetInputHandler().GoToSelectMode();
            }
            while (currentStareTime < corruptionDuration && !attackToken.CancellationRequested)
            {
                Vector3 direction = target.HeadTransform.position - eyeBall.transform.position;
                Quaternion lookRotation = Quaternion.LookRotation(direction, Vector3.up);
                baseRotator.transform.rotation = Quaternion.Euler(0, lookRotation.eulerAngles.y, 0);
                /*
                RaycastHit rayInfo;
                if (!Physics.Raycast(eyeBall.transform.position, direction, out rayInfo))
                {
                    Destroy(beam);
                    yield break;
                }*/

                yield return new WaitForEndOfFrame();
                currentStareTime += Time.deltaTime;
            }
            Destroy(beam);
            if (behaviour != null)
            {
                target.SetAIBehaviour(behaviour);
            }
            target.ControlBlocked = false;

            animator.CenterHeadOverBase = false;
            InvokeAttackFinished(idleDelay);
        }

        #endregion
    }
}
