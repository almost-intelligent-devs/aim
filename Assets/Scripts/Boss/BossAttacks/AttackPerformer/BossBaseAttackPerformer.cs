﻿using AIM.Bots;
using AIM.Bots.AI;
using AIM.Environment;
using AIM.Sound;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AIM.Boss
{
    public class BossBaseAttackPerformer : BossAttackPerformer
    {
        [SerializeField]
        private Transform bossBase;

        [SerializeField]
        private GameObject sawBladeSidePrefab;
        private Transform[] sawBladeSides = new Transform[3];
        [SerializeField]
        private Transform[] sawBladeSpawnPoints = new Transform[3];

        #region SawBladeAttack
        /// <summary>
        /// For usefullness calculation. Returns number of bots within boxArea of one bossSide
        /// </summary>
        /// <param name="side">index of bossSide</param>
        /// <param name="sideWidth">Width of the box</param>
        /// <param name="range">Height of the box</param>
        /// <returns></returns>
        public int GetNumberBotsOnSide(int side, float sideWidth, float range)
        {
            Vector3 forward = sawBladeSpawnPoints[side].forward.normalized;
            Vector3 left = Quaternion.Euler(0, -90, 0) * forward;
            Vector3 cornerPoint = sawBladeSpawnPoints[side].position + left.normalized * sideWidth / 2;
            return GetNumberBotsInBox(cornerPoint, forward, sideWidth, range);
        }

        /// <summary>
        /// Spawns Sawblades and moves them out and back of Base
        /// </summary>
        /// <param name="cycles">needs to be even</param>
        /// <param name="speed"></param>
        /// <param name="range"></param>
        /// <returns></returns>
        public IEnumerator PerformSawBladeAttack(int cycles, float startDelay, float interCyclePauseTime, float speed, float range, float idleDelay)
        {
            if (cycles % 2 != 0) //cycles has to be even
            {
                Debug.LogError("BossBaseAttackPerformer.PerformSawBladeAttack: number of cycles is not even!");
                InvokeAttackFinished();
                yield break;
            }
            //Spawn Sawblades on each sides
            for (int i = 0; i < sawBladeSides.Length; i++)
            {
                sawBladeSides[i] = Instantiate(sawBladeSidePrefab, sawBladeSpawnPoints[i].position, sawBladeSpawnPoints[i].rotation).transform;
                sawBladeSides[i].SetParent(bossBase);
            }
            AudioClip clip = soundsProvider.GetClip(BossSounds.DestructionDisk);
            soundsPlayer.PlaySound(clip, false);
            yield return new WaitForSeconds(clip.length + startDelay);
            //Move Sawblades
            for (int i = 0; i < cycles; i++)
            {
                float traveledDistance = 0;
                while (traveledDistance < range)
                {
                    if (attackToken.CancellationRequested) { break; }
                    traveledDistance += Time.deltaTime * speed;
                    foreach (Transform sawBladeSide in sawBladeSides)
                    {
                        sawBladeSide.Translate((i % 2 == 0 ? 1 : -1) * Vector3.forward * Time.deltaTime * speed);
                    }
                    yield return new WaitForEndOfFrame();
                }
                if (i % 2 == 1 && !attackToken.CancellationRequested)
                {
                    for (int j = 0; j < sawBladeSides.Length; j++)
                    {
                        sawBladeSides[j].position = sawBladeSpawnPoints[j].position;
                    }
                    yield return new WaitForSeconds(interCyclePauseTime);
                }
            }

            //Destroy Sawblades again after attack
            foreach (Transform sawBladeSide in sawBladeSides)
            {
                Destroy(sawBladeSide.gameObject);
            }
            InvokeAttackFinished(idleDelay);
        }
        #endregion

        #region StompAttack

        /// <summary> Gets number of bots within range of boss </summary>
        public int GetNumberOfAffectedTargets(float range) => FindBotsInRange(gameObject, range).Count;

        /// <summary>
        /// Boss jumps & stomps on the ground => damages and knocks back nearby robots
        /// </summary>
        /// <param name="effectRange">Effective range of attack from base</param>
        /// <param name="damage">Damage dealt to affected bots</param>
        /// <returns></returns>
        public IEnumerator PerformStompAttack(float effectRange, int damage, float idleDelay, float knockBackForce)
        {
            soundsPlayer.PlayOneShotDelayed(soundsProvider.GetClip(BossSounds.Stomp), 1f);
            animator.PerformStomp();

            bool attackFinished = false;

            //Resolve Attack Effect when the boss hits the ground
            EventHandler handler = null;
            animator.OnStompHitGround += handler = (sender, args) =>
            {
                ResolveStompEffect(effectRange, damage, knockBackForce);
                animator.OnStompHitGround -= handler;
                attackFinished = true;
            };

            yield return new WaitUntil(() => attackFinished || attackToken.CancellationRequested);
            InvokeAttackFinished(idleDelay);
        }

        private void ResolveStompEffect(float effectRange, int damage, float knockBackForce)
        {
            List<AIMBotController> affectedBots = FindBotsInRange(gameObject, effectRange);

            foreach (AIMBotController bot in affectedBots)
            {
                var forceDir = bot.transform.position - bossBase.position;
                forceDir.y = 0f;
                bot.ChangeHP(-damage, gameObject, forceDir.normalized * ArenaData.HardRagDollForce);
                Vector3 difference = bot.transform.position - gameObject.transform.position;
                difference.y = 0;
                float distance = Mathf.Sqrt(difference.sqrMagnitude);
                bot.KnockBack(knockBackForce * difference.normalized * (effectRange - distance) / effectRange);
                if (!bot.CurrentBehaviour.IsMalicious())
                {
                    bot.SetAIBehaviour(new ConfusedBehaviour(bot));
                }
            }
            soundsPlayer.PlayOneShot(SharedSoundsMaster.Instance.BossWeapons.GetClip(BossWeaponSounds.JumpLand), 1);

        }
        #endregion

        #region JumpAttack

        [SerializeField]
        private Transform baseRotator;
        private float maxJumpHeight = 10f;
        private float jumpTime = 1f;

        /// <summary>
        /// Boss chooses one of the 4 walls or center at random
        /// Jumps to it and causes damage and knockback on bots near the landingposition
        /// </summary>
        /// <param name="effectRange"></param>
        /// <param name="damage"></param>
        /// <param name="idleDelay"></param>
        /// <param name="knockBackForce"></param>
        /// <returns></returns>
        public IEnumerator PerformJumpAttack(float effectRange, int damage, float idleDelay, float knockBackForce)
        {
            //Choose targetPosition and look at it
            JumpableWall[] jumpPoints = GameObject.FindObjectsOfType<JumpableWall>();
            int random = UnityEngine.Random.Range(0, 4);
            Vector3 startPosition = this.transform.position;
            Vector3 targetPosition;
            if (random < jumpPoints.Length)
                targetPosition = jumpPoints[random].transform.position;
            else
                targetPosition = Vector3.zero;
            Vector3 direction = targetPosition - startPosition;
            yield return StartCoroutine(RotateTo(baseRotator, direction, 100));
            if (attackToken.CancellationRequested) { yield break; }
            //start StompAnimation
            animator.PerformStomp();

            soundsPlayer.PlayOneShotDelayed(soundsProvider.GetClip(BossSounds.Jump), 1.5f);
            yield return new WaitForSeconds(2f);
            if (attackToken.CancellationRequested) { yield break; }
            //Jump from current to target Position
            this.GetComponent<BossController>().ReleasePositionPoint();
            float timer = 0;
            while (timer < jumpTime)
            {
                this.transform.position = GetParabolaPosition(startPosition, targetPosition, timer / jumpTime);
                yield return new WaitForEndOfFrame();
                timer += Time.deltaTime;
            }
            this.transform.position = targetPosition;
            ResolveStompEffect(effectRange, damage, knockBackForce);
            InvokeAttackFinished(idleDelay);
        }

        private Vector3 GetParabolaPosition(Vector3 start, Vector3 target, float t)
        {
            float currentHeight = -4 * maxJumpHeight * t * t + 4 * maxJumpHeight * t; //f(x) =-4H * x^2 + 4H *x
            var mid = Vector3.Lerp(start, target, t);

            return new Vector3(mid.x, currentHeight, mid.z);
        }
        #endregion
    }
}
