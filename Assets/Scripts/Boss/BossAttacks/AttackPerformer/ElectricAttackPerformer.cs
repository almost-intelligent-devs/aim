﻿using AIM.Bots;
using AIM.Sound;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace AIM.Boss
{
    public class ElectricAttackPerformer : BossAttackPerformer
    {
        [SerializeField]
        private GameObject electricFieldPrefab;

        [SerializeField]
        private GameObject pillarPrefab;

        public ElectricField ElectricField { get; private set; }
        private Pillar[] pillars;
        private int activePillars = 0;
        private float currentPillarTimer = 0;

        private const float PillarTime = 3;
        private const float MinPillarDistance = 7;
        private const float PillarSpawnHeight = -5.5f;

        private int numberOfPillars;
        private void Update()
        {
            //Timeout for pillars
            if (activePillars > 0)
            {
                currentPillarTimer += Time.deltaTime;
                if (currentPillarTimer > PillarTime)
                {
                    foreach (Pillar pillar in pillars)
                        pillar.Deactivate();
                    activePillars = 0;
                    currentPillarTimer = 0;
                }
            }
        }

        #region ElectricFieldAttack
        public void SetNumberOfPillars(float hpThreshold, float hpThreshold2)
        {
            if (ElectricFieldActive())
                return;
            List<AIMBotController> bots = Gamemaster.Instance.GetAIMBots();
            int botCount = 0;
            float accumulativeBotHp = 0;
            foreach(AIMBotController bot in bots)
            {
                if (bot.Alive)
                {
                    botCount++;
                    accumulativeBotHp += bot.HitPoints;
                }
            }
            if(botCount < 2 || accumulativeBotHp < hpThreshold2)
            {
                numberOfPillars = 1;
            }
            else if (botCount < 4 || accumulativeBotHp < hpThreshold || !Gamemaster.Instance.HardMode) //cap numberOfPillars for normalmode to 2
            {
                numberOfPillars = 2;
            }
            else
            {
                numberOfPillars = 3;
            }
            pillars = new Pillar[numberOfPillars];
        }

        /// <summary>
        /// Spawns electrical field and pillars
        /// </summary>
        /// <returns></returns>
        public void PerformElectricFieldAttack(float idleDelay)
        {
            if (ElectricFieldActive())
            {
                InvokeAttackFinished();
                return;
            }

            soundsPlayer.PlaySound(soundsProvider.GetClip(BossSounds.ElectricFieldStart), false);
            SpawnElectricField();
            SpawnPillars();
            InvokeAttackFinished(idleDelay);
        }

        /// <summary>
        /// [Callded by Pillar]Called when a pillar is hit and updates the progress
        /// </summary>
        public void PillarHit()
        {
            currentPillarTimer = 0;//reset timer on successful hit
            activePillars++;
            if (activePillars == numberOfPillars)
                ClearObjects();
        }
        #endregion

        #region SpawnAndCleanUp
        private void SpawnElectricField()
        {
            ElectricField = Instantiate(electricFieldPrefab).GetComponent<ElectricField>();
            Vector3 spawnPosition = Gamemaster.Instance.GetBoss().transform.position;
            spawnPosition.y = ArenaData.GroundHeight + 0.01f;
            ElectricField.transform.position = spawnPosition;
        }

        private void SpawnPillars()
        {
            int index = 0;
            int counter = 0;
            while (index < numberOfPillars && counter < 1000)
            {
                Vector3 position = NavMeshHelper.RandomPositionOnNavMesh();
                position.y = PillarSpawnHeight;
                if (HasEnoughDistance(position))
                {
                    pillars[index] = Instantiate(pillarPrefab).GetComponent<Pillar>();
                    pillars[index].transform.position = position;
                    index++;
                }
                counter++;
            }
        }

        /// <summary>
        /// Checks if the new Pillarposition has enough distance to other pillars
        /// </summary>
        /// <param name="pillarPos"></param>
        /// <returns></returns>
        private bool HasEnoughDistance(Vector3 pillarPos)
        {
            for(int i=0; i < numberOfPillars; i++)
            {
                if (pillars[i] == null)
                    return true;
                if (Mathf.Sqrt((pillars[i].transform.position - pillarPos).sqrMagnitude) < MinPillarDistance)
                    return false;
            }
            return true;
        }

        private void ClearObjects()
        {
            soundsPlayer.PlaySound(soundsProvider.GetClip(BossSounds.ElectricFieldEnd), false);
            activePillars = 0;
            currentPillarTimer = 0;
            foreach (Pillar pillar in pillars)
                Destroy(pillar.gameObject);
            pillars = null;
            Destroy(ElectricField.gameObject);
        }
        #endregion
    }
}
