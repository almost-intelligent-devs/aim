﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AIM.Boss
{
    public class EyeSlamAttack : BossAttack
    {
        public override float Cooldown => 20;
        protected override float idleDelay => mediumIdle;

        private float[] usefullness = new float[] { 40, 70, 100}; //usefullness depending on number of bots affected
        private int damage = 5;
        private float attackRange = 4f;
        private float damageTime = 15f;
        private float maxDamage = 200;
        private float knockBackForce = 40;
        private BossEyeAttackPerformer attackPerformer;

        public EyeSlamAttack(BossEyeAttackPerformer attackPerformer)
        {
            this.attackPerformer = attackPerformer;
        }

        /// <summary>
        /// Depends on how many Bots are in melee range(effect range) and how much damage has been taken recently
        /// </summary>
        /// <returns></returns>
        public override float GetAttackUsefullness()
        {

            int affectedBots = attackPerformer.GetNumberOfAffectedTargets(attackRange);
            if (affectedBots == 0)
                return 0;
            //float targetParameter = usefullness[Math.Min(affectedBots, usefullness.Length) - 1];
            float damage = Gamemaster.Instance.GetBoss().GetDamageTaken(damageTime);
            float damagePercent = Mathf.Min(damage / maxDamage, 1);
            return damagePercent * 100;
        }

        public override void PerformAttack()
        {
            attackPerformer.StartCoroutine(attackPerformer.PerformEyeSlamAttack(attackRange, damage, idleDelay, knockBackForce));
        }
    }
}
