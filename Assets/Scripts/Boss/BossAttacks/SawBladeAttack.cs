﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AIM.Boss
{
    public class SawBladeAttack : BossAttack
    {
        public override float Cooldown => 25;
        protected override float idleDelay => mediumIdle;

        private float[] usefullness = new float[] { 20, 30, 50, 80, 100 }; //usefullness depending on number of bots affected
        private int minCycles = 3;
        private int maxCycles = 5;
        private float interCyclePauseTime
        {
            get
            {
                if (Gamemaster.Instance.HardMode)
                    return 1.5f;
                else
                    return 3f;
            }
        }

        private float startDelay
        {
            get
            {
                if (Gamemaster.Instance.HardMode)
                    return 3f;
                else
                    return 5f;
            }
        }
        private float speed = 2f;
        private float range = 2f;
        private float sideWidth = 4;
        private BossBaseAttackPerformer attackPerformer;

        public SawBladeAttack(BossBaseAttackPerformer attackPerformer)
        {
            this.attackPerformer = attackPerformer;
        }

        /// <summary>
        /// Checks how many bots would be affected by sawblade attack
        /// </summary>
        /// <returns></returns>
        public override float GetAttackUsefullness()
        {
            int cumulativeNumberBots = 0;
            for (int i = 0; i < 3; i++)
            {
                cumulativeNumberBots += attackPerformer.GetNumberBotsOnSide(i, sideWidth, range);
            }
            if (cumulativeNumberBots == 0)
                return 0;

            return usefullness[Math.Min(cumulativeNumberBots, usefullness.Length) - 1];
        }

        public override void PerformAttack()
        {
            int cycles = 2 * UnityEngine.Random.Range(minCycles, maxCycles + 1);//3-5 in and out cycles
            attackPerformer.StartCoroutine(attackPerformer.PerformSawBladeAttack(cycles, startDelay, interCyclePauseTime, speed, range, idleDelay));
        }
    }
}