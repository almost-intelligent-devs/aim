﻿using AIM.Bots;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace AIM.Boss
{
    public abstract class BossAttack
    {
        public bool Available = false;
        public abstract float Cooldown { get; }
        protected abstract float idleDelay { get; }
        protected const float lowIdle = 0;
        protected const float mediumIdle = 3;
        /// <summary>
        /// Sets the Attack target and returns the Usefullness of the Attack
        /// </summary>
        /// <returns>Usefullness value between 0 and 100</returns>
        public abstract float GetAttackUsefullness();

        public abstract void PerformAttack();
    }
}
