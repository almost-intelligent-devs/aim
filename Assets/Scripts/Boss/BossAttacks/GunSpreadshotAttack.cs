﻿using AIM.DangerArea;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace AIM.Boss
{

    public class GunSpreadshotAttack : BossAttack
    {
        public const float HardModeShootDelayMultiplier = 0.75f;
        public override float Cooldown => 10;
        protected override float idleDelay => lowIdle;

        private float[] usefullness = new float[] { 20, 60, 90, 100 }; //usefullness depending on number of bots affected
        private float spreadAngle = 60;
        private int rotationRounds = 4;
        private float projectileRange = 7;
        private float startDelay
        {
            get
            {
                if (Gamemaster.Instance.HardMode)
                    return 2f;
                else
                    return 4f;
            }
        }
        private BossGunAttackPerformer attackPerformer;

        public GunSpreadshotAttack(BossGunAttackPerformer attackPerformer)
        {
            this.attackPerformer = attackPerformer;
        }

        /// <summary>
        /// Depends on how many bots would be affected by spreadAttack
        /// </summary>
        /// <returns></returns>
        public override float GetAttackUsefullness()
        {
            int numberOfHitBots = attackPerformer.GetNumberOfHitBots(spreadAngle, projectileRange);
            if (numberOfHitBots == 0)
                return 0;
            return usefullness[Math.Min(numberOfHitBots, usefullness.Length)-1];
        }

        public override void PerformAttack()
        {
            attackPerformer.StartCoroutine(attackPerformer.PerformSpreadShotAttack(startDelay, spreadAngle, rotationRounds, projectileRange, idleDelay));
        }

    }
}