﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AIM.Boss
{
    public class ElectricFieldAttack : BossAttack
    {
        public override float Cooldown => 100;
        protected override float idleDelay => 25;

        private float hpThreshold = 200f;
        private float hpThreshold2 = 100f;
        private float baseUsefullness = 30f;
        private ElectricAttackPerformer attackPerformer;

        public ElectricFieldAttack(ElectricAttackPerformer attackPerformer)
        {
            this.attackPerformer = attackPerformer;
        }

        /// <summary>
        /// Depends only if attack is currently still active
        /// </summary>
        /// <returns>0 if still active | 50 if not active</returns>
        public override float GetAttackUsefullness()
        {
            if (attackPerformer.ElectricFieldActive() || !attackPerformer.InCenter())
                return 0;
            return baseUsefullness;
        }

        public override void PerformAttack()
        {
            attackPerformer.SetNumberOfPillars(hpThreshold, hpThreshold2);
            attackPerformer.PerformElectricFieldAttack(idleDelay);
        }
    }
}