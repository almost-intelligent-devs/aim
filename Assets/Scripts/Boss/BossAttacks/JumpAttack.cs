﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AIM.Boss
{
    public class JumpAttack : BossAttack
    {
        public override float Cooldown => 30;

        protected override float idleDelay => 3;

        private float damageTime = 5f;
        private float maxDamage = 50;
        private int damage = 5;
        private float attackRange = 5f;
        private float knockBackForce = 80;

        private BossBaseAttackPerformer attackPerformer;

        public JumpAttack(BossBaseAttackPerformer attackPerformer)
        {
            this.attackPerformer = attackPerformer;
        }
        /// <summary>
        /// Usefullness depends on damageReceived and is zero when electricfield is active
        /// </summary>
        /// <returns></returns>
        public override float GetAttackUsefullness()
        {
            if (attackPerformer.ElectricFieldActive())
                return 0;
            float damage = Gamemaster.Instance.GetBoss().GetDamageTaken(damageTime);
            float damagePercent = Mathf.Min(damage / maxDamage, 1);
            return 100*damagePercent;
        }

        public override void PerformAttack()
        {
            attackPerformer.StartCoroutine(attackPerformer.PerformJumpAttack(attackRange, damage, idleDelay, knockBackForce));
        }

    }
}