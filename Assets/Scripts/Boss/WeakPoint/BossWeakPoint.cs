﻿using System;
using UnityEngine;

namespace AIM.Boss.Weakpoints
{
    public class BossWeakPoint : MonoBehaviour
    {

        protected BossController boss;
        [SerializeField]
        private int id;
        [SerializeField]
        private float bonusDamageMultiplier = 1;
        private new Collider collider;

        public event EventHandler WeakpointHit;

        public bool IsActive()
        {
            return collider.enabled;
        }

        protected virtual void Awake()
        {
            collider = this.GetComponent<Collider>();
            if (collider == null)
            {
                throw new InvalidOperationException($"{nameof(collider)} is not specified!");
            }
        }

        protected virtual void Start()
        {
            if (this.gameObject.tag != TagDictionary.BossWeakPoint)
            {
                throw new InvalidOperationException($"{nameof(BossWeakPoint)} should have tag {TagDictionary.BossWeakPoint}!");
            }
            if (this.gameObject.layer != LayerMask.NameToLayer(LayerDictionary.Boss))
            {
                throw new InvalidOperationException($"{nameof(BossWeakPoint)} should have layer {LayerMask.NameToLayer(LayerDictionary.Boss)}!");
            }
            collider.enabled = false;
            boss = Gamemaster.Instance.GetBoss();
            SubscribeToBoss();
        }

        protected virtual void SubscribeToBoss()
        {
            boss.SubscribeWeakPoint(this, id);
        }
        /// <summary>
        /// Deals extra damage additonal to the already received damage
        /// </summary>
        /// <param name="damage"></param>
        public void DealBonusDamage(float damage)
        {
            Debug.Log("Dealt bonus damage!");
            boss.ChangeHP(-bonusDamageMultiplier * damage);
            WeakpointHit?.Invoke(this, EventArgs.Empty);
        }

        public void DealCriticalDamage(float damage)
        {
            boss.ChangeHP(-damage); //deal normal damage
            DealBonusDamage(damage); //deal bonus damage
            WeakpointHit?.Invoke(this, EventArgs.Empty);
        }

        public virtual void ActivateWeakPoint()
        {
            if (Gamemaster.Instance.HardMode)
            {
                bonusDamageMultiplier = 1.5f;
            }
            else
            {
                bonusDamageMultiplier = 1f;
            }
            collider.enabled = true;
        }

        public virtual void DeactivateWeakPoint()
        {
            collider.enabled = false;
        }

    }
}
