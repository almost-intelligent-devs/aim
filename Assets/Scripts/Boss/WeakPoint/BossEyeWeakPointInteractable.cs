﻿using System.Collections;
using System.Collections.Generic;
using AIM.Bots.AI;
using UnityEngine;

namespace AIM.Interactables
{
    public class BossEyeWeakPointInteractable : Interactable, IPunchable
    {
        public override void Interact(GameObject interactor) {}
        #region Bots AI utils
        private List<AttackToken> attackTokens;
        public List<AttackToken> GetAttackTokens()
        {
            if (attackTokens == null)
            {
                attackTokens = AttackToken.GenerateTokensAroundCollider(this.transform, GetComponent<Collider>(), .1f, 1.5f, 0, 360, false);
                Debug.Log(attackTokens.Count);
            }

            return attackTokens;
        }


        private void OnDrawGizmos()
        {
            var tokens = GetAttackTokens();
            foreach (var token in tokens)
            {
                if (token.State == TokenState.Open)
                {
                    Gizmos.DrawWireSphere(token.Position, .1f);
                }
                else
                {
                    Gizmos.DrawSphere(token.Position, .1f);
                }
            }
        }

        #endregion
    }
}
