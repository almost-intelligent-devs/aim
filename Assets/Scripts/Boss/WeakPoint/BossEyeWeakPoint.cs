﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace AIM.Boss.Weakpoints
{
    public class BossEyeWeakPoint : BossWeakPoint
    {
        [SerializeField] private NavMeshObstacle obstacle;
        protected override void Awake()
        {
            base.Awake();
            obstacle = GetComponent<NavMeshObstacle>();
            if (obstacle == null)
            {
                throw new InvalidOperationException($"{nameof(obstacle)} is not specified!");
            }
        }
        protected override void Start()
        {
            base.Start();
            obstacle.enabled = false;
        }

        public override void ActivateWeakPoint()
        {
            base.ActivateWeakPoint();
            obstacle.enabled = true;
        }

        public override void DeactivateWeakPoint()
        {
            base.DeactivateWeakPoint();
            obstacle.enabled = false;
        }

        protected override void SubscribeToBoss()
        {
            boss.SubscribeEyeWeakPoint(this);
        }
    }
}
