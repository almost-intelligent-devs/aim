﻿using AIM.Controllables;
using AIM.Interactables;
using System;
using UnityEngine;

namespace AIM.Boss.Weakpoints
{
    public class WeakPointController : Controllable
    {
        public event EventHandler<BossWeakPointEventArgs> OnWeakpointActivated;
        public event EventHandler<BossWeakPointEventArgs> OnWeakpointDeactivated;

        private int side = -1;

        private BossWeakPoint[] weakPoints = new BossWeakPoint[3];

        public BossWeakPoint[] GetWeakPoints()
        {
            return weakPoints;
        }

        private BossEyeWeakPoint eyeWeakPoint;

        public BossEyeWeakPoint GetEyeWeakPoint()
        {
            return eyeWeakPoint;
        }

        public override void Activate(Button button)

        {
            if (button == null || side != -1)
            {
                return;
            }
            side = ((BossWeakPointButton)button).Id;
            weakPoints[side].ActivateWeakPoint();
            OnWeakpointActivated?.Invoke(this, new BossWeakPointEventArgs(weakPoints[side], side));

        }

        public override void Deactivate(Button button)
        {
            if (button == null || side != ((BossWeakPointButton)button).Id)
            {
                return;
            }
            weakPoints[side].DeactivateWeakPoint();
            OnWeakpointDeactivated?.Invoke(this, new BossWeakPointEventArgs(weakPoints[side], side));
            side = -1;
        }

        public void SubscribeWeakPoint(BossWeakPoint weakPoint, int id)
        {
            if (id > weakPoints.Length - 1)
            {
                throw new InvalidOperationException($"Id of weakpoint larger than array size!");
            }
            if (weakPoints[id] != null)
            {
                throw new InvalidOperationException($"Trying to register the same {nameof(BossWeakPoint)} twice!");
            }
            else
            {
                weakPoints[id] = weakPoint;
            }
        }

        public void SubscribeEyeWeakPoint(BossEyeWeakPoint eyeWeakPoint)
        {
            if (this.eyeWeakPoint != null)
            {
                throw new InvalidOperationException($"Trying to register the same {nameof(BossEyeWeakPoint)} twice!");
            }
            else
            {
                this.eyeWeakPoint = eyeWeakPoint;
            }

        }

        public void ActivateEyeWeakPoint()
        {
            eyeWeakPoint.ActivateWeakPoint();
        }

        public void DeactivateEyeWeakPoint()
        {
            eyeWeakPoint.DeactivateWeakPoint();
        }
    }
}

