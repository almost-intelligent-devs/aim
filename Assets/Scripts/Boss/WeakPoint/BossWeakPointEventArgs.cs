﻿using System;

namespace AIM.Boss.Weakpoints
{
    public class BossWeakPointEventArgs : EventArgs
    {
        public BossWeakPoint Weakpoint { get; private set; }
        public int Side { get; private set; }

        public BossWeakPointEventArgs(BossWeakPoint weakpoint, int side)
        {
            Weakpoint = weakpoint;
            Side = side;
        }
    }
}
