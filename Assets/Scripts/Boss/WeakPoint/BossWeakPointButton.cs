﻿using AIM.Interactables;
using UnityEngine;

namespace AIM.Boss.Weakpoints
{
    public class BossWeakPointButton : Button
    {
        public int Id;


        protected override void Start()
        {
            // isInteractable = false;
            // isActivated = false;

            base.Start();

            Gamemaster.Instance.GetBoss().BossPhaseChanged += OnBossPhaseChanged;
        }

        private void OnBossPhaseChanged(object sender, System.EventArgs args)
        {
            BossPhaseChangedEventArgs phaseArgs = args as BossPhaseChangedEventArgs;
            if (phaseArgs.Phase == 2 || phaseArgs.Phase == 44)
            {
                SetInteractable(true);
            }
            else
            {
                SetInteractable(false);
            }
        }
    }
}
