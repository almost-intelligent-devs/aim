﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AIM.Boss
{
    public class BossDiedEventArgs : EventArgs
    {
        public BossController Boss { get; private set; }

        public BossDiedEventArgs(BossController boss)
        {
            this.Boss = boss;
        }
    }
}
