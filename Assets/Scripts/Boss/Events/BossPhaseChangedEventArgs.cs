﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AIM.Boss
{
    public class BossPhaseChangedEventArgs : EventArgs
    {
        public int Phase { get; private set; }

        public BossPhaseChangedEventArgs(int phase)
        {
            this.Phase = phase;
        }
    }
}
