﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AIM.Boss
{
    public class BossHpChangedEventArgs : EventArgs
    {
        public float Difference { get; private set; }

        public BossHpChangedEventArgs(float diff)
        {
            this.Difference = diff;
        }
    }
}
