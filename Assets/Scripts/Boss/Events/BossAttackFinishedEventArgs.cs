﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AIM.Boss
{
    public class BossAttackFinishedEventArgs : EventArgs
    {
        public float IdleTime { get; private set; }

        public BossAttackFinishedEventArgs(float idleTime)
        {
            this.IdleTime = idleTime;
        }
    }
}