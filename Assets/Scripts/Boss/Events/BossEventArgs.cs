﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AIM.Boss
{
    public class BossEventArgs : EventArgs
    {
        public BossController Boss { get; private set; }

        public BossEventArgs(BossController boss)
        {
            this.Boss = boss;
        }
    }
}
