﻿using AIM.Boss.Weakpoints;
using AIM.Bots.AI;
using AIM.Environment;
using AIM.Sound;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AIM.Boss
{
    [RequireComponent(typeof(BossGunAttackPerformer), typeof(WeakPointController), typeof(BossAnimator))]
    public class BossController : MonoBehaviour, IPunchable
    {
        #region structs
        private struct Damage
        {
            public float Amount { get; }
            public float Time { get; }
            public Damage(float amount, float time)
            {
                this.Amount = amount;
                this.Time = time;
            }
        }

        private struct Attack
        {
            public Type AttackType { get; }
            public float Time { get; }
            public Attack(Type attackType, float time)
            {
                this.AttackType = attackType;
                this.Time = time;
            }
        }
        #endregion

        #region events
        public event EventHandler BossHpChanged;
        public event EventHandler BossPhaseChanged;
        public event EventHandler BossDied;
        public event EventHandler BossDeathAnimationFinished;
        public event EventHandler ReleaseControlBlock;
        public event EventHandler LALA;
        public event EventHandler EndLALA;

        public void InvokeEndLALA()
        {
            EndLALA?.Invoke(this, EventArgs.Empty);
        }

        public void InvokeLALA()
        {
            LALA?.Invoke(this, EventArgs.Empty);
        }

        public void InvokeReleaseControlBlock()
        {
            ReleaseControlBlock?.Invoke(this, EventArgs.Empty);
        }
        #endregion

        #region HP management
        public const float MaxHP = 1000f;
        private float hp = MaxHP;
        private List<Damage> damageLog = new List<Damage>();
        private enum ProtectionState { None, Transform, Protected };
        private ProtectionState currentProtectionState = ProtectionState.None;

        public void ChangeHP(float diff)
        {
            if (currentProtectionState == ProtectionState.Protected)
            {
                if (Gamemaster.Instance.HardMode)
                {
                    diff *= 0.4f;
                }
                else
                {
                    diff *= 0.5f;
                }
            }
            hp = Mathf.Max(0, hp + diff);
            damageLog.Add(new Damage(Mathf.Abs(diff), Time.time));
            BossHpChanged?.Invoke(this, new BossHpChangedEventArgs(diff));
            if (Alive && (hp < 0f || Mathf.Approximately(hp, 0f)))
            {
                ProcessDeath();
            }
            else if (hp < MaxHP / 3f && phase == 2) //Change to last phase on last 333 HP
            {
                BossPhaseChanged?.Invoke(this, new BossPhaseChangedEventArgs(3));
            }
            else if (hp < MaxHP / 3f * 2f && phase == 1) //Change to second phase when bots gets lowered to 666 HP
            {
                BossPhaseChanged?.Invoke(this, new BossPhaseChangedEventArgs(2));
            }
        }

        // private void Start()
        // {
        //     ActivateBoss();
        // }

        /// <summary>
        /// Default value is 1000; dies at ≤ 0;
        /// </summary>
        /// <value>Current amount of the boss' hitpoints</value>
        public float HitPoints
        {
            get { return hp; }
        }

        public bool Alive
        {
            get; private set;
        } = true;

        public float GetDamageTaken(float inLastSeconds)
        {
            float cumulativeDamage = 0;
            for (int i = damageLog.Count - 1; i >= 0; i--)
            {
                if (damageLog[i].Time + inLastSeconds >= Time.time)
                    cumulativeDamage += damageLog[i].Amount;
                else
                    break;
            }
            return cumulativeDamage;
        }

        #endregion

        #region Unity lifecycle
        private void Awake()
        {
            if (colliderObject.tag != TagDictionary.Boss)
            {
                throw new InvalidOperationException($"{nameof(BossController)} collider object should have tag {TagDictionary.Boss}!");
            }
            if (colliderObject.GetComponent<MeshCollider>() == null)
            {
                throw new InvalidOperationException($"{nameof(BossController)} does not have a mesh collider component");
            }
            animator = this.GetComponent<BossAnimator>();
            soundsProvider = GetComponentInChildren<BossSoundsProvider>();
            soundsPlayer = GetComponentInChildren<SoundsPlayer>();
            if (soundsProvider == null)
            {
                throw new InvalidOperationException($"{nameof(soundsProvider)} component could not be found!");
            }
            if (soundsPlayer == null)
            {
                throw new InvalidOperationException($"{nameof(soundsPlayer)} component could not be found!");
            }
            if (animator == null)
            {
                throw new InvalidOperationException($"{nameof(animator)} component could not be found!");
            }

            Gamemaster.Instance.Register(this);
            BossPhaseChanged += OnPhaseChanged;
            AttackInitialization();
            WeakpointInitialization();
        }
        #endregion

        #region Attacks
        private List<Attack> performedAttacks;
        private List<BossAttack> knownAttacks;
        private Type lastAttackType;
        private BossAnimator animator;
        [SerializeField]
        private float initialIdleTime = 0;
        [SerializeField]
        private bool stopAttacking;
        [SerializeField]
        private bool forceAttacking;
        [SerializeField]
        private int forceAttackIndex;

        private float idleTime, phase2IdleTime, phase3IdleTime;

        /// <summary>
        /// Finds all existing attackPerformers and creates attackClasses
        /// </summary>
        private void AttackInitialization()
        {
            BossGunAttackPerformer gunAttackPerformer = this.GetComponent<BossGunAttackPerformer>();
            gunAttackPerformer.AttackFinished += OnAttackFinished;
            BossEyeAttackPerformer eyeAttackPerformer = this.GetComponent<BossEyeAttackPerformer>();
            eyeAttackPerformer.AttackFinished += OnAttackFinished;
            ElectricAttackPerformer electricAttackPerformer = this.GetComponent<ElectricAttackPerformer>();
            electricAttackPerformer.AttackFinished += OnAttackFinished;
            BossBaseAttackPerformer baseAttackPerformer = this.GetComponent<BossBaseAttackPerformer>();
            baseAttackPerformer.AttackFinished += OnAttackFinished;

            knownAttacks = new List<BossAttack>();
            //Phase 1 Attacks:
            knownAttacks.Add(new GunSpreadshotAttack(gunAttackPerformer));  //0
            knownAttacks.Add(new GunSnipeAttack(gunAttackPerformer));       //1
            knownAttacks.Add(new EyeSlamAttack(eyeAttackPerformer));        //2
            knownAttacks.Add(new BouncyBallAttack(eyeAttackPerformer));     //3
            knownAttacks.Add(new DoNothingBeamAttack(eyeAttackPerformer));  //4
            knownAttacks.Add(new SawBladeAttack(baseAttackPerformer));      //5

            //Phase 2 Attacks:
            knownAttacks.Add(new EvilBeamAttack(eyeAttackPerformer));       //6
            knownAttacks.Add(new LALAAttack(eyeAttackPerformer));           //7
            knownAttacks.Add(new ElectricFieldAttack(electricAttackPerformer));//8
            knownAttacks.Add(new StompAttack(baseAttackPerformer));         //9

            //Phase 3 Attack:
            knownAttacks.Add(new JumpAttack(baseAttackPerformer));          //10
            performedAttacks = new List<Attack>();
        }

        /// <summary>
        /// Determine Usefullness of each attack(also the target(internal))
        /// and determine chosen attack with probabilities proportional to usefullness
        /// Usefullness is 0 if attack is still in cooldown
        /// </summary>
        /// <returns>AttackIndex</returns>
        private int ChooseAttack()
        {
            //Determine Usefullness of each attack
            float[] attackUsefullness = new float[knownAttacks.Count];
            float cumulativeUsefullness = 0;
            string debugAttackUsefullness = "AttackUsefullness: ";
            for (int i = 0; i < knownAttacks.Count; i++)
            {
                BossAttack attack = knownAttacks[i];
                if (GetTimeSinceLastAttack(attack.GetType()) < attack.Cooldown || !attack.Available)
                {
                    attackUsefullness[i] = 0;
                }
                else
                {
                    attackUsefullness[i] = attack.GetAttackUsefullness();
                }
                debugAttackUsefullness += "(" + i + ")" + Mathf.Floor(attackUsefullness[i]) + "; ";
                cumulativeUsefullness += attackUsefullness[i];
            }
            Debug.Log(debugAttackUsefullness);

            //Choose attackIndex
            float random = UnityEngine.Random.Range(0, cumulativeUsefullness);
            cumulativeUsefullness = 0;
            for (int i = 0; i < knownAttacks.Count; i++)
            {
                cumulativeUsefullness += attackUsefullness[i];
                if (cumulativeUsefullness >= random)
                {
                    return i;
                }
            }
            Debug.LogError("BossController.ChooseAttack: Could not decide on attack(Should not happen)!");
            return UnityEngine.Random.Range(0, knownAttacks.Count);
        }

        public void StopAttacking()
        {
            stopAttacking = true;
        }

        public void Reset()
        {
            phase = 0;
            BossPhaseChanged?.Invoke(this, new BossPhaseChangedEventArgs(0));
        }

        /// <summary>
        /// Start performing chosen attack and go into Cooldown
        /// </summary>
        /// <param name="attackIndex"></param>
        public void PerformAttack(int attackIndex)
        {
            lastAttackType = knownAttacks[attackIndex].GetType();
            Debug.Log("Perform Attack: " + lastAttackType);
            knownAttacks[attackIndex].PerformAttack();
        }

        private void OnAttackFinished(object sender, EventArgs args)
        {
            StartCoroutine(IdleAndChooseAttack(((BossAttackFinishedEventArgs)args).IdleTime + idleTime));
        }

        /// <summary>
        /// After finishing an attack boss idles and then starts his next attack
        /// </summary>
        /// <param name="idleDelay"></param>
        /// <returns></returns>
        IEnumerator IdleAndChooseAttack(float idleDelay)
        {
            Debug.Log("Start next attack");
            if (stopAttacking || !Alive)
            {
                Debug.Log("Stop attacking");
                yield break;
            }
            performedAttacks.Add(new Attack(lastAttackType, Time.time));
            Debug.Log("IDLE: " + idleDelay + "\n");

            if (idleDelay >= 15)
            {
                StartCoroutine(animator.IdleHeadUp(idleDelay));
            }
            float timer = 0;
            while (timer < idleDelay)
            {
                yield return new WaitForEndOfFrame();
                if (!Alive)
                {
                    yield break;
                }
                timer += Time.deltaTime;
                if (currentProtectionState == ProtectionState.Transform)
                {
                    soundsPlayer.PlaySound(soundsProvider.GetClip(BossSounds.ProtectionStart), false);
                    yield return StartCoroutine(animator.PerformUpgradeAnimation());
                    currentProtectionState = ProtectionState.Protected;
                    idleDelay = 3;
                    timer = 0;
                }
            }
            int attackIndex = 0;
            if (forceAttacking)
            {
                attackIndex = forceAttackIndex;
                Debug.Log("Attack Usefullness: " + knownAttacks[attackIndex].GetAttackUsefullness()); //maybe change to target aquired
            }
            else
            {
                attackIndex = ChooseAttack();
            }

            PerformAttack(attackIndex);
        }

        /// <summary>
        /// Gets time in seconds since last attack of attackType was performed
        /// </summary>
        /// <param name="attackType"></param>
        /// <returns></returns>
        private float GetTimeSinceLastAttack(Type attackType)
        {
            for (int i = performedAttacks.Count - 1; i >= 0; i--)
            {
                if (attackType == performedAttacks[i].AttackType)
                {
                    return Time.time - performedAttacks[i].Time;
                }
            }
            return float.MaxValue;
        }

        /// <summary>
        /// Activates attacks so they become usable
        /// </summary>
        /// <param name="startIndex">Included</param>
        /// <param name="endIndex">Included</param>
        private void ActivateAttacks(int startIndex, int endIndex)
        {
            for (int i = startIndex; i <= endIndex; i++)
            {
                knownAttacks[i].Available = true;
            }
        }

        /// <summary>
        /// Dectivates attacks
        /// </summary>
        /// <param name="startIndex">Included</param>
        /// <param name="endIndex">Included</param>
        private void DeactivateAttacks(int startIndex, int endIndex)
        {
            for (int i = startIndex; i <= endIndex; i++)
            {
                knownAttacks[i].Available = false;
            }
        }
        public GameObject GetDamageSource()
        {
            return this.gameObject;
        }
        #endregion

        #region Weakpoint
        private WeakPointController weakPointController;

        public WeakPointController GetWeakPointController()
        {
            return weakPointController;
        }

        private void WeakpointInitialization()
        {
            weakPointController = GetComponent<WeakPointController>();
            weakPointController.OnWeakpointActivated += OnWeakpointActivated;
            weakPointController.OnWeakpointDeactivated += OnWeakpointDeactivated;
        }

        public void OnWeakpointActivated(object sender, BossWeakPointEventArgs args)
        {

            soundsPlayer.PlaySound(soundsProvider.GetClip(BossSounds.ProtectionLiftUp), false);
            animator.LiftProtection(args.Side);
        }

        public void OnWeakpointDeactivated(object sender, BossWeakPointEventArgs args)

        {
            animator.LowerProtection(args.Side);
        }

        public void SubscribeWeakPoint(BossWeakPoint weakPoint, int id)
        {
            weakPointController.SubscribeWeakPoint(weakPoint, id);
        }

        public void SubscribeEyeWeakPoint(BossEyeWeakPoint eyeWeakPoint)
        {
            weakPointController.SubscribeEyeWeakPoint(eyeWeakPoint);
        }
        #endregion


        #region BossPhases

        private BossSoundsProvider soundsProvider;
        private SoundsPlayer soundsPlayer;
        private int phase = 0;

        public void ActivateBoss()
        {
            if (phase == 0)
            {
                if (Gamemaster.Instance.HardMode)
                {
                    Debug.Log("Start HardMode");
                    idleTime = 4;
                    phase2IdleTime = 2;
                    phase3IdleTime = 0;
                }
                else
                {
                    Debug.Log("Start EasyMode");
                    idleTime = 10;
                    phase2IdleTime = 5;
                    phase3IdleTime = 3;
                }
                phase = 1;
                BossPhaseChanged?.Invoke(this, new BossPhaseChangedEventArgs(1));
            }
        }

        public void SetToTut4Behaviour()
        {
            phase = 42;
            BossPhaseChanged?.Invoke(this, new BossPhaseChangedEventArgs(42));
        }

        public void SetToTut5Behaviour()
        {
            phase = 43;
            BossPhaseChanged?.Invoke(this, new BossPhaseChangedEventArgs(43));
        }

        public void SetToTut6Behaviour()
        {
            phase = 44;
            BossPhaseChanged?.Invoke(this, new BossPhaseChangedEventArgs(44));
        }

        public void SetToTut7Behaviour()
        {
            phase = 45;
            BossPhaseChanged?.Invoke(this, new BossPhaseChangedEventArgs(45));
        }

        private void OnPhaseChanged(object sender, EventArgs args)
        {
            phase = ((BossPhaseChangedEventArgs)args).Phase;
            //Phase 1 transition
            if (phase == 1)
            {

                soundsPlayer.PlaySound(soundsProvider.GetClip(BossSounds.Startup), false);
                Debug.Log("Starting L.A.M.P.exe... Destruction imminent");
                ActivateAttacks(0, 5);//Phase 1 Attack Initialization

                StartCoroutine(IdleAndChooseAttack(initialIdleTime)); //Start Attacking
            }
            //Phase 2 Transition
            else if (phase == 2)
            {
                currentProtectionState = ProtectionState.Transform;
                DeactivateAttacks(5, 5);
                ActivateAttacks(6, 9);//Phase 2 Attack Initialization
                idleTime = phase2IdleTime;
            }
            //Phase 3 Transition
            else if (phase == 3)
            {
                ActivateAttacks(10, 10);
            }
            //Tut4 Behaviour
            else if (phase == 42)
            {
                ActivateAttacks(6, 6);
                StartCoroutine(IdleAndChooseAttack(initialIdleTime));
            }
            //Tut5 Behaviour
            else if (phase == 43)
            {
                ActivateAttacks(8, 8);
                StartCoroutine(IdleAndChooseAttack(initialIdleTime));
            }
            //Tut7 Behaviour
            else if (phase == 45)
            {
                ActivateAttacks(7, 7);
                StartCoroutine(IdleAndChooseAttack(initialIdleTime));
            }
        }

        private void ProcessDeath()
        {
            Alive = false;
            BossDied?.Invoke(this, new BossDiedEventArgs(this));
            StartCoroutine(DeathAnimation());
        }

        private bool deathSafetyNet = false;
        private IEnumerator DeathAnimation()
        {
            animator.StartLala();
            animator.StunLala();
            EventHandler handler = null;
            bool animationDone = false;
            animator.OnEyeStunned += handler = (sender, args) =>
            {
                animationDone = true;
                animator.OnEyeStunned -= handler;
            };
            StartCoroutine(DeathTimer(15f));
            yield return new WaitUntil(() => animationDone || deathSafetyNet);
            BossDeathAnimationFinished?.Invoke(this, null);
        }

        private IEnumerator DeathTimer(float waitTime)
        {
            yield return new WaitForSeconds(waitTime);
            deathSafetyNet = true;
        }
        #endregion

        #region BossPosition
        private JumpableWall positionPoint;

        public JumpableWall GetPositionPoint()
        {
            return positionPoint;
        }

        public void SetPositionPoint(JumpableWall point)
        {
            if (positionPoint != null)
            {
                throw new InvalidOperationException($"Tried to call {nameof(BossController.SetPositionPoint)} while oldPositionPoint still registered");
            }
            positionPoint = point;
        }

        public void ReleasePositionPoint()
        {
            positionPoint?.Reappear();
            positionPoint = null;
        }

        public ElectricField GetElectricField()
        {
            return this.GetComponent<ElectricAttackPerformer>().ElectricField;
        }
        #endregion

        #region Bots AI utils
        [SerializeField]
        private GameObject colliderObject;
        private List<AttackToken> attackTokens;
        public List<AttackToken> GetAttackTokens()
        {
            if (attackTokens == null)
            {
                attackTokens = AttackToken.GenerateTokensAroundCollider(this.transform, colliderObject.GetComponent<Collider>(), ArenaData.AgentRadius, ArenaData.AgentRadius);
            }
            return attackTokens;
        }


        private void OnDrawGizmos()
        {
            var tokens = GetAttackTokens();
            foreach (var token in tokens)
            {
                if (token.State == TokenState.Open)
                {
                    Gizmos.DrawWireSphere(token.Position, .1f);
                }
                else
                {
                    Gizmos.DrawSphere(token.Position, .1f);
                }
            }
        }

        #endregion
    }
}
