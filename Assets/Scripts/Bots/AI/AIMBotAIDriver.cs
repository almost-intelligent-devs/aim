﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AIM.Bots.AI
{
    /// <summary>
    /// Responsible for giving control of the bot to the AI and asking the AI to yield the control back to player.
    /// Uses <see cref="BotMoodListener"/> to determine which behaviour should be activated after the player stopped
    /// controlling the bot.
    /// </summary>
    [RequireComponent(typeof(AIMBotController), typeof(BotMoodListener))]
    public class AIMBotAIDriver : MonoBehaviour
    {
        public event EventHandler BehaviourChanged;
        private AIMBotController botController;
        private BotMoodListener botMoodListener;
        public AIMBotAIBehaviour CurrentBehaviour { get; private set; }


        public void SetBehaviour(AIMBotAIBehaviour newBehaviour)
        {
            CurrentBehaviour?.Shutdown();
            CurrentBehaviour = newBehaviour;
            botController.Corrupted = newBehaviour.IsMalicious();
            if (!botController.ControlledByPlayer)
            {
                CurrentBehaviour.Act();
            }
            BehaviourChanged?.Invoke(this, new BotBehaviourChangedEventArgs(newBehaviour.GetCorrespondingColor(), newBehaviour));
        }

        private void Awake()
        {
            botController = GetComponent<AIMBotController>();
            if (botController == null)
            {
                throw new InvalidOperationException($"{nameof(botController)} is not specified!");
            }
            botMoodListener = GetComponent<BotMoodListener>();
            if (botMoodListener == null)
            {
                throw new InvalidOperationException($"{nameof(botMoodListener)} is not specified!");
            }
        }

        private void Start()
        {
            SetBehaviour(new ConfusedBehaviour(botController));
            // SetBehaviour(new EvilBehaviour(botController));
            // SetBehaviour(new EvilBehaviour(botController, 5f));
            // SetBehaviour(new StareAtPointBehaviour(botController, Gamemaster.Instance.transform, 5f));
            // SetBehaviour(new CowardBehaviour(botController));
            // SetBehaviour(new DiggerBehaviour(botController));
            // SetBehaviour(new AggressiveBehaviour(botController));
            // var coin = UnityEngine.Random.value;
            // if (coin > .8f)
            // {
            //     SetBehaviour(new ConfusedBehaviour(botController));

            // }
            // else
            // {
            //     SetBehaviour(new CowardBehaviour(botController));
            // }
            // if (coin > .66f)
            // {
            //     SetBehaviour(new ConfusedBehaviour(botController));
            // }
            // else if(coin > .33f)
            // {
            //     SetBehaviour(new CowardBehaviour(botController));
            // }
            // else
            // {
            //     SetBehaviour(new AggressiveBehaviour(botController));
            // }
        }

        private void OnEnable()
        {
            botController.TakenByThePlayer += OnTakenByThePlayer;
            botController.ReleasedByThePlayer += OnReleasedByThePlayer;
            botController.BotDied += OnBotDeath;
            botController.BotHpChanged += OnBotTakenDamage;
        }

        private void OnDisable()
        {
            botController.TakenByThePlayer -= OnTakenByThePlayer;
            botController.ReleasedByThePlayer -= OnReleasedByThePlayer;
            botController.BotDied -= OnBotDeath;
            botController.BotHpChanged -= OnBotTakenDamage;
        }

        private void OnBotDeath(object sender, EventArgs e)
        {
            CurrentBehaviour.Shutdown();
        }

        private void OnTakenByThePlayer(object sender, EventArgs e)
        {
            CurrentBehaviour.Suspend();
            //Reset taken damage counter
            accumulativeChange = 0f;
        }

        private void OnReleasedByThePlayer(object sender, EventArgs e)
        {
            CurrentBehaviour.Act();
        }


        float accumulativeChange;
        const float TriggerPoint = -2f;
        private void OnBotTakenDamage(object sender, EventArgs e)
        {
            //Currently evil behaviours can be spoooked - I think this adds gameplay depth as you can "beat your teammate into senses"
            if (!botController.ControlledByPlayer && botController.Alive && !(CurrentBehaviour is CowardBehaviour))
            {
                var args = (BotHpChangedEventArgs)e;
                accumulativeChange += args.Difference;
                if (accumulativeChange <= TriggerPoint)
                {
                    var modifier = Mathf.Abs(accumulativeChange / TriggerPoint);
                    accumulativeChange %= TriggerPoint;
                    var chance = modifier * GetCowardActivationChance();
                    Debug.Log("coward CHange" + chance);
                    //For corrupted the chance to get scared is lower
                    if (botController.Corrupted)
                    {
                        chance /= 10f;
                    }
                    if (UnityEngine.Random.value < chance)
                    {
                        SetBehaviour(new CowardBehaviour(botController, args.Source));
                    }
                }
            }
        }
        private float GetCowardActivationChance()
        {
            var x = botController.HitPoints;
            if (x > 70f)
            {
                return 0f;
            }
            else
            {
                //1/2 at 0hp, .2 at 30hp, .05 at 70 hp
                return 0.000061f * x * x - 0.01071f * x + 0.5f;
            }
        }
    }
}
