﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace AIM.Bots.AI
{
    public interface IPunchable
    {
        List<AttackToken> GetAttackTokens();
    }
}
