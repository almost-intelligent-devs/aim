﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

namespace AIM.Bots.AI
{
    public class AttackToken
    {
        private Func<Vector3> positionGetFunc;
        public GameObject Holder { get; private set; }
        public TokenState State { get; private set; }
        public GameObject Target { get; private set; }

        public AttackToken(GameObject target, Func<Vector3> positionGetter)
        {
            Target = target;
            positionGetFunc = positionGetter;
            State = TokenState.Open;
        }

        public void Seize(GameObject owner)
        {
            if (State != TokenState.Held)
            {
                Holder = owner;
                State = TokenState.Held;
            }
            else
            {
                Debug.LogError($"{owner.name} tried to seize the token that belongs to the {Holder.name}!");
            }
        }

        public void Release(GameObject owner)
        {
            if (owner != Holder)
            {
                throw new InvalidOperationException($"{owner.name} tried to relase the attack token even though the holder is {Holder.name}!");
            }
            Holder = null;
            State = TokenState.Open;
        }

        public Vector3 Position
        {
            get
            {
                return positionGetFunc();
            }
        }

        public static List<AttackToken> GenerateTokensAroundCollider(Transform origin, Collider collider, float enforcedDistance, float distToCollider, int startAngle = 0, int endAngle = 360, bool groundPos = true)
        {
            const float ProjectionSphereRadius = 20f;
            var attackTokens = new List<AttackToken>();
            for (int i = startAngle; i < endAngle; i += 10)
            {
                int angle = i;
                var token = new AttackToken(origin.gameObject, () =>
                {
                    var currPos = origin?.position ?? Vector3.zero;
                    var forward = origin?.forward ?? Vector3.forward;
                    if (groundPos)
                    {
                        currPos.y = 0f;
                    }
                    var pointOnCollider = collider.ClosestPoint(currPos + Quaternion.Euler(0, angle, 0) * forward * ProjectionSphereRadius);
                    pointOnCollider.y = 0f;
                    currPos.y = 0f;
                    var positionForBot = (pointOnCollider - currPos).normalized * distToCollider + pointOnCollider;
                    return positionForBot;
                });
                if (!attackTokens.Any(existing => Vector3.Distance(existing.Position, token.Position) < enforcedDistance))
                {
                    attackTokens.Add(token);
                }
            }
            return attackTokens;
        }
    }

    public static class AttackTokenExtensions
    {
        public static IPunchable GetPunchable(this GameObject obj)
        {
            IPunchable punchable = null;
            switch (obj.tag)
            {
                case TagDictionary.Boss: punchable = obj.GetComponent<Boss.BossController>() as IPunchable; break;
                case TagDictionary.Bot: punchable = obj.GetComponent<AIMBotController>() as IPunchable; break;
                case TagDictionary.Interactable: punchable = obj.GetComponent<Interactables.Interactable>() as IPunchable; break;
                default: Debug.Log($"Wasn't able to classify {obj.name} based on Tag!"); break;
            }
            if (punchable == null)
            {
                Debug.Log($"NULL FOR {obj.name}");
            }
            return punchable;
        }

        public static AttackToken GetClosestTokenOnNavMesh(this IEnumerable<AttackToken> tokens, Vector3 position)
        {
            NavMeshHit stub = new NavMeshHit();
            var minDistance = float.MaxValue;
            AttackToken closestToken = null;
            foreach (var token in tokens.Where(token => token.State != TokenState.Invalid && NavMesh.SamplePosition(token.Position, out stub, .5f, NavMesh.AllAreas)))
            {
                var dist = Vector3.Distance(token.Position, position);
                if (dist < minDistance)
                {
                    minDistance = dist;
                    closestToken = token;
                }
            }
            return closestToken;
        }
    }

    public enum TokenState
    {
        Open,
        Held,
        Invalid
    }
}
