﻿using AIM.Bots.AI;
using System;
using UnityEngine;

namespace AIM.Bots
{
    [RequireComponent(typeof(PlaceholderBotAppearance))]
    public class BehaviourBasedAppearanceChanger : MonoBehaviour
    {
        private AIMBotAIDriver driver;

        private PlaceholderBotAppearance appearanceController;


        private void Awake()
        {
            appearanceController = GetComponent<PlaceholderBotAppearance>();
            if (appearanceController == null)
            {
                throw new InvalidOperationException($"{nameof(appearanceController)} is not specified!");
            }

            driver = GetComponent<AIMBotAIDriver>();
        }

        private void OnEnable()
        {
            if (driver != null)
            {
                driver.BehaviourChanged += OnBehaviourChanged;
            }
        }

        private void OnDisable()
        {
            if (driver != null)
            {
                driver.BehaviourChanged -= OnBehaviourChanged;
            }
        }

        private void OnBehaviourChanged(object sender, EventArgs e)
        {
            var args = (BotBehaviourChangedEventArgs)e;
            appearanceController.SetHeadColor(args.Color);
        }
    }
}
