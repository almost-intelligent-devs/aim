﻿using UnityEngine;

namespace AIM.Bots.AI
{
    public static class BotMoodColor
    {
        public static readonly Color Default = new Color(1f, 1f, 1f, .5f);
        public static readonly Color Aggressive = new Color(1f, .5f, .5f, .5f);
        public static readonly Color Confused = new Color(.5f, .5f, 1f, .5f);
        public static readonly Color Coward = new Color(1f, 1f, 0f, .5f);
        public static readonly Color Evil = new Color(0f, 0f, 0f, 1f);
        public static readonly Color Staring = new Color(1f, 1f, 1f, .5f);
        public static readonly Color Digger = new Color(.2f, .62f, .012f, .42f);
    }
}
