﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AIM.Bots.AI
{
    public class BotBehaviourChangedEventArgs : EventArgs
    {
        public Color Color { get; private set; }
        public AIMBotAIBehaviour Behaviour { get; private set; }

        public BotBehaviourChangedEventArgs(Color color, AIMBotAIBehaviour behaviour)
        {
            Color = color;
            Behaviour = behaviour;
        }
    }
}
