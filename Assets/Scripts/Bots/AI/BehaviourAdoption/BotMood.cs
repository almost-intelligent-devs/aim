﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AIM.Bots.AI
{
    public enum BotMood
    {
        Undefined,
        Coward,
        Aggressive,
        Confused,
        Digger,
        Evil,
        Staring
    }

    public static class BotMoodExtension
    {
        public static AIMBotAIBehaviour GetBehaviour(this BotMood mood, AIMBotController controller, GameObject extra = null)
        {
            switch (mood)
            {
                case BotMood.Coward: return new CowardBehaviour(controller);
                case BotMood.Aggressive: return extra == null ? new AggressiveBehaviour(controller) : new AggressiveBehaviour(controller, extra);
                case BotMood.Confused: return new ConfusedBehaviour(controller);
                case BotMood.Digger: return new DiggerBehaviour(controller);
                default: throw new InvalidOperationException($"Unknown {nameof(BotMood)} value of {mood}");
            }
        }

        public static BotMood GetMood(this AIMBotAIBehaviour behaviour)
        {
            if (behaviour is CowardBehaviour)
            {
                return BotMood.Coward;
            }
            else if (behaviour is AggressiveBehaviour && !(behaviour is EvilBehaviour))
            {
                return BotMood.Aggressive;
            }
            else if(behaviour is DiggerBehaviour)
            {
                return BotMood.Digger;
            }
            return BotMood.Undefined;
        }
    }
}
