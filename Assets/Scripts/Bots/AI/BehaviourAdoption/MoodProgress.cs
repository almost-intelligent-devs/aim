﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AIM.Bots.AI
{
    public class MoodProgress
    {
        /// <summary>
        /// Current progress for the bot's mood
        /// </summary>
        /// <value>progress, from 0 to 100</value>
        public float Progress
        {
            get

            {
                return progress;
            }
            set
            {
                progress = Mathf.Clamp(value, 0f, 100f);
            }
        }
        private float progress;

        public BotMood Mood { get; private set; }

        public MoodProgress(BotMood mood)
        {
            Mood = mood;
            Progress = 0f;
        }

        public void ResetProgress()
        {
            if (Mood != BotMood.Confused)
            {
                Progress = 0f;
            }
        }
    }
}
