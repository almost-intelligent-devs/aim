﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using AIM.Editor;
using AIM.Sound;
using UnityEngine;
using UnityEngine.AI;

namespace AIM.Bots.AI
{
    /*
     Behaviours to choose from:
         Coward
         Aggressive
         Confused


         Coward:
         *didn't damage the boss*
             was hit by N damage in the last N seconds (later for hero!)
             has ran away from the boss by N units
             has hidden from the boss behind the wall

         Aggressive:
             interacted with something
             hit something more than N times in the last N seconds - no need to hit, just an action is enough!
             when actually hit the target -> set it as a target for aggression!




         idea:
             Each behaviour has it's own progressbar that changes the value based on player's action.
             if the progress is > 70%, then it can be adopted. The biggest progress wins.

             when the player takes the control of the bot, the current behaviour (if not confused) is assigned 90 points, the rest is 0.

    */



    /// <summary>
    /// Responsible for listening to the player's input and choosing the proper adopted behaviour
    /// </summary>
    [RequireComponent(typeof(AIMBotController))]
    public class BotMoodListener : MonoBehaviour
    {
        [SerializeField]
        private float decayPerTick = 1f;
        [SerializeField]
        private float changeForBreakingLoS = 50f;
        [SerializeField]
        private float changeForChangingDistance = 5f;
        [SerializeField]
        private float increasePerDamage = 5f;
        [SerializeField]
        private float increasePerAction = 15f;
        private const float ChangeThreshold = 70f;
        private const float BaseValueForExistingBehaviour = 90f;
        private AIMBotController botController;

        private GameObject extraBehaviourAttribute;

        public event EventHandler MoodChanged;

        private MoodProgress cowardProgress;
        private MoodProgress aggressiveProgress;
        private MoodProgress diggerProgress;

        private List<MoodProgress> moodPorgresses;

        private CoroutineCancellationToken trackingCancellationToken;

        private BotMood currentMood;
        #region Show progresses in editor
#if UNITY_EDITOR
        [ReadOnly] public float CowardProgress;
        [ReadOnly] public float AggressiveProgress;
        [ReadOnly] public float DiggerProgress;


        private void OnEnable()
        {
            UpdateValuesForEditor();
        }

        private void OnValidate()
        {
            UpdateValuesForEditor();
        }

        private void Update()
        {
            UpdateValuesForEditor();

        }

        private void UpdateValuesForEditor()
        {
            CowardProgress = cowardProgress?.Progress ?? 0f;
            AggressiveProgress = aggressiveProgress?.Progress ?? 0f;
            DiggerProgress = diggerProgress?.Progress ?? 0f;
        }
#endif
        #endregion

        private void Awake()
        {
            botController = GetComponent<AIMBotController>();
            if (botController == null)
            {
                throw new InvalidOperationException($"{nameof(botController)} is not specified!");
            }

            cowardProgress = new MoodProgress(BotMood.Coward);
            aggressiveProgress = new MoodProgress(BotMood.Aggressive);
            diggerProgress = new MoodProgress(BotMood.Digger);
            moodPorgresses = new List<MoodProgress> { cowardProgress, aggressiveProgress, diggerProgress };
        }

        private void Start()
        {
            botController.TakenByThePlayer += OnPlayerTakenControl;
            botController.ReleasedByThePlayer += OnPlayerReleased;
        }


        private void OnBotDamaged(object sender, EventArgs e)
        {

            var args = (BotHpChangedEventArgs)e;
            cowardProgress.Progress -= args.Difference * increasePerDamage;
        }

        private void OnAttackPerformed(object sender, EventArgs e)
        {
            aggressiveProgress.Progress += increasePerAction;
        }

        private void OnDiggedPerformed(object sender, EventArgs e)
        {
            const float IncreaseRate = 100f / ArenaData.DiggingTime;
            diggerProgress.Progress += IncreaseRate * Time.deltaTime;
        }

        private void OnPlayerTakenControl(object sender, EventArgs e)
        {
            currentMood = botController.CurrentBehaviour.GetMood();
            SubscribeToEvents();
            ResetProgresses();
            trackingCancellationToken = new CoroutineCancellationToken();
            StartCoroutine(DecayProgress(trackingCancellationToken));
            if (Gamemaster.Instance.GetBoss() != null)
            {
                StartCoroutine(TrackPlayerMovement(trackingCancellationToken));
            }
            StartCoroutine(CheckMoodChange(trackingCancellationToken));
        }

        private void OnPlayerReleased(object sender, EventArgs e)
        {
            trackingCancellationToken.CancellationRequested = true;
            UnsubscribeFromEvents();
        }

        private void ResetProgresses()
        {
            foreach (var progress in moodPorgresses)
            {
                progress.Progress = progress.Mood == botController.CurrentBehaviour.GetMood() ? BaseValueForExistingBehaviour : 0f;
            }
        }

        private void SubscribeToEvents()
        {
            botController.AttackedOrUsedItem += OnAttackPerformed;
            botController.BotHpChanged += OnBotDamaged;
            botController.DiggedThisFrame += OnDiggedPerformed;
        }

        private void UnsubscribeFromEvents()
        {
            botController.AttackedOrUsedItem -= OnAttackPerformed;
            botController.BotHpChanged -= OnBotDamaged;
            botController.DiggedThisFrame -= OnDiggedPerformed;
        }

        private IEnumerator DecayProgress(CoroutineCancellationToken cancellationToken)
        {
            const float TickDelay = 1f;
            while (!cancellationToken.CancellationRequested)
            {
                moodPorgresses.ForEach(progress => progress.Progress -= decayPerTick);
                yield return new WaitForSeconds(TickDelay);
            }
        }

        private IEnumerator CheckMoodChange(CoroutineCancellationToken cancellationToken)
        {
            while (!cancellationToken.CancellationRequested && !botController.Corrupted)
            {
                var curr = GetCurrentMood();
                if (curr != currentMood)
                {
                    currentMood = curr;
                    if (!botController.Corrupted)
                    {
                        botController.SetAIBehaviour(currentMood.GetBehaviour(botController, extraBehaviourAttribute));
                    }
                }
                yield return null;
            }
        }

        private IEnumerator TrackPlayerMovement(CoroutineCancellationToken cancellationToken)
        {
            const float PositionChangeDelay = 1f;
            var boss = Gamemaster.Instance.GetBoss();
            var bossPosition = boss.transform.position;
            bossPosition.y = ArenaData.LoSHeight;
            var botPosition = botController.transform.position;
            botPosition.y = ArenaData.LoSHeight;
            var toBoss = bossPosition - botPosition;
            var sqrDistToTheBoss = toBoss.sqrMagnitude;
            RaycastHit hit;
            bool bossWasVisible = false;
            if (Physics.Raycast(botPosition, toBoss, out hit))
            {
                if (hit.collider.gameObject.tag == TagDictionary.Boss)
                {
                    bossWasVisible = true;
                }
            }
            yield return new WaitForSeconds(PositionChangeDelay);

            while (!cancellationToken.CancellationRequested)
            {
                bossPosition = boss.transform.position;
                bossPosition.y = ArenaData.LoSHeight;
                botPosition = botController.transform.position;
                botPosition.y = ArenaData.LoSHeight;
                toBoss = bossPosition - botPosition;


                if (Physics.Raycast(botPosition, toBoss, out hit))
                {
                    if (hit.collider.gameObject.tag == TagDictionary.Boss)
                    {
                        if (!bossWasVisible)
                        {
                            cowardProgress.Progress -= changeForBreakingLoS;
                        }
                        bossWasVisible = true;
                    }
                    else
                    {
                        if (bossWasVisible)
                        {
                            cowardProgress.Progress += changeForBreakingLoS;
                        }
                        bossWasVisible = false;
                    }
                }

                if (bossWasVisible)
                {
                    var newSqrDistToTheBoss = toBoss.sqrMagnitude;
                    if (newSqrDistToTheBoss > sqrDistToTheBoss)
                    {
                        cowardProgress.Progress += changeForChangingDistance;
                    }
                    else
                    {
                        cowardProgress.Progress -= changeForChangingDistance;
                    }
                    sqrDistToTheBoss = newSqrDistToTheBoss;
                }
                yield return new WaitForSeconds(PositionChangeDelay);
            }
        }

        public AIMBotAIBehaviour GetCurrentBehaviour()
        {
            return GetCurrentMood().GetBehaviour(botController, extraBehaviourAttribute);
        }

        private BotMood GetCurrentMood()
        {
            var highestProgress = moodPorgresses.OrderByDescending(progress => progress.Progress).First();
            if (highestProgress.Progress > ChangeThreshold)
            {
                return highestProgress.Mood;
            }
            else
            {
                return BotMood.Confused;
            }
        }
    }
}
