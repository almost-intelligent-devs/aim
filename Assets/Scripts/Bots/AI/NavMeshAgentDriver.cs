﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace AIM.Bots.AI
{
    [RequireComponent(typeof(NavMeshAgent))]
    public class NavMeshAgentDriver : MonoBehaviour
    {
        private NavMeshAgent agent;
        private Vector3 previousPosition;

        private const float StuckTimeout = 10f;
        private bool suspectingStuck = false;
        private void Awake()
        {
            agent = GetComponent<NavMeshAgent>();
            if (agent == null)
            {
                throw new InvalidOperationException($"{nameof(agent)} is not specified!");
            }
            agent.speed = 2f;
        }

        private void Update()
        {
            var currentPosition = transform.position;
            currentPosition.y = 0f;
            var speed = currentPosition - previousPosition;
            previousPosition = currentPosition;

            // Pull agent towards character
            if (!agent.isStopped)
            {
                var worldDeltaPosition = agent.nextPosition - transform.position;
                var magintude = worldDeltaPosition.magnitude;
                if (magintude > agent.radius)
                {
                    agent.nextPosition = transform.position + 0.9f * worldDeltaPosition;
                }
                else if (magintude < agent.radius / 2f)
                {
                    agent.nextPosition = transform.position + speed.normalized * agent.radius;
                }

                if (IsNonWalkableAhead() && !suspectingStuck)
                {
                    StartCoroutine(SuspectStuck());
                }
            }
        }

        private void Start()
        {
            agent.updatePosition = false;
            previousPosition = transform.position;
            previousPosition.y = 0f;
        }

        private void OnEnable()
        {
            previousPosition = transform.position;
            previousPosition.y = 0f;
            agent.Warp(previousPosition);
        }

        public Vector3 GetDirection()
        {
            if (agent.isStopped)
            {
                agent.isStopped = false;
            }
            var direction = (agent.nextPosition - transform.position);
            direction.y = 0f;
            return direction.normalized;
        }

        public bool ReachedDestination()
        {
            var distance = (agent.destination - transform.position).magnitude;
            return distance < agent.radius;
        }

        public void SetDestination(Vector3 desiredWorldPosition, bool updateAgent = true)
        {
            var currPos = transform.position;
            var agPos = agent.nextPosition;
            desiredWorldPosition.y = 0f;
            agPos.y = 0f;
            currPos.y = 0f;
            agent.SetDestination(desiredWorldPosition);
            if (updateAgent)
            {
                agent.nextPosition = transform.position + (desiredWorldPosition - currPos).normalized * agent.radius;
            }
        }

        public void Stop()
        {
            agent.isStopped = true;
            agent.ResetPath();
        }

        private IEnumerator SuspectStuck()
        {
            suspectingStuck = true;
            var timeElapsed = 0f;
            while (IsNonWalkableAhead() && timeElapsed < StuckTimeout)
            {
                yield return null;
                timeElapsed += Time.deltaTime;
            }
            if (!(timeElapsed < StuckTimeout))
            {
                agent.Warp(transform.position);
            }
            suspectingStuck = false;
        }

        private bool IsNonWalkableAhead()
        {
            var agentPos = agent.nextPosition;
            agentPos.y = 0f;
            NavMeshHit hit;
            return NavMesh.Raycast(transform.position, agentPos, out hit, NavMesh.AllAreas);
        }

        public static Vector3? GetPositionOnNavMesh(Vector3 centerOfSearch, float radius)
        {
            Vector3 randomDirection = UnityEngine.Random.insideUnitSphere * radius;
            randomDirection.y = 0f;
            randomDirection += centerOfSearch;
            NavMeshHit hit;
            Vector3? finalPosition = null;
            if (NavMesh.SamplePosition(randomDirection, out hit, radius, NavMesh.AllAreas))
            {
                finalPosition = hit.position;
            }
            return finalPosition;
        }

        public Vector3? GetPositionOnNavMesh(float radius)
        {
            return GetPositionOnNavMesh(transform.position, radius);
        }

        #region static
        public static Vector3? GetClosestPoint(Vector3 target)
        {
            const float AcceptedError = 10f;
            NavMeshHit hit;
            if (NavMesh.SamplePosition(target, out hit, AcceptedError, NavMesh.AllAreas))
            {
                return hit.position;
            }
            else
            {
                return null;
            }
        }

        public static Vector3 GetRandomWalkablePosition()
        {
            Debug.Log("GetRandomWalkablePosition called! (should happen super-rare!");
            return GetGuaranteedPositionOnNavMesh(Vector3.zero, 20f);
        }

        public static Vector3 GetGuaranteedPositionOnNavMesh(Vector3 centerOfSearch, float radius)
        {
            return NavMeshAgentDriver.GetPositionOnNavMesh(centerOfSearch, radius) ?? NavMeshAgentDriver.GetRandomWalkablePosition();
        }
        #endregion

    }
}
