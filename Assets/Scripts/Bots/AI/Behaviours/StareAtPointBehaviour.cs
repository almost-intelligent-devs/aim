﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AIM.Bots.AI
{
    public class StareAtPointBehaviour : AIMBotAIBehaviour
    {
        private float timeout;
        public StareAtPointBehaviour(AIMBotController controller, Transform target) : base(controller)
        {
            TargetToStareAt = target;
            timeout = 0f;
        }

        public StareAtPointBehaviour(AIMBotController controller, Transform target, float timeout) : base(controller)
        {
            TargetToStareAt = target;
            this.timeout = timeout;
        }

        /// <summary>
        /// The transform of the object that the bot is suppsed to track with its rotation
        /// </summary>
        public Transform TargetToStareAt { get; set; }

        private CoroutineCancellationToken cancellationToken;

        public override void Act()
        {
            if (!IsActive)
            {
                cancellationToken = new CoroutineCancellationToken();
                controller.StartCoroutine(FollowPoint(cancellationToken));
                if (!Mathf.Approximately(timeout, 0f))
                {
                    controller.StartCoroutine(ConfuseOnTimeout(cancellationToken));
                }
            }
        }

        private IEnumerator ConfuseOnTimeout(CoroutineCancellationToken cancellationToken)
        {
            float timeElapsed = 0f;
            while (!cancellationToken.CancellationRequested && timeElapsed < timeout)
            {
                yield return null;
                timeElapsed += Time.deltaTime;
            }
            if (!cancellationToken.CancellationRequested)
            {
                controller.SetAIBehaviour(new ConfusedBehaviour(controller));
                Debug.Log($"{nameof(StareAtPointBehaviour)} for {controller.gameObject.name} timed out out after {timeout} seconds");
            }
        }

        public override void Shutdown()
        {
            if (cancellationToken != null)
            {
                cancellationToken.CancellationRequested = true;
            }
        }

        public override void Suspend()
        {
            Shutdown();
        }

        IEnumerator FollowPoint(CoroutineCancellationToken cancellationToken)
        {
            IsActive = true;
            while (!cancellationToken.CancellationRequested)
            {
                var direction = TargetToStareAt.position - controller.transform.position;
                float angle = Mathf.Atan2(direction.z, direction.x) * Mathf.Rad2Deg - 90f;
                controller.RotateAroundY(angle);
                yield return null;
            }
            IsActive = false;
        }

        public override Color GetCorrespondingColor()
        {
            return BotMoodColor.Staring;
        }

        public override bool IsMalicious()
        {
            return true;
        }
    }
}
