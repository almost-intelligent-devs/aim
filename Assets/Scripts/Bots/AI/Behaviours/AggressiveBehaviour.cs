﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using AIM.Items;
using UnityEngine;
using UnityEngine.AI;

namespace AIM.Bots.AI
{
    public class AggressiveBehaviour : AIMBotAIBehaviour
    {
        protected GameObject aggressionTarget;
        protected IPunchable currentPunchable;
        protected AttackToken desiredToken;

        public AggressiveBehaviour(AIMBotController controller) : base(controller)
        {
        }
        public AggressiveBehaviour(AIMBotController controller, GameObject target) : base(controller)
        {
            aggressionTarget = target;
        }

        private const float InactiveTimeToChangeTarget = 3f;
        private const float InactiveTimeToCalmDown = 100f;

        protected float timeSinceTargetChanged = 0f;
        private float timeSinceAction = 0f;

        protected CoroutineCancellationToken cancellationToken;

        public GameObject GetAggressionTarget() { return aggressionTarget; }

        public override void Act()
        {
            if (!IsActive)
            {
                aggressionTarget = null;
                currentPunchable = null;
                desiredToken = null;
                cancellationToken = new CoroutineCancellationToken();
                controller.StartCoroutine(Acting(cancellationToken));
                controller.StartCoroutine(NoAttackTimer(cancellationToken));
                controller.StartCoroutine(NoTargetTimer(cancellationToken));
            }
        }

        public override void Shutdown()
        {
            if (cancellationToken != null)
            {
                cancellationToken.CancellationRequested = true;
            }
        }

        public override void Suspend()
        {
            Shutdown();
        }

        private IEnumerator NoAttackTimer(CoroutineCancellationToken cancellationToken)
        {
            while (!cancellationToken.CancellationRequested)
            {
                if (timeSinceTargetChanged > InactiveTimeToChangeTarget && timeSinceAction > InactiveTimeToChangeTarget)
                {
                    FindNextTarget();
                }
                yield return null;
                timeSinceTargetChanged += Time.deltaTime;
            }
        }

        private IEnumerator NoTargetTimer(CoroutineCancellationToken cancellationToken)
        {
            while (!cancellationToken.CancellationRequested)
            {
                if (timeSinceAction > InactiveTimeToCalmDown)
                {
                    controller.SetAIBehaviour(new ConfusedBehaviour(controller));
                }
                yield return null;
                timeSinceAction += Time.deltaTime;
            }
        }

        protected virtual void FindNextTarget()
        {
            timeSinceTargetChanged = 0f;

            List<GameObject> targets = new List<GameObject>();
            targets.AddRange(Gamemaster.Instance.GetAIMBots().Where(bot => bot != controller && bot.Alive).Select(bot => bot.gameObject));
            var boss = Gamemaster.Instance.GetBoss();
            if (boss != null && boss.Alive)
            {
                targets.Add(boss.gameObject);
            }
            targets.AddRange(Gamemaster.Instance.GetInteractables().Select(interactable => interactable.gameObject));

            var tokens = new List<AttackToken>();
            foreach (var target in targets)
            {
                tokens.AddRange(target.GetPunchable()?.GetAttackTokens());
            }

            var currentToken = tokens.GetClosestTokenOnNavMesh(controller.transform.position);
            desiredToken = null;
            if (currentToken == null)
            {
                Debug.Log("Wasn't able to find closest token: received NULL");
                aggressionTarget = null;
                currentPunchable = null;
                return;
            }

            aggressionTarget = currentToken.Target;
            currentPunchable = aggressionTarget.GetPunchable();

        }

        private IEnumerator Acting(CoroutineCancellationToken cancellationToken)
        {
            IsActive = true;
            while (!cancellationToken.CancellationRequested)
            {
                if (!BehaviourRoutines.IsAlive(aggressionTarget))
                {
                    FindNextTarget();
                    if (aggressionTarget == null)
                    {
                        //TODO: "look" for targets AKA wait for available target
                        controller.SetAIBehaviour(new ConfusedBehaviour(controller));
                        IsActive = false;
                        yield break;
                    }
                }
                while (!cancellationToken.CancellationRequested && BehaviourRoutines.IsAlive(aggressionTarget))
                {
                    if (BehaviourRoutines.IsInRange(controller, aggressionTarget, desiredToken))
                    {
                        yield return Attack(cancellationToken);
                    }
                    else
                    {
                        yield return GetInRange(cancellationToken);
                    }
                }
            }
            IsActive = false;
        }

        private IEnumerator Attack(CoroutineCancellationToken cancellationToken)
        {
            if (BehaviourRoutines.IsCurrentlyMelee(controller))
            {
                yield return MeleeAttack(cancellationToken);
            }
            else
            {
                yield return BehaviourRoutines.RangedAttack(controller, aggressionTarget, () => timeSinceAction = 0f, cancellationToken);
            }
        }

        private IEnumerator GetInRange(CoroutineCancellationToken cancellationToken)
        {
            const float RepathDelay = 1 / 60f;
            var timeSinceRepath = float.MaxValue;
            var targetChanged = true;
            while (!cancellationToken.CancellationRequested && !BehaviourRoutines.IsInRange(controller, aggressionTarget, desiredToken))
            {
                if (BehaviourRoutines.IsCurrentlyMelee(controller))// && desiredToken == null)
                {
                    desiredToken = currentPunchable.GetAttackTokens().GetClosestTokenOnNavMesh(controller.transform.position);
                }
                //TRY TO GET THE FREE TOKEN IN THE CHAIN OF TOKENS!
                while (desiredToken != null)
                {
                    if (desiredToken.State != TokenState.Open)
                    {
                        currentPunchable = desiredToken.Holder.GetPunchable();
                        aggressionTarget = desiredToken.Holder;
                        desiredToken = currentPunchable.GetAttackTokens().GetClosestTokenOnNavMesh(controller.transform.position);
                        targetChanged = true;
                    }
                    else
                    {
                        break;
                    }
                }

                var desiredPosition = desiredToken?.Position ?? aggressionTarget.transform.position;

                if (controller.NavigationDriver.ReachedDestination() || timeSinceRepath >= RepathDelay || targetChanged)
                {
                    controller.NavigationDriver.SetDestination(NavMeshAgentDriver.GetClosestPoint(desiredPosition) ?? NavMeshAgentDriver.GetRandomWalkablePosition(), targetChanged);
                    timeSinceRepath = 0f;
                }

                controller.RunTowards(controller.NavigationDriver.GetDirection());
                yield return null;
                timeSinceRepath += Time.deltaTime;
                targetChanged = false;
            }
            controller.NavigationDriver.Stop();
        }

        private void CheckTargetIsAlive()
        {
            if (!BehaviourRoutines.IsAlive(aggressionTarget))
            {
                FindNextTarget();
            }
        }

        private IEnumerator MeleeAttack(CoroutineCancellationToken cancellationToken)
        {

            controller.NavigationDriver.Stop();
            if (desiredToken != null && desiredToken.State == TokenState.Open)
            {
                desiredToken.Seize(controller.gameObject);
            }
            else
            {
                yield break;
            }
            while (!cancellationToken.CancellationRequested && BehaviourRoutines.IsInRange(controller, aggressionTarget, desiredToken))
            {
                var dirToTarget = aggressionTarget.transform.position - controller.transform.position;
                float angle = Mathf.Atan2(dirToTarget.z, dirToTarget.x) * Mathf.Rad2Deg - 90f;
                controller.RotateAroundY(angle + BehaviourRoutines.DiscplacementAngleForMelee);
                controller.UseHeldItem();
                timeSinceAction = 0f;
                yield return null;
                CheckTargetIsAlive();
            }
            //null when the target was killed
            desiredToken?.Release(controller.gameObject);
        }

        public override Color GetCorrespondingColor()
        {
            return BotMoodColor.Aggressive;
        }

        public override bool IsMalicious()
        {
            return false;
        }
    }
}
