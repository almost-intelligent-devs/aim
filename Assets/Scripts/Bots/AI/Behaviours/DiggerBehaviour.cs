﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using AIM.Environment;
using UnityEngine;

namespace AIM.Bots.AI
{
    public class DiggerBehaviour : AIMBotAIBehaviour
    {
        public DiggerBehaviour(AIMBotController controller) : base(controller)
        {
        }


        public override bool IsMalicious()
        {
            return false;
        }

        private CoroutineCancellationToken cancellationToken;

        public override void Shutdown()
        {
            if (cancellationToken != null)
            {
                cancellationToken.CancellationRequested = true;
            }
        }

        public override void Suspend()
        {
            //This behaviour doesn't try to act when the player has the control
            Shutdown();
        }

        public override void Act()
        {
            if (!IsActive)
            {
                cancellationToken = new CoroutineCancellationToken();
                controller.StartCoroutine(Acting(cancellationToken));
            }
        }
        public override Color GetCorrespondingColor()
        {
            return BotMoodColor.Digger;
        }

        private IEnumerator Acting(CoroutineCancellationToken cancellationToken)
        {
            IsActive = true;
            while (!cancellationToken.CancellationRequested)
            {
                if (controller.HeldItem != null)
                {
                    yield return ShareItem(cancellationToken);
                }
                else
                {
                    if (!Gamemaster.Instance.GetDiggingSpots().Any())
                    {
                        yield return WaitForSpot(cancellationToken);
                    }
                    else
                    {
                        yield return GetToTheDiggingPlace(cancellationToken);
                    }
                }
            }
            IsActive = false;
        }

        private IEnumerator GetToTheDiggingPlace(CoroutineCancellationToken cancellationToken)
        {
            var closestPlace = Gamemaster.Instance.GetDiggingSpots().OrderBy(spot => Vector3.Distance(controller.transform.position, spot.transform.position)).First();
            var destination = closestPlace.transform.position;
            controller.NavigationDriver.SetDestination(destination);
            while (!cancellationToken.CancellationRequested && closestPlace != null && !controller.NavigationDriver.ReachedDestination() && controller.HeldItem == null)
            {
                controller.RunTowards(controller.NavigationDriver.GetDirection());
                yield return null;
            }
            if (cancellationToken.CancellationRequested)
            {
                yield break;
            }
            if (controller.HeldItem != null)
            {
                yield return ShareItem(cancellationToken);
            }
            else if (controller.NavigationDriver.ReachedDestination())
            {
                yield return Dig(cancellationToken, closestPlace);
            }
        }

        private IEnumerator Dig(CoroutineCancellationToken cancellationToken, DiggingSpot spot)
        {
            while (!cancellationToken.CancellationRequested && spot != null)
            {
                controller.TryToDig();
                yield return null;
            }
        }

        private IEnumerator ShareItem(CoroutineCancellationToken cancellationToken)
        {
            var closestBot = Gamemaster.Instance.GetAIMBots().Where(bot => bot != controller && bot.Alive).OrderBy(bot => Vector3.Distance(bot.transform.position, controller.transform.position)).FirstOrDefault();
            if (closestBot == null)
            {
                //If we're the last bot standing, there's no one to share with. Confused!
                controller.SetAIBehaviour(new ConfusedBehaviour(controller));
                yield break;
            }
            controller.ThrowItem();
            const float timeToThrow = 1.5f;
            var elapsedTime = 0f;
            while (!cancellationToken.CancellationRequested && elapsedTime < timeToThrow)
            {
                var dirToTarget = closestBot.transform.position - controller.transform.position;
                float angle = Mathf.Atan2(dirToTarget.z, dirToTarget.x) * Mathf.Rad2Deg - 90f;
                controller.RotateAroundY(angle);
                yield return null;
                elapsedTime += Time.deltaTime;
            }
        }

        private IEnumerator WaitForSpot(CoroutineCancellationToken cancellationToken)
        {
            const float WaitingTime = 5f;
            float elapsedTime = 0f;
            while (!cancellationToken.CancellationRequested && elapsedTime < WaitingTime && !Gamemaster.Instance.GetDiggingSpots().Any())
            {
                yield return null;
                elapsedTime += Time.deltaTime;
            }
            if (cancellationToken.CancellationRequested)
            {
                yield break;
            }

            if (!Gamemaster.Instance.GetDiggingSpots().Any())
            {
                controller.SetAIBehaviour(new ConfusedBehaviour(controller));
            }
        }
    }
}
