﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using AIM.Items;
using UnityEngine;
using UnityEngine.AI;

namespace AIM.Bots.AI
{
    public class EvilBehaviour : AIMBotAIBehaviour
    {
        private float timeout;
        private CoroutineCancellationToken cancellationToken;
        private CoroutineCancellationToken struggleCancellationToken;
        protected GameObject aggressionTarget;
        protected IPunchable currentPunchable;
        protected AttackToken desiredToken;

        private const float InactiveTimeToChangeTarget = 3f;
        protected float timeSinceTargetChanged = 0f;
        private float timeSinceAction = 0f;
        private bool IsStruggling { get; set; }

        public EvilBehaviour(AIMBotController controller) : base(controller)
        {
            timeout = 0f;
        }

        public EvilBehaviour(AIMBotController controller, float timeout) : base(controller)
        {
            this.timeout = timeout;
        }

        public override void Act()
        {
            if (controller.Hijacked)
            {
                controller.ReleaseControlsHijack();
            }
            struggleCancellationToken?.Cancel();
            if (!IsActive)
            {
                cancellationToken = new CoroutineCancellationToken();
                controller.StartCoroutine(ActingCycle(cancellationToken));
                controller.StartCoroutine(NoAttackTimer(cancellationToken));
                if (!Mathf.Approximately(timeout, 0f))
                {
                    controller.StartCoroutine(ConfuseOnTimeout(cancellationToken));
                }
            }
        }
        private IEnumerator NoAttackTimer(CoroutineCancellationToken cancellationToken)
        {
            while (!cancellationToken.CancellationRequested)
            {
                if (timeSinceTargetChanged > InactiveTimeToChangeTarget && timeSinceAction > InactiveTimeToChangeTarget)
                {
                    FindNextTarget();
                }
                yield return null;
                timeSinceTargetChanged += Time.deltaTime;
            }
        }
        private IEnumerator ActingCycle(CoroutineCancellationToken cancellationToken)
        {
            IsActive = true;
            while (!cancellationToken.CancellationRequested)
            {
                if (!BehaviourRoutines.IsAlive(aggressionTarget))
                {
                    FindNextTarget();
                    if (aggressionTarget == null)
                    {
                        //TODO: EVERYONE IS DEAD EXCEPT US: HURT OURSELVES!
                        controller.SetAIBehaviour(new ConfusedBehaviour(controller));
                        IsActive = false;
                        yield break;
                    }
                }
                if (!BehaviourRoutines.IsCurrentlyMelee(controller))
                {
                    yield return RangedKiller(cancellationToken);
                }
                else
                {
                    if (IsWeaponNearby())
                    {
                        yield return LookForWeapon(cancellationToken);
                    }
                    else
                    {
                        yield return MeleeMurder(cancellationToken);
                    }
                }
            }
            IsActive = false;
        }
        private bool IsWeaponNearby()
        {
            if (controller.HeldItem != null)
            {
                //never tell about a weapon if already holding something
                return false;
            }
            const float CloseDistance = 10f;
            var weapons = Gamemaster.Instance.GetItems().Where(item => !item.Equipped && item is Weapon && Vector3.Distance(controller.transform.position, item.transform.position) < CloseDistance);
            return weapons.Any();
        }

        private IEnumerator RangedKiller(CoroutineCancellationToken cancellationToken)
        {
            while (!cancellationToken.CancellationRequested && !BehaviourRoutines.IsCurrentlyMelee(controller) && BehaviourRoutines.IsAlive(aggressionTarget))
            {
                if (!IsShootPositionGood(controller.transform.position, aggressionTarget, BehaviourRoutines.GetAttackRange(controller)))
                {
                    yield return GetToShootingPosition(cancellationToken);
                }
                else
                {
                    yield return BehaviourRoutines.RangedAttack(controller, aggressionTarget, () => timeSinceAction = 0f, cancellationToken);
                }
            }
        }

        private IEnumerator GetToShootingPosition(CoroutineCancellationToken cancellationToken)
        {
            var position = controller.transform.position;

            while (!cancellationToken.CancellationRequested && !BehaviourRoutines.IsCurrentlyMelee(controller) && BehaviourRoutines.IsAlive(aggressionTarget))
            {
                if (!IsShootPositionGood(position, aggressionTarget, BehaviourRoutines.GetAttackRange(controller)))
                {
                    position = GetPositionToShoot(aggressionTarget, BehaviourRoutines.RangedDistanceMediator * BehaviourRoutines.GetAttackRange(controller));
                    controller.NavigationDriver.SetDestination(position);
                }
                controller.RunTowards(controller.NavigationDriver.GetDirection());
                if (controller.NavigationDriver.ReachedDestination() || IsShootPositionGood(controller.transform.position, aggressionTarget, BehaviourRoutines.RangedDistanceMediator * BehaviourRoutines.GetAttackRange(controller)))
                {
                    break;
                }
                yield return null;
            }
        }

        private Vector3 GetPositionToShoot(GameObject target, float range)
        {
            var targetPosition = target.transform.position;
            var dir = (controller.transform.position - targetPosition).normalized;
            var closestPoint = dir * range + targetPosition;
            if (IsShootPositionGood(closestPoint, target, range))
            {
                return closestPoint;
            }
            const float AngleStep = 1f;
            int rangeIteration = 0;
            const int RangeIterationMax = 20;
            while (rangeIteration < RangeIterationMax + 1)
            {
                int iteration = 1;
                var currRange = range  - (range / RangeIterationMax * rangeIteration);
                while (iteration * AngleStep < 180f)
                {
                    var point = Quaternion.Euler(0f, iteration * AngleStep, 0f) * dir * currRange + targetPosition;
                    if (IsShootPositionGood(point, target, range))
                    {
                        return point;
                    }
                    point = Quaternion.Euler(0f, -iteration * AngleStep, 0f) * dir * currRange + targetPosition;
                    if (IsShootPositionGood(point, target, range))
                    {
                        return point;
                    }
                    iteration++;
                }
                rangeIteration++;
            }

            //Very rare case when the target doesn't have a position to shoot it from. Better to default to confused instead of breaking the game
            Debug.LogWarning("Couldn't find a position to shoot. Does this even happen?!");
            controller.SetAIBehaviour(new ConfusedBehaviour(controller));
            return Vector3.zero;
        }


        private bool IsShootPositionGood(Vector3 position, GameObject target, float range)
        {
            if (Vector3.Distance(position, target.transform.position) > range)
            {
                return false;
            }
            NavMeshHit trash;
            position.y = 0f;
            if (!NavMesh.SamplePosition(position, out trash, .1f, NavMesh.AllAreas))
            {
                return false;
            }
            RaycastHit hit;
            position.y = ArenaData.LoSHeight;
            var targetPos = target.transform.position;
            targetPos.y = ArenaData.LoSHeight;

            int layerMask = 1 << LayerMask.NameToLayer(LayerDictionary.NonTargetable);
            layerMask = ~layerMask;
            if (Physics.Raycast(position, targetPos - position, out hit, float.MaxValue, layerMask))
            {
                if (hit.collider.gameObject == target)
                {
                    return true;
                }
                if (target.tag == TagDictionary.Boss && hit.collider.gameObject.tag == TagDictionary.Boss)
                {
                    return true;
                }
            }
            return false;
        }


        private IEnumerator LookForWeapon(CoroutineCancellationToken cancellationToken)
        {
            var weaponToChase = Gamemaster.Instance.GetItems().Where(item => !item.Equipped && item is Weapon).OrderBy(item => Vector3.Distance(controller.transform.position, item.transform.position)).First();
            var position = NavMeshAgentDriver.GetClosestPoint(weaponToChase.transform.position);
            if (position == null)
            {
                Debug.Log("Couldn't get to the weapon position!");
                yield break;
            }
            controller.NavigationDriver.SetDestination(position.Value);
            while (!cancellationToken.CancellationRequested && weaponToChase != null && !weaponToChase.Equipped && controller.HeldItem == null)
            {
                if (controller.NavigationDriver.ReachedDestination())
                {
                    position = NavMeshAgentDriver.GetClosestPoint(weaponToChase.transform.position);
                    if (position == null)
                    {
                        Debug.Log("Couldn't get to the weapon position!");
                        yield break;
                    }
                    controller.NavigationDriver.SetDestination(position.Value);
                }
                controller.RunTowards(controller.NavigationDriver.GetDirection());
                yield return null;
            }
        }

        private IEnumerator MeleeMurder(CoroutineCancellationToken cancellationToken)
        {
            while (!cancellationToken.CancellationRequested && BehaviourRoutines.IsCurrentlyMelee(controller) && !IsWeaponNearby())
            {
                if (BehaviourRoutines.IsInRange(controller, aggressionTarget, desiredToken))
                {
                    yield return MeleeAttack(cancellationToken);
                }
                else
                {
                    yield return GetInRange(cancellationToken);
                }
            }
        }
        private IEnumerator MeleeAttack(CoroutineCancellationToken cancellationToken)
        {
            controller.NavigationDriver.Stop();
            if (desiredToken != null && desiredToken.State == TokenState.Open)
            {
                desiredToken.Seize(controller.gameObject);
            }
            else
            {
                yield break;
            }
            while (!cancellationToken.CancellationRequested && BehaviourRoutines.IsInRange(controller, aggressionTarget, desiredToken) && !IsWeaponNearby())
            {
                var dirToTarget = aggressionTarget.transform.position - controller.transform.position;
                float angle = Mathf.Atan2(dirToTarget.z, dirToTarget.x) * Mathf.Rad2Deg - 90f;
                controller.RotateAroundY(angle + BehaviourRoutines.DiscplacementAngleForMelee);
                controller.UseHeldItem();
                timeSinceAction = 0f;
                yield return null;
                CheckTargetIsAlive();
            }
            //null when the target was killed
            desiredToken?.Release(controller.gameObject);
        }

        private IEnumerator GetInRange(CoroutineCancellationToken cancellationToken)
        {
            const float RepathDelay = 1 / 60f;
            var timeSinceRepath = float.MaxValue;
            var targetChanged = true;
            while (!cancellationToken.CancellationRequested && !BehaviourRoutines.IsInRange(controller, aggressionTarget, desiredToken) && !IsWeaponNearby())
            {
                if (BehaviourRoutines.IsCurrentlyMelee(controller))// && desiredToken == null)
                {
                    desiredToken = currentPunchable.GetAttackTokens().GetClosestTokenOnNavMesh(controller.transform.position);
                }
                //TRY TO GET THE FREE TOKEN IN THE CHAIN OF TOKENS!
                while (desiredToken != null)
                {
                    if (desiredToken.State != TokenState.Open)
                    {
                        currentPunchable = desiredToken.Holder.GetPunchable();
                        aggressionTarget = desiredToken.Holder;
                        desiredToken = currentPunchable.GetAttackTokens().GetClosestTokenOnNavMesh(controller.transform.position);
                        targetChanged = true;
                    }
                    else
                    {
                        break;
                    }
                }

                var desiredPosition = desiredToken?.Position ?? aggressionTarget.transform.position;

                if (controller.NavigationDriver.ReachedDestination() || timeSinceRepath >= RepathDelay || targetChanged)
                {
                    controller.NavigationDriver.SetDestination(NavMeshAgentDriver.GetClosestPoint(desiredPosition) ?? NavMeshAgentDriver.GetRandomWalkablePosition(), targetChanged);
                    timeSinceRepath = 0f;
                }

                controller.RunTowards(controller.NavigationDriver.GetDirection());
                yield return null;
                timeSinceRepath += Time.deltaTime;
                targetChanged = false;
            }
            controller.NavigationDriver.Stop();
        }

        private void CheckTargetIsAlive()
        {
            if (!BehaviourRoutines.IsAlive(aggressionTarget))
            {
                FindNextTarget();
            }
        }

        private IEnumerator StruggleCycle(CoroutineCancellationToken cancellationToken)
        {
            IsStruggling = true;
            while (!cancellationToken.CancellationRequested)
            {
                if (!BehaviourRoutines.IsCurrentlyMelee(controller))
                {
                    if (aggressionTarget != null && IsShootPositionGood(controller.transform.position, aggressionTarget, BehaviourRoutines.GetAttackRange(controller)))
                    {
                        yield return BehaviourRoutines.RangedAttack(controller, aggressionTarget, () => timeSinceAction = 0f, cancellationToken);
                    }
                }
                yield return null;
            }
            IsStruggling = false;
        }

        private IEnumerator ConfuseOnTimeout(CoroutineCancellationToken cancellationToken)
        {
            float timeElapsed = 0f;
            while (!cancellationToken.CancellationRequested && timeElapsed < timeout)
            {
                yield return null;
                timeElapsed += Time.deltaTime;
            }
            if (!cancellationToken.CancellationRequested)
            {
                controller.SetAIBehaviour(new ConfusedBehaviour(controller));
                Debug.Log($"{nameof(EvilBehaviour)} for {controller.gameObject.name} timed out out after {timeout} seconds");
            }
        }

        protected void FindNextTarget()
        {
            timeSinceTargetChanged = 0f;

            List<GameObject> targets = new List<GameObject>();
            targets.AddRange(Gamemaster.Instance.GetAIMBots().Where(bot => bot != controller && bot.Alive).Select(bot => bot.gameObject));

            var tokens = new List<AttackToken>();
            foreach (var target in targets)
            {
                tokens.AddRange(target.GetPunchable()?.GetAttackTokens());
            }

            var currentToken = tokens.GetClosestTokenOnNavMesh(controller.transform.position);
            desiredToken = null;
            if (currentToken == null)
            {
                Debug.Log("Wasn't able to find closest token: received NULL");
                aggressionTarget = null;
                currentPunchable = null;
                return;
            }

            aggressionTarget = currentToken.Target;
            currentPunchable = aggressionTarget.GetPunchable();
        }

        public override Color GetCorrespondingColor()
        {
            return BotMoodColor.Evil;
        }

        public override bool IsMalicious()
        {
            return true;
        }

        public override void Suspend()
        {
            var randomAngle = UnityEngine.Random.Range(10f, 270f);

            controller.Hijack(Quaternion.Euler(0f, randomAngle, 0f));
            cancellationToken?.Cancel();

            if (!IsStruggling)
            {
                struggleCancellationToken = new CoroutineCancellationToken();
                controller.StartCoroutine(StruggleCycle(struggleCancellationToken));
            }
        }

        public override void Shutdown()
        {
            cancellationToken?.Cancel();
            struggleCancellationToken?.Cancel();
            controller.ReleaseControlsHijack();
        }
    }
}
