﻿using System;
using System.Collections;
using System.Collections.Generic;
using AIM.Items;
using UnityEngine;

namespace AIM.Bots.AI
{
    public static class BehaviourRoutines
    {
        public const float MeleeDistance = 0.1f;
        public const float RangedDistanceMediator = .85f;
        public const float DiscplacementAngleForMelee = -20f;

        public static IEnumerator RangedAttack(AIMBotController controller, GameObject target, Action wakeUpAction, CoroutineCancellationToken cancellationToken)
        {
            const float aimRotationEasing = 6f;
            controller.NavigationDriver.Stop();
            var lastAimRotation = controller.transform.rotation;
            var lastTargetPosition = target.transform.position;
            var speed = Vector3.zero;
            var weapon = controller.HeldItem as Weapon;
            if (weapon == null)
            {
                throw new InvalidOperationException($"Bot {controller.gameObject.name} was not prepared to perform ranged attack without a weapon in hands!");
            }
            while (!cancellationToken.CancellationRequested && Vector3.Distance(target.transform.position, controller.transform.position) < weapon.GetRange())
            {

                Transform s = controller.transform;

                float distance = float.PositiveInfinity;
                float minDistance = 0.01f;
                int iteration = 30;

                Quaternion oldRotation = lastAimRotation;
                Vector3 targetPos = target.transform.position;
                targetPos.y = 0f;
                var bulletSpawn = weapon.GetBulletSpawnPoint();
                bulletSpawn.y = 0f;

                //Lead with shots on moving targets
                targetPos = targetPos + (targetPos - bulletSpawn).magnitude * speed / weapon.GetProjectileVelocity();

                while (distance > minDistance && iteration > 0)
                {
                    Vector3 aimOrigin = weapon.GetBulletSpawnPoint();
                    aimOrigin.y = 0f;
                    Vector3 origin;
                    var weaponForward = weapon.GetGunDirection();
                    weaponForward.y = 0f;
                    weaponForward.Normalize();
                    var sForw = s.forward; sForw.y = 0f; sForw.Normalize();
                    var sPos = s.position; sPos.y = 0f;
                    if (!Geometry.LinePlaneIntersection(out origin, aimOrigin, -weaponForward, s.forward, s.position))
                    {

                        //if for some readon the weapon cannot intersect with spine forward plane, just use the aim origin
                        //hopefully this never happens as weapon should always be in front of spine

                        origin = aimOrigin;
                    }
                    origin.y = 0f;
                    Vector3 aimForward = targetPos - origin;

                    Quaternion rot = Quaternion.FromToRotation(weaponForward, aimForward);

                    distance = rot.eulerAngles.sqrMagnitude;
                    iteration--;

                    s.rotation = rot * s.rotation;
                }

                //lerp rotation to make it look smooth
                var wantedRot = s.rotation;
                s.rotation = Quaternion.Slerp(lastAimRotation, s.rotation, Time.deltaTime * aimRotationEasing);
                lastAimRotation = s.rotation;

                //check if we can shoot (first aim, then shoot!)
                if (Mathf.Abs((wantedRot.eulerAngles.y - s.rotation.eulerAngles.y)) < 3f)
                {
                    controller.UseHeldItem();
                    wakeUpAction.Invoke();
                }

                Debug.DrawLine(controller.transform.position, target.transform.position, Color.blue);
                Debug.DrawLine(weapon.GetBulletSpawnPoint(), weapon.GetBulletSpawnPoint() + weapon.GetGunDirection() * 10f, Color.red);
                yield return null;
                //check that the weapon is still in hands
                weapon = controller.HeldItem as Weapon;
                //exit if the weapon or the target are not there after the frame update
                if (weapon == null || !IsAlive(target))
                {
                    break;
                }
                //update speed of the target
                speed = target.transform.position - lastTargetPosition;
                speed.y = 0f;
                speed /= Time.deltaTime;
                lastTargetPosition = target.transform.position;
            }
        }

        public static bool IsCurrentlyMelee(AIMBotController controller)
        {
            bool isMelee = false;

            if (controller.HeldItem != null)
            {
                var weapon = controller.HeldItem as Weapon;
                if (weapon == null)
                {
                    controller.ThrowItem();
                    isMelee = true;
                }
                else
                {
                    if (weapon.GetRange() < MeleeDistance || Mathf.Approximately(weapon.GetRange(), MeleeDistance))
                    {
                        isMelee = true;
                    }
                }
            }
            else
            {
                isMelee = true;
            }
            return isMelee;
        }

        public static bool IsInRange(AIMBotController controller, GameObject aggressionTarget, AttackToken desiredToken)
        {
            const float RequiredTokenCloseness = .3f;
            if (IsCurrentlyMelee(controller))
            {
                return desiredToken != null && Vector3.Distance(controller.transform.position, desiredToken.Position) < RequiredTokenCloseness;
            }
            else
            {
                var weapon = controller.HeldItem as Weapon;
                var workingDistance = weapon.GetRange() * RangedDistanceMediator;
                return aggressionTarget != null && Vector3.Distance(controller.transform.position, aggressionTarget.transform.position) < workingDistance;
            }
        }

        public static float GetAttackRange(AIMBotController controller)
        {
            var weapon = controller.HeldItem as Weapon;
            return weapon?.GetRange() ?? MeleeDistance;
        }

        public static bool IsAlive(GameObject obj)
        {
            if (obj == null)
            {
                return false;
            }
            else if (obj.tag == TagDictionary.Bot)
            {
                var bot = obj.GetComponent<AIMBotController>();
                return bot.Alive;
            }
            else if (obj.tag == TagDictionary.Boss)
            {
                var boss = obj.GetComponent<Boss.BossController>();
                return boss.Alive;
            }
            else
            {
                //we assume the interactables are always alive
                return true;
            }
        }
    }
}
