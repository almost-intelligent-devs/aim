﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using AIM.Items;
using UnityEngine;

namespace AIM.Bots.AI
{
    public class CowardBehaviour : AIMBotAIBehaviour
    {
        public CowardBehaviour(AIMBotController controller) : base(controller)
        {
            controller.BotHpChanged += BotHpChanged;
        }

        public CowardBehaviour(AIMBotController controller, GameObject dangerSource) : base(controller)
        {
            controller.BotHpChanged += BotHpChanged;
            fearSource = dangerSource;
            fearSourceChanged = true;
        }

        private GameObject fearSource;
        private bool fearSourceChanged;

        private float timeSinceDamaged = 0f;
        private const float TimeWithoutDamageToCalm = 60f;
        private IEnumerator LastDamageReceivedTimer(CoroutineCancellationToken cancellationToken)
        {
            while (!cancellationToken.CancellationRequested)
            {
                if (timeSinceDamaged > TimeWithoutDamageToCalm)
                {
                    controller.SetAIBehaviour(new ConfusedBehaviour(controller));
                }
                yield return null;
                timeSinceDamaged += Time.deltaTime;
            }
        }

        private void BotHpChanged(object sender, EventArgs e)
        {
            var args = (BotHpChangedEventArgs)e;
            if (IsActive && args.Difference < 0)
            {
                timeSinceDamaged = 0f;
                UpdateFearSource(args.Source);
            }
        }

        private void UpdateFearSource(GameObject newSource)
        {
            if (fearSource != newSource)
            {
                fearSource = newSource;
                fearSourceChanged = true;
            }
        }

        private IEnumerator Acting(CoroutineCancellationToken cancellationToken)
        {
            IsActive = true;
            controller.StartCoroutine(LastDamageReceivedTimer(cancellationToken));
            controller.StartCoroutine(PanicDashes(cancellationToken));
            while (!cancellationToken.CancellationRequested)
            {
                if (fearSource != null)
                {
                    yield return RunAwayFromDanger(cancellationToken);
                }
                else
                {
                    if (Gamemaster.Instance.GetItems().Any(item => item is HealthPack && !item.Equipped))
                    {
                        yield return GetHealthpack(cancellationToken);
                    }
                    else
                    {
                        yield return Panic(cancellationToken);
                    }
                }
            }
            IsActive = false;
        }

        private IEnumerator PanicDashes(CoroutineCancellationToken cancellationToken)
        {
            const float DashIntervalTop = 20f;
            const float DashIntervalBot = 10f;
            var waitTime = UnityEngine.Random.Range(DashIntervalBot, DashIntervalTop);
            var elapsedTime = 0f;
            while (!cancellationToken.CancellationRequested)
            {
                if (elapsedTime > waitTime)
                {
                    controller.PerformDash();
                    waitTime = UnityEngine.Random.Range(DashIntervalBot, DashIntervalTop);
                    elapsedTime = 0f;
                }
                yield return null;
                elapsedTime += Time.deltaTime;
            }
        }

        private IEnumerator RunAwayFromDanger(CoroutineCancellationToken cancellationToken)
        {
            fearSourceChanged = false;
            if (fearSource == null)
            {
                yield break;
            }

            var dirFromDanger = (controller.transform.position - fearSource.transform.position).normalized;
            const float SearchCone = 45f;

            const float DistToRun = 15f;

            List<Vector3?> runningProbes = new List<Vector3?>();
            const int ProbesCount = 3;

            for (int i = 0; i < ProbesCount; ++i)
            {
                var currentProbe = Quaternion.Euler(0, -SearchCone / 2f + i * SearchCone / (ProbesCount - 1), 0) * dirFromDanger;
                runningProbes.Add(NavMeshAgentDriver.GetPositionOnNavMesh(controller.transform.position + currentProbe * DistToRun, 40f));
            }
            if (!runningProbes.Any(point => point.HasValue))
            {
                runningProbes.Add(NavMeshAgentDriver.GetRandomWalkablePosition());
            }

            var furthestPoint = runningProbes.Where(point => point.HasValue).OrderByDescending(point => (fearSource.transform.position - point).Value.sqrMagnitude).First();

            controller.NavigationDriver.SetDestination(furthestPoint.Value);
            while (!cancellationToken.CancellationRequested && !fearSourceChanged)
            {
                if (!controller.NavigationDriver.ReachedDestination())
                {
                    controller.RunTowards(controller.NavigationDriver.GetDirection());
                }
                else
                {
                    UpdateFearSource(null);
                    break;
                }
                yield return null;
            }
            controller.NavigationDriver.Stop();
        }

        private IEnumerator Panic(CoroutineCancellationToken cancellationToken)
        {
            fearSourceChanged = false;
            const float PositionSearchRadius = 15f;
            var randomDestination = NavMeshAgentDriver.GetGuaranteedPositionOnNavMesh(controller.transform.position, PositionSearchRadius);
            controller.NavigationDriver.SetDestination(randomDestination);
            while (!cancellationToken.CancellationRequested && !fearSourceChanged && !controller.NavigationDriver.ReachedDestination())
            {
                controller.RunTowards(controller.NavigationDriver.GetDirection());
                yield return null;
            }
            controller.NavigationDriver.Stop();
        }

        private IEnumerator GetHealthpack(CoroutineCancellationToken cancellationToken)
        {
            fearSourceChanged = false;
            var packToGet = Gamemaster.Instance.GetItems().Where(item => item is HealthPack && !item.Equipped).OrderBy(pack => Vector3.Distance(pack.transform.position, controller.transform.position)).First();
            var targetPosition = NavMeshAgentDriver.GetClosestPoint(packToGet.transform.position);
            if (targetPosition == null)
            {
                yield return Panic(cancellationToken);
                yield break;
            }
            controller.NavigationDriver.SetDestination(targetPosition.Value);
            while (!cancellationToken.CancellationRequested && !fearSourceChanged && !controller.NavigationDriver.ReachedDestination() && packToGet != null && !packToGet.Equipped)
            {
                controller.RunTowards(controller.NavigationDriver.GetDirection());
                yield return null;
            }
            if (cancellationToken.CancellationRequested || fearSourceChanged)
            {
                yield break;
            }
            while (controller.HeldItem != packToGet)
            {
                bool threwItem = false;
                if (packToGet == null || (packToGet.Equipped && controller.HeldItem != packToGet))
                {
                    yield break;
                }
                if (controller.HeldItem != null)
                {
                    controller.ThrowItem();
                    threwItem = true;
                }
                targetPosition = NavMeshAgentDriver.GetClosestPoint(packToGet.transform.position);
                if (targetPosition == null)
                {
                    yield return Panic(cancellationToken);
                    yield break;
                }
                controller.NavigationDriver.SetDestination(targetPosition.Value, false);
                controller.RunTowards(controller.NavigationDriver.GetDirection());
                yield return threwItem ? new WaitForSeconds(1f) : null;
            }
            controller.UseHeldItem();
        }

        private CoroutineCancellationToken cancellationToken;

        public override void Shutdown()
        {
            if (cancellationToken != null)
            {
                cancellationToken.CancellationRequested = true;
            }
            controller.BotHpChanged -= BotHpChanged;
        }

        public override void Suspend()
        {
            //This behaviour doesn't try to act when the player has the control
            Shutdown();
        }

        public override void Act()
        {
            if (!IsActive)
            {
                cancellationToken = new CoroutineCancellationToken();
                controller.StartCoroutine(Acting(cancellationToken));
            }
        }

        public override Color GetCorrespondingColor()
        {
            return BotMoodColor.Coward;
        }

        public override bool IsMalicious()
        {
            return false;
        }
    }
}
