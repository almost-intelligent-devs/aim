﻿using System.Collections;
using System.Linq;
using UnityEngine;

namespace AIM.Bots.AI
{
    public class ConfusedBehaviour : AIMBotAIBehaviour
    {
        public ConfusedBehaviour(AIMBotController controller) : base(controller)
        {
        }

        private CoroutineCancellationToken cancellationToken;

        public override void Act()
        {
            if (!IsActive)
            {
                cancellationToken = new CoroutineCancellationToken();
                controller.StartCoroutine(ActCycle(cancellationToken));
            }
        }

        public override void Shutdown()
        {
            if (cancellationToken != null)
            {
                cancellationToken.CancellationRequested = true;
            }
        }

        public override void Suspend()
        {
            //This behaviour doesn't try to act when the player has the control
            Shutdown();
        }

        private IEnumerator ActCycle(CoroutineCancellationToken cancellationToken)
        {
            //At the moment just running around in random directions for 5 seconds
            IsActive = true;
            while (!cancellationToken.CancellationRequested)
            {
                if(controller.HeldItem != null)
                {
                    //if we have item, use it once, then throw it away
                    yield return UseItem(cancellationToken);
                }
                else if (UnityEngine.Random.value > .5f)
                {
                    yield return WalkRandomly(cancellationToken);
                }
                else
                {
                    yield return LookRandomly(cancellationToken);
                }
            }
            IsActive = false;
        }

        private IEnumerator WalkRandomly(CoroutineCancellationToken cancellationToken)
        {
            const float PositionSearchRadius = 10f;
            const float ItemSearchRadius = 5f;
            var items = Gamemaster.Instance.GetItems().Where(item => !item.Equipped);
            Vector3 randomDestination;
            if (items.Any() && UnityEngine.Random.value > .7f && controller.HeldItem == null)
            {
                var randomItemPosition = items.OrderBy(item => UnityEngine.Random.value).First().transform.position;
                randomDestination = NavMeshAgentDriver.GetClosestPoint(randomItemPosition) ?? NavMeshAgentDriver.GetGuaranteedPositionOnNavMesh(randomItemPosition, ItemSearchRadius);
            }
            else
            {
                randomDestination = NavMeshAgentDriver.GetGuaranteedPositionOnNavMesh(controller.transform.position, PositionSearchRadius);
            }
            controller.NavigationDriver.SetDestination(randomDestination);
            var done = false;
            while (!cancellationToken.CancellationRequested && !done)
            {
                if (controller.NavigationDriver.ReachedDestination())
                {
                    // Done
                    done = true;
                }
                controller.WalkTowards(controller.NavigationDriver.GetDirection());

                yield return null;
            }
            controller.NavigationDriver.Stop();
        }

        private IEnumerator LookRandomly(CoroutineCancellationToken cancellationToken)
        {
            var lookDuration = UnityEngine.Random.Range(1f, 3f);
            var elapsed = 0f;

            const float AngularSpeed = 50f;
            float angularSpeed = AngularSpeed * (UnityEngine.Random.value > .5f ? 1f : -1f);

            while (!cancellationToken.CancellationRequested && elapsed < lookDuration)
            {
                controller.transform.Rotate(0f, angularSpeed * Time.deltaTime, 0f, Space.Self);
                yield return null;
                elapsed += Time.deltaTime;
            }
        }

        private IEnumerator UseItem(CoroutineCancellationToken cancellationToken)
        {
            yield return LookRandomly(cancellationToken);
            if(!cancellationToken.CancellationRequested && controller.HeldItem != null)
            {
                controller.UseHeldItem();
                var elapsedTime = 0f;
                const float WaitTime = .5f;
                while(!cancellationToken.CancellationRequested && elapsedTime < WaitTime)
                {
                    yield return null;
                    elapsedTime += Time.deltaTime;
                }
                if(!cancellationToken.CancellationRequested && controller.HeldItem != null)
                {
                    controller.ThrowItem();
                }
            }
        }

        public override Color GetCorrespondingColor()
        {
            return BotMoodColor.Confused;
        }

        public override bool IsMalicious()
        {
            return false;
        }
    }
}
