﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace AIM.Bots.AI
{
    /// <summary>
    /// The base class for all the <see cref="AIMBotController"/> behaviours
    /// </summary>
    public abstract class AIMBotAIBehaviour
    {
        protected AIMBotController controller;
        protected const float DecisionActFrequency = 20f;
        public bool IsActive { get; protected set; }

        public AIMBotAIBehaviour(AIMBotController controller)
        {
            Init(controller);
        }

        private void Init(AIMBotController controller)
        {
            this.controller = controller;
        }

        /// <summary>
        /// Tells the behaviour that it should yield the control
        /// </summary>
        public abstract void Suspend();
        /// <summary>
        /// Activates the controlling loop
        /// </summary>
        public abstract void Act();
        /// <summary>
        /// Shuts down the behaviour immediately
        /// </summary>
        public abstract void Shutdown();
        public abstract bool IsMalicious();

        public virtual Color GetCorrespondingColor()
        {
            return BotMoodColor.Default;
        }
    }
}
