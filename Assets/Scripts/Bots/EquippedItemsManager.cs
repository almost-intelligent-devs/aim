﻿using AIM.Items;
using System;
using UnityEngine;

namespace AIM.Bots
{
    /// <summary>
    /// Responsible for putting the picked up items by the bots to the proper positions
    /// within the bot model
    /// </summary>
    public class EquippedItemsManager : MonoBehaviour
    {
        public Transform HeldItemPosition;

        private void Awake()
        {
            if (HeldItemPosition == null)
            {
                throw new InvalidOperationException($"{nameof(HeldItemPosition)} is not specified!");
            }
        }

        public void Equip(Item item)
        {
            item.transform.SetParent(HeldItemPosition, false);
            item.transform.localPosition = Vector3.zero;
            item.transform.localRotation = Quaternion.identity;
        }
    }
}
