﻿using System;
using UnityEngine;

namespace AIM.Bots
{
    public class BotMoveEventArgs : EventArgs
    {
        public Vector3 WorldSpaceDirection { get; private set; }

        public BotMoveEventArgs(Vector3 worldSpaceDirection)
        {
            WorldSpaceDirection = worldSpaceDirection;
        }
    }
}
