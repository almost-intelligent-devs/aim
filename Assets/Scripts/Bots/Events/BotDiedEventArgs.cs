﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AIM.Bots
{
    public class BotDiedEventArgs : EventArgs
    {
        public AIMBotController Deceased { get; private set; }

        public BotDiedEventArgs(AIMBotController victim)
        {
            this.Deceased = victim;
        }
    }
}
