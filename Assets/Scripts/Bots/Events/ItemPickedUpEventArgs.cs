﻿using System;
using System.Collections;
using System.Collections.Generic;
using AIM.Items;
using UnityEngine;

namespace AIM.Bots
{
    public class ItemPickedUpEventArgs : EventArgs
    {
        public Item Item { get; private set; }
        public ItemPickedUpEventArgs(Item item)
        {
            Item = item;
        }
    }
}
