﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AIM.Bots
{
    public class BotHpChangedEventArgs : EventArgs
    {
        public float Difference { get; private set; }
        public GameObject Source;

        public BotHpChangedEventArgs(float diff, GameObject source)
        {
            this.Difference = diff;
            this.Source = source;
        }
    }
}
