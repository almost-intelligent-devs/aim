﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace AIM.Bots
{
    public class RagdollController : MonoBehaviour
    {
        public Rigidbody Rigidbody;
        [SerializeField] GameObject rigRoot;


        private void Awake()
        {
            if (rigRoot == null)
            {
                throw new InvalidOperationException($"{nameof(rigRoot)} is not specified!");
            }
            if (Rigidbody == null)
            {
                throw new InvalidOperationException($"{nameof(Rigidbody)} is not specified!");
            }
        }

        public void MatchTransform(GameObject objToMatch)
        {
            Transform[] allChildrenToMatch = objToMatch.GetComponentsInChildren<Transform>(true);
            Transform[] allChildren = rigRoot.GetComponentsInChildren<Transform>(true);
            foreach(var obj in allChildren)
            {
                var match = allChildrenToMatch.FirstOrDefault(tr => tr.name == obj.name);
                if(match != null)
                {
                    obj.localEulerAngles = match.localEulerAngles;
                    obj.localRotation = match.localRotation;
                    obj.localPosition = match.localPosition;
                }
            }
        }
    }
}
