﻿using System;
using AIM.Bots;
using UnityEngine;


/// <summary>
/// Handles melee weapon collider (de-)activation when attacking.
/// Is attached to the striking state of the bot animator.
/// </summary>
public class MeleeWeaponBehaviour : StateMachineBehaviour
{
    private AIMBotController aimBotController;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (!aimBotController)
            aimBotController = animator.GetComponent<AIMBotController>();

        aimBotController.MeleeWeapon.ColliderActive = true;
        aimBotController.OnAttackUsed(this, EventArgs.Empty);
    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        // only deactivate the collider if the next state is not the same state
        // (due to transitions the next state will already be the current state
        //  of the animator, thus we have to se GetCurrentAnimatorStateInfo)
        if (animator.GetCurrentAnimatorStateInfo(0).shortNameHash != stateInfo.shortNameHash)
            aimBotController.MeleeWeapon.ColliderActive = false;
    }
}
