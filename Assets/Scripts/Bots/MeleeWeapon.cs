﻿using System;
using System.Collections;
using System.Collections.Generic;
using AIM.Boss;
using AIM.Boss.Weakpoints;
using AIM.Interactables;
using AIM.Sound;
using UnityEngine;


namespace AIM.Bots
{
    /// <summary>
    /// Contains collision logic for the melee attacks.
    /// </summary>
    [RequireComponent(typeof(Collider))]
    public class MeleeWeapon : MonoBehaviour
    {
        new private Collider collider;

        private List<GameObject> interactedList = new List<GameObject>();

        [SerializeField]
        private float damage = 3f;

        [SerializeField] private ParticleSystem psSparks;

        private SoundsPlayer soundsPlayer;

        /// <summary>
        /// Turn on/off the weapon collider.
        /// </summary>
        /// <value><see langword="true"/> if the weapon collider is enabled</value>
        public bool ColliderActive
        {
            get
            {
                return collider.enabled;
            }
            set
            {
                collider.enabled = value;
                if (value)
                {
                    interactedList.Clear();
                }
            }
        }

        private void Awake()
        {
            collider = GetComponent<Collider>();
            if (collider == null)
            {
                throw new InvalidOperationException($"{nameof(collider)} is not specified!");
            }
            if (psSparks == null)
            {
                throw new InvalidOperationException($"{nameof(psSparks)} is not specified!");
            }

            soundsPlayer = GetComponentInChildren<SoundsPlayer>();
            if (soundsPlayer == null)
            {
                throw new InvalidOperationException($"{nameof(soundsPlayer)} is not specified!");
            }
        }

        private void Start()
        {
            ColliderActive = false;
        }

        private void OnTriggerEnter(Collider other)
        {
            psSparks.Play();

            if (!interactedList.Contains(other.gameObject))
            {
                soundsPlayer.PlayOneShot(SharedSoundsMaster.Instance.Weapons.GetClip(WeaponSounds.WrenchClang));
                interactedList.Add(other.gameObject);

                if (other.gameObject.tag == TagDictionary.Bot)
                {
                    Debug.Log($"Hit {other.gameObject.name} for {damage} HP");
                    var controller = other.GetComponent<AIMBotController>();
                    var force = other.transform.position - transform.parent.position;
                    force.y = 0f;
                    controller?.ChangeHP(-damage, this.gameObject, force.normalized * ArenaData.MediumRagdollForce);
                }

                if(other.gameObject.tag == TagDictionary.BossWeakPoint)
                {
                    other.GetComponent<BossWeakPoint>().DealBonusDamage(damage);
                }

                if (other.gameObject.tag == TagDictionary.Boss)
                {
                    other.GetComponentInParent<BossController>().ChangeHP(-damage);
                }

                if (other.gameObject.tag == TagDictionary.Interactable)
                {
                    var interactable = other.GetComponent<Interactable>();
                    interactable.Interact(this.gameObject);
                }
            }
        }
    }
}
