﻿using UnityEngine;


namespace AIM.Bots
{
    /// <summary>
    /// Contains hashes for accessing parameters of the aim bot animation controller
    /// </summary>
    public static class BotAnimParameter
    {
        // Int
        public static readonly int IDashDirection = Animator.StringToHash("DashDirection"); //  0,1=forward, 2=back_right, 3=back_left
        public static readonly int IKnockBackDirection = Animator.StringToHash("KnockBackDirection"); //  1=front, -1=behind

        // Bools
        public static readonly int BKnockBack = Animator.StringToHash("KnockBack");
        public static readonly int BStomp = Animator.StringToHash("Stomp");

        // Floats
        public static readonly int FHorizontal = Animator.StringToHash("Horizontal");
        public static readonly int FVertical = Animator.StringToHash("Vertical");
        public static readonly int FLocoBlend = Animator.StringToHash("LocoBlend");

        // Triggers
        public static readonly int TStrike = Animator.StringToHash("Strike");
        public static readonly int TDash = Animator.StringToHash("Dash");
        public static readonly int TThrow = Animator.StringToHash("Throw");
        public static readonly int TCarry = Animator.StringToHash("Carry");
        public static readonly int THoldWeapon = Animator.StringToHash("HoldWeapon");

        // State Tags
        public static readonly int SDash = Animator.StringToHash("Dash");
        public static readonly int SStrike = Animator.StringToHash("Strike");
        public static readonly int SThrow= Animator.StringToHash("Throw");
        public static readonly int SKnockBack = Animator.StringToHash("KnockBack");
        public static readonly int SLocomotion = Animator.StringToHash("Locomotion");

        // Layer Index
        public const int LBaseLayer = 0;
        public const int LUpperBody = 1;
    }
}
