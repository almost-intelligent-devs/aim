﻿using AIM.Animation;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AIM.Bots
{
    /// <summary>
    /// Handles all animation logic of AIM bots
    /// </summary>
    [RequireComponent(typeof(Animator))]
    public class AIMBotAnimator : MonoBehaviour
    {
        private const float Damping = 0.05f;

        private Animator animator;
        private CoroutineCancellationToken maskUpperBodyToken;

        [SerializeField] private float decayTimeWalking = 0.4f;
        [SerializeField] private float decayTimeRunning = 0.1f;
        private Vector2 decayVelocity = Vector2.zero;
        private bool movedThisFrame = false;
        private MovementSpeed currentMovementSpeed = MovementSpeed.Running;

        private Dictionary<int, SmartTrigger> triggers;

        private void Awake()
        {
            animator = GetComponent<Animator>();
            triggers = new Dictionary<int, SmartTrigger>
            {
                { BotAnimParameter.TDash, new SmartTrigger(this, animator, BotAnimParameter.TDash, BotAnimParameter.SDash)
                    { BlockUntilFinished = true, AnyStateExceptions = new List<int>(){ BotAnimParameter.SKnockBack, BotAnimParameter.SThrow} }
                },
                { BotAnimParameter.TThrow, new SmartTrigger(this, animator, BotAnimParameter.TThrow, BotAnimParameter.SThrow) { BlockUntilFinished = true } },
                { BotAnimParameter.TStrike, new SmartTrigger(this, animator, BotAnimParameter.TStrike, BotAnimParameter.SStrike) { BlockUntilFinished = false } }
            };

            triggers[BotAnimParameter.TThrow].OnReset += (obj, args) => OnItemRelease = null;
        }

        #region Movement

        /// <summary>
        /// Sets animator parameters so that bot is moving towards the target direction with given speed (via root motion)
        /// </summary>
        public void MoveTowards(Vector3 worldSpaceDirection, MovementSpeed speed = MovementSpeed.Running)
        {
            if (Mathf.Approximately(worldSpaceDirection.sqrMagnitude, 0f)) { return; }
            else if (triggers[BotAnimParameter.TStrike].IsControlledStateActive)
            {
                // reseting the melee attack trigger here, so if the player mashes the attack button
                // but then decides to run away, he doesn't have to wait for the attack animation
                triggers[BotAnimParameter.TStrike].Reset();
            }

            currentMovementSpeed = speed;

            Vector3 dir = transform.InverseTransformDirection(worldSpaceDirection).normalized;
            animator.SetFloat(BotAnimParameter.FHorizontal, dir.x, Damping, Time.deltaTime);
            animator.SetFloat(BotAnimParameter.FVertical, dir.z, Damping, Time.deltaTime);
            animator.SetFloat(BotAnimParameter.FLocoBlend, (float)speed);

            direction = worldSpaceDirection;

            movedThisFrame = true;
        }

        private void LateUpdate()
        {
            if (askedToDigThisFrame)
            {
                askedToDigThisFrame = false;
            }
            else
            {
                Dig(false);
            }

            if (movedThisFrame)
            {
                movedThisFrame = false;
            }
            else
            {
                float horizontal = animator.GetFloat(BotAnimParameter.FHorizontal);
                float vertical = animator.GetFloat(BotAnimParameter.FVertical);

                if (Mathf.Approximately(horizontal, 0f) && Mathf.Approximately(vertical, 0f))
                {
                    horizontal = vertical = 0f;
                    direction = Vector3.zero;
                }
                else
                {
                    float decayTime = (currentMovementSpeed == MovementSpeed.Walking) ? decayTimeWalking : decayTimeRunning;
                    horizontal = Mathf.SmoothDamp(horizontal, 0f, ref decayVelocity.x, decayTime);
                    vertical = Mathf.SmoothDamp(vertical, 0f, ref decayVelocity.y, decayTime);
                }

                animator.SetFloat(BotAnimParameter.FHorizontal, horizontal);
                animator.SetFloat(BotAnimParameter.FVertical, vertical);
            }
        }
        #endregion

        #region Actions

        /// <summary>
        /// Triggers dash animation and sets direction according to the bots current movement direction
        /// </summary>
        public bool Dash()
        {
            if (triggers[BotAnimParameter.TDash].Trigger())
            {
                ResetAllOtherTriggers(BotAnimParameter.TDash);

                Vector2 velocity = new Vector2(animator.GetFloat(BotAnimParameter.FHorizontal), animator.GetFloat(BotAnimParameter.FVertical));

                int direction = 0;
                if (velocity.sqrMagnitude > 0)
                    direction = (int)((Vector2.SignedAngle(velocity, Vector2.up) + 450f) % 360f / 90f);

                animator.SetInteger(BotAnimParameter.IDashDirection, direction);
                return true;
            }
            return false;
        }

        /// <summary> Plays throwing animation if the animation is not already queued or playing </summary>
        /// <returns> Indicates whether the was triggered by this call</returns>
        public bool Throw()
        {
            if (triggers[BotAnimParameter.TThrow].Trigger())
            {
                EquipMelee();
                return true;
            }

            return false;
        }

        private bool askedToDigThisFrame = false;
        public void Dig(bool shouldDig)
        {
            animator.SetBool(BotAnimParameter.BStomp, shouldDig);
            askedToDigThisFrame = true;
        }

        public void Carry()
        {
            animator.SetTrigger(BotAnimParameter.TCarry);
            MaskUpperBody(false);
        }

        #endregion

        #region Attacking

        public void PerformMelee() => triggers[BotAnimParameter.TStrike].Trigger();

        /// <summary>
        /// Sets layer weights so that bot is holding rifle
        /// </summary>
        public void EquipRifle()
        {
            animator.SetTrigger(BotAnimParameter.THoldWeapon);
            MaskUpperBody();
        }

        /// <summary>
        /// Resets rifle layer weights to default
        /// </summary>
        public void EquipMelee()
        {
            UnmaskUpperBody();
        }

        #endregion

        #region KnockBack

        /// <summary> Shows knock back animation until velocity is below a threshold </summary>
        /// <param name="botRigidbody">Used to get velocity and check when to stop animation</param>
        /// <param name="force">Used for determining the knock back direction</param>
        public void KockBack(Rigidbody botRigidbody, Vector3 force)
        {
            // To avoid conflicts with weapon holding mask
            // just drop the item if throw was triggered before
            if (animator.IsCurrentStateTagHash(BotAnimParameter.SThrow)
                || animator.IsNextStateTagHash(BotAnimParameter.SThrow))
            {
                InvokeOnItemRelease();
            }

            float angleToBot = Vector3.SignedAngle(transform.right, force, Vector3.up);
            int facing = (int)Mathf.Sign(angleToBot);
            animator.SetInteger(BotAnimParameter.IKnockBackDirection, facing);
            animator.SetBool(BotAnimParameter.BKnockBack, true);
            animator.applyRootMotion = false;
            force *= -facing;
            transform.rotation = Quaternion.LookRotation(force, Vector3.up);
            StartCoroutine(WaitAndStopKnockBack(botRigidbody, 40f));
        }

        private IEnumerator WaitAndStopKnockBack(Rigidbody botRigidbody, float threshold)
        {
            yield return new WaitForFixedUpdate(); // Wait until next physics update
            yield return new WaitUntil(() => animator.IsCurrentStateTagHash(BotAnimParameter.SKnockBack));
            yield return new WaitWhile(() => botRigidbody.velocity.sqrMagnitude > threshold);

            animator.SetBool(BotAnimParameter.BKnockBack, false);
            animator.applyRootMotion = true;

            yield return new WaitUntil(() => !animator.IsCurrentStateTagHash(BotAnimParameter.SKnockBack));
            ResetAllTriggers();
            OnKnockBackFinished?.Invoke(this, EventArgs.Empty);
        }

        #endregion

        #region Animation Events

        /// <summary> Invoked when the item should be released after calling <see cref="Throw"/> </summary>
        public event EventHandler OnItemRelease;
        private void InvokeOnItemRelease() => OnItemRelease?.Invoke(this, EventArgs.Empty);

        /// <summary> Invoked when knock back is finished after calling <see cref="KockBack"/> </summary>
        public event EventHandler OnKnockBackFinished;

        #endregion

        #region Masking

        private void MaskUpperBody(bool animate = true)
        {
            if (animate)
            {
                maskUpperBodyToken?.Cancel();
                maskUpperBodyToken = new CoroutineCancellationToken();
                StartCoroutine(AnimateLayerWeight(BotAnimParameter.LUpperBody, 1f, .5f, maskUpperBodyToken));
            }
            else
            {
                animator.SetLayerWeight(BotAnimParameter.LUpperBody, 1f);
            }
        }

        private void UnmaskUpperBody()
        {
            maskUpperBodyToken?.Cancel();
            maskUpperBodyToken = new CoroutineCancellationToken();
            StartCoroutine(AnimateLayerWeight(BotAnimParameter.LUpperBody, 0f, .5f, maskUpperBodyToken));
        }

        #endregion

        #region Util

        /// <summary>
        /// Lerps between the current weight of @layerIndex and @targetWeight in time @duration
        /// </summary>
        private IEnumerator AnimateLayerWeight(int layerIndex, float targetWeight, float duration, CoroutineCancellationToken ct)
        {
            float time = 0f;
            float startWeight = animator.GetLayerWeight(layerIndex);
            while (!ct.CancellationRequested && time < duration)
            {
                time += Time.deltaTime;
                animator.SetLayerWeight(layerIndex, Mathf.Lerp(startWeight, targetWeight, time / duration));
                yield return 0;
            }

            maskUpperBodyToken = null;
        }

        private void ResetAllTriggers()
        {
            foreach (SmartTrigger st in triggers.Values)
                st.Reset();
        }

        /// <summary>
        /// Reset all triggers except <paramref name="triggerNameHash"/>/>
        /// </summary>
        private void ResetAllOtherTriggers(int triggerNameHash)
        {
            foreach (int key in triggers.Keys)
            {
                if (key != triggerNameHash)
                {
                    triggers[key].Reset();
                }
            }
        }

        #endregion

        #region MovementHack
        private const float movementSpeedMultiplier = 0.4f;
        private Vector3 direction = Vector3.zero;
        private new Rigidbody rigidbody;
        private AIMBotController botController;
        private void Start()
        {
            rigidbody = GetComponent<Rigidbody>();
            if (!rigidbody)
            {
                throw new InvalidOperationException($"{nameof(rigidbody)} is not specified!");
            }
            botController = GetComponent<AIMBotController>();
            if (botController == null)
            {
                throw new InvalidOperationException($"{nameof(botController)} is not specified!");
            }
        }
        private void OnAnimatorMove()
        {
            if (animator.IsCurrentStateTagHash(BotAnimParameter.SLocomotion, BotAnimParameter.LBaseLayer)
                && movedThisFrame && currentMovementSpeed == MovementSpeed.Running && botController.ControlledByPlayer)
            {
                rigidbody.AddForce(direction.normalized * movementSpeedMultiplier * (Time.deltaTime / (1 / 60f)), ForceMode.VelocityChange);
            }
            else
            {
                animator.ApplyBuiltinRootMotion();
            }
        }
        #endregion
    }

    public enum MovementSpeed
    {
        Walking = 0,
        Running = 1
    }
}
