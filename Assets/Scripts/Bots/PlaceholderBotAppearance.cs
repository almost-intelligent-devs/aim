﻿using System;
using UnityEngine;
using AIM.VFX;

namespace AIM.Bots
{
    public class PlaceholderBotAppearance : MonoBehaviour
    {
        [SerializeField] private ParticleColorChanger headLight;
        [SerializeField] private SpriteRenderer playerIndicator;
        private AIMBotController botController;

        public void SetHeadColor(Color color) => headLight.ChangeColor(color);
        public void SetIndicatorColor(Color color) => playerIndicator.color = color;
        public bool SetIndicatorEnabled(bool enabled) => playerIndicator.enabled = enabled;

        private void Awake()
        {
            if (headLight == null)
            {
                throw new InvalidOperationException($"{nameof(headLight)} is not specified!");
            }

            if (playerIndicator == null)
            {
                throw new InvalidOperationException($"{nameof(playerIndicator)} is not specified!");
            }

            botController = GetComponent<AIMBotController>();
            if (botController == null)
            {
                throw new InvalidOperationException($"{nameof(botController)} is not specified!");
            }
        }

        private void OnEnable()
        {
            botController.BotDied += OnDeath;
        }
        private void OnDisable()
        {
            botController.BotDied -= OnDeath;
        }

        private void OnDeath(object sender, EventArgs e)
        {
            headLight.TurnOff();
        }

        public bool CanBeControlled()
        {
            return !botController.ControlBlocked;
        }
    }
}
