using AIM.Bots.AI;
using AIM.Environment;
using AIM.Items;
using AIM.Sound;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

namespace AIM.Bots
{
    /// <summary>
    /// The main container of the AIM bots interactions logic.
    /// Exposes HP, actions, movement, etc logic
    /// </summary>
    [RequireComponent(typeof(Rigidbody), typeof(NavMeshAgent), typeof(AIMBotAIDriver))]
    [RequireComponent(typeof(AIMBotAnimator))]
    public class AIMBotController : MonoBehaviour, IPunchable
    {
        #region events
        public event EventHandler BotHpChanged;
        public event EventHandler BotDied;

        public event EventHandler TakenByThePlayer;
        public event EventHandler ReleasedByThePlayer;

        public event EventHandler ItemPickedUp;
        public event EventHandler AttackedOrUsedItem;
        public event EventHandler DiggedThisFrame;


        #endregion
        #region Sounds
        [SerializeField] private SoundsPlayer soundsPlayer;
        [SerializeField] private BotSoundsProvider defaultSounds;
        [SerializeField] private BotSoundsProvider cowardSounds;
        [SerializeField] private BotSoundsProvider angrySounds;
        [SerializeField] private BotSoundsProvider evilSounds;

        public BotSoundsProvider SoundsProvider
        {
            get
            {
                if (AIdriver.CurrentBehaviour is CowardBehaviour)
                {
                    return cowardSounds;
                }
                else if (AIdriver.CurrentBehaviour is AggressiveBehaviour)
                {
                    return angrySounds;
                }
                else if (AIdriver.CurrentBehaviour is EvilBehaviour)
                {
                    return evilSounds;
                }
                return defaultSounds;
            }
        }
        public SoundsPlayer Sounds { get { return soundsPlayer; } }

        #endregion

        public bool ControlBlocked { get; set; } = false;
        [SerializeField]
        EquippedItemsManager equippedItemsManager;

        [SerializeField]
        private float diggingRange = 3.0f;
        [SerializeField]
        private Transform headTransform;
        public Transform HeadTransform { get { return headTransform; } }
        [SerializeField] private Transform carriedItemTransform;
        [SerializeField]
        private RagdollController ragdoll;
        private Rigidbody rigidBody;
        public AIMBotAIDriver AIdriver { get; private set; }

        public NavMeshAgentDriver NavigationDriver { get; private set; }

        private AIMBotAnimator animator;

        private bool lockRotation = false;

        [SerializeField]
        private MeleeWeapon meleeWeapon;
        public MeleeWeapon MeleeWeapon { get { return meleeWeapon; } }

        /// <summary>
        /// Whether the bot has a corrupted aka malicious AI behaviour
        /// </summary>
        /// <value><see langword="true"/> when corrupted</value>
        public bool Corrupted { get; set; }

        public AIMBotAIBehaviour CurrentBehaviour
        {
            get
            {
                return AIdriver.CurrentBehaviour;
            }
        }


        /// <summary>
        /// Reference to the currently held item
        /// </summary>
        public Item HeldItem { get; private set; }

        #region HP management
        private float hp = 100f;
        private const float HardModeDamageMultiplier = 1.25f;
        private const float HardModeHealMultiplier = 0.5f;
        /// <summary>
        /// Default value is 100; dies at ≤ 0;
        /// </summary>
        /// <value>Current amount of bot's hitpoints</value>
        public float HitPoints
        {
            get
            {
                return hp;
            }
        }

        public void ChangeHP(float diff, GameObject source, Vector3? force = null)
        {
            if(Gamemaster.Instance.HardMode)
            {
                if (diff < 0 && source != null && source.tag == TagDictionary.Boss)
                {
                    diff *= HardModeDamageMultiplier;
                }
                else if(diff > 0)
                {
                    diff *= HardModeHealMultiplier;
                }
            }
            if (Alive)
            {
                hp = Mathf.Clamp(hp + diff, 0f, 100f);
                BotHpChanged?.Invoke(this, new BotHpChangedEventArgs(diff, source));
                if (diff < 0)
                {

                    AudioClip clip = source != null && (source.tag == TagDictionary.Bot || source.tag == TagDictionary.BotWeapon) ? SoundsProvider.GetClip(BotSounds.DamagedByBot) : SoundsProvider.GetClip(BotSounds.Damaged);
                    Sounds.PlayIfEmpty(clip, 0);
                }
                else
                {
                    Sounds.PlayOrQueue(SoundsProvider.GetClip(BotSounds.EatHealthpack), 0);
                }
                if (hp < 0f || Mathf.Approximately(hp, 0f))
                {
                    ProcessDeath(force);
                }
            }
        }

        public bool Alive
        {
            get; private set;
        } = true;
        #endregion

        #region Taking/Releasing control by the Player
        public bool ControlledByPlayer { get; private set; }
        public void AssumeDirectControl()
        {
            ControlledByPlayer = true;
            NavigationDriver.enabled = false;
            TakenByThePlayer?.Invoke(this, EventArgs.Empty);
            Sounds.PlayOrQueue(SoundsProvider.GetClip(BotSounds.BotControlled), 0);
        }
        public void ReleaseDirectControl()
        {
            ControlledByPlayer = false;
            if (Alive)
            {
                NavigationDriver.enabled = true;
            }
            ReleasedByThePlayer?.Invoke(this, EventArgs.Empty);
        }
        #endregion

        #region Unity lifecycle
        private void Awake()
        {
            rigidBody = GetComponent<Rigidbody>();
            if (rigidBody == null)
            {
                throw new InvalidOperationException($"{nameof(rigidBody)} is not specified!");
            }

            AIdriver = GetComponent<AIMBotAIDriver>();
            if (AIdriver == null)
            {
                throw new InvalidOperationException($"{nameof(AIdriver)} is not specified!");
            }

            NavigationDriver = GetComponent<NavMeshAgentDriver>();
            if (NavigationDriver == null)
            {
                throw new InvalidOperationException($"{nameof(NavigationDriver)} is not specified!");
            }

            if (meleeWeapon == null)
            {
                throw new InvalidOperationException($"Missing reference: \"meleeWeapon\"");
            }
            if (equippedItemsManager == null)
            {
                throw new InvalidOperationException($"{nameof(equippedItemsManager)} is not specified!");
            }

            animator = GetComponent<AIMBotAnimator>();
            if (animator == null)
            {
                throw new InvalidOperationException($"{nameof(animator)} is not specified!");
            }

            if (soundsPlayer == null)
            {
                throw new InvalidOperationException($"{nameof(soundsPlayer)} is not specified!");
            }
            if (defaultSounds == null)
            {
                throw new InvalidOperationException($"{nameof(defaultSounds)} is not specified!");
            }
            if (evilSounds == null)
            {
                throw new InvalidOperationException($"{nameof(evilSounds)} is not specified!");
            }
            if (angrySounds == null)
            {
                throw new InvalidOperationException($"{nameof(angrySounds)} is not specified!");
            }
            if (cowardSounds == null)
            {
                throw new InvalidOperationException($"{nameof(cowardSounds)} is not specified!");
            }

            Gamemaster.Instance.Register(this);
        }

        private void Start()
        {
            if (gameObject.tag != TagDictionary.Bot)
            {
                throw new InvalidOperationException($"{nameof(AIMBotController)} under the name of {gameObject.name} doesn't have the proper tag = \"{TagDictionary.Bot}\" on it! Interactions will not work for this bot!");
            }
            var boss = Gamemaster.Instance.GetBoss();
            if (boss != null)
            {
                boss.LALA += PlayLALAPanic;
                boss.EndLALA += EndLALAPanic;
            }
        }


        #endregion

        #region Actions
        public void RotateAroundY(float angle)
        {
            if (Alive && !lockRotation && !Hijacked)
            {
                transform.rotation = Quaternion.AngleAxis(angle, Vector3.down);
            }
        }

        /// <summary>
        /// Tell the bot to move in the specified direction
        /// </summary>
        /// <param name="worldSpaceDirection">Direction vector, should be normalized</param>
        public void RunTowards(Vector3 worldSpaceDirection)
        {
            if (Alive)
            {
                animator.MoveTowards(movementConfusion * worldSpaceDirection, MovementSpeed.Running);
            }
        }

        /// <summary>
        /// Tell the bot to walk (slower) in the specified direction
        /// </summary>
        /// <param name="worldSpaceDirection">Direction vector, should be normalized</param>
        public void WalkTowards(Vector3 worldSpaceDirection)
        {
            if (Alive)
            {
                animator.MoveTowards(movementConfusion * worldSpaceDirection, MovementSpeed.Walking);
            }
        }

        /// <summary>
        /// Tell the bot to dash in the direction it's moving
        /// </summary>
        public void PerformDash()
        {
            if (Alive)
            {
                if (animator.Dash())
                {
                    soundsPlayer.PlayIfEmpty(SoundsProvider.GetClip(BotSounds.Dash), 0);
                }
            }
        }

        /// <summary>
        /// Tell the bot to use the held item. If no item is held, melee attack/terminal interaction will be performed
        /// </summary>
        public void UseHeldItem()
        {
            if (HeldItem == null)
            {
                animator.PerformMelee();
            }
            else
            {
                //TODO: call item's logic here!
                HeldItem.Use(this);
                Debug.Log("Item was used!");
            }
        }

        /// <summary>
        /// Tell the bot to throw an item.
        /// </summary>
        public void ThrowItem()
        {
            const float ThrowForce = 10f;
            if (HeldItem != null && animator.Throw())
            {
                Sounds.PlayOrQueue(SoundsProvider.GetClip(BotSounds.Throw), 0);
                EventHandler handler = null;
                var throwingItem = HeldItem;
                animator.OnItemRelease += handler = (sender, args) =>
                {
                    animator.OnItemRelease -= handler;
                    if (HeldItem != null)
                    {
                        DeattachItem();
                        HeldItem.Throw(transform.forward * ThrowForce, gameObject);
                    }
                    HeldItem = null;
                    EquipMelee();
                };
            }
        }
        private void DeattachItem()
        {
            HeldItem.transform.SetParent(null);
            HeldItem.Equipped = false;
            HeldItem.OnItemDestroyed -= OnHeldItemDestroyed;
            HeldItem.ItemUsed -= OnItemUsed;
        }

        /// <summary>
        /// Tell the bot to equip item. Will ignore the call if already has an item
        /// </summary>
        /// <param name="item">item to be equipped</param>
        /// <returns><see langword="true"/> if the item was equipped</returns>
        public bool EquipItem(Item item)
        {
            if (HeldItem == null)
            {
                HeldItem = item;
                equippedItemsManager.Equip(item);
                item.ItemUsed += OnItemUsed;
                ItemPickedUp?.Invoke(this, new ItemPickedUpEventArgs(item));
                meleeWeapon.gameObject.SetActive(false);
                item.OnItemDestroyed += OnHeldItemDestroyed;

                if (HeldItem is FireArm)
                {
                    animator.EquipRifle();
                }
                else if (HeldItem is Bomb)
                {
                    animator.Carry();
                    StartCoroutine(SetCarriedItemTransform(item.transform));
                }

                Sounds.PlayOrQueue(SoundsProvider.GetClip(BotSounds.ItemPickup), 0);

                return true;
            }
            return false;
        }

        private IEnumerator SetCarriedItemTransform(Transform item)
        {
            yield return new WaitForEndOfFrame();
            item.rotation = carriedItemTransform.rotation;
            item.position = carriedItemTransform.position;
        }

        private void OnHeldItemDestroyed(object sender, EventArgs args) => EquipMelee();

        private void EquipMelee()
        {
            meleeWeapon.gameObject.SetActive(true);
            animator.EquipMelee();
        }

        /// <summary>
        /// Tell the bot to try digging the closest digging spot, if there is one within its digging range.
        /// </summary>
        public void TryToDig()
        {
            var closeDiggingSpots = Gamemaster.Instance.GetDiggingSpots()
                .Where(ds => Vector3.Distance(ds.transform.position, gameObject.transform.position) <= diggingRange).ToList()
                .OrderBy(ds => Vector3.Distance(ds.transform.position, gameObject.transform.position));

            if (closeDiggingSpots.Count() == 0)
                return;

            closeDiggingSpots.ElementAt(0).ProcessHit();
            soundsPlayer.PlayIfEmpty(SoundsProvider.GetClip(BotSounds.Dig), 0);
            animator.Dig(true);
            DiggedThisFrame?.Invoke(this, EventArgs.Empty);
        }

        #endregion

        private void ProcessDeath(Vector3? force)
        {
            Debug.Log($"I ({gameObject.name}) died! :'(");
            Alive = false;
            NavigationDriver.enabled = false;
            if (ControlledByPlayer)
            {
                Gamemaster.Instance.GetInputHandler().GoToSelectMode();
            }
            BotDied?.Invoke(this, new BotDiedEventArgs(this));
            if (HeldItem != null)
            {
                DeattachItem();
                HeldItem = null;
            }
            if (ragdoll != null)
            {
                var ragInstance = Instantiate(ragdoll);
                ragInstance.transform.position = transform.position;
                ragInstance.transform.rotation = transform.rotation;
                ragInstance.MatchTransform(this.gameObject);
                ragInstance.Rigidbody.AddForce(rigidBody.velocity, ForceMode.VelocityChange);
                if (force != null)
                {
                    ragInstance.Rigidbody.AddForce(force.Value, ForceMode.Impulse);
                }
                transform.localScale = Vector3.zero;
            }
            Sounds.StopAll();
            Sounds.PlaySound(SoundsProvider.GetClip(BotSounds.Death), false, 0);
        }

        public void KnockBack(Vector3 force)
        {
            const float ItemForceReduction = 0.2f;

            rigidBody.AddForce(force, ForceMode.Impulse);
            animator.KockBack(rigidBody, force);
            lockRotation = true;
            Sounds.PlayOrQueue(SoundsProvider.GetClip(BotSounds.KnockBack), 0);

            if (HeldItem != null)
            {
                DeattachItem();
                HeldItem.Throw(force * ItemForceReduction, gameObject);
                EquipMelee();
                HeldItem = null;
            }

            EventHandler handler = null;
            animator.OnKnockBackFinished += handler = (obj, args) =>
            {
                lockRotation = false;
                animator.OnKnockBackFinished -= handler;
                Sounds.PlayOrQueue(SoundsProvider.GetClip(BotSounds.GettingUp), 0);
            };
        }

        /// <summary>
        /// Sets the new current behaviour for the bot driven by the AI.
        /// If the bot is controlled by the player then this method will not have
        /// any effect
        /// </summary>
        /// <param name="newBehaviour">New behaviour that will be adapted</param>
        public void SetAIBehaviour(AIMBotAIBehaviour newBehaviour)
        {
            AIdriver.SetBehaviour(newBehaviour);
            Sounds.PlayOrQueue(SoundsProvider.GetClip(BotSounds.BehaviourChange), 0);
        }

        private void OnItemUsed(object sender, EventArgs e)
        {
            AttackedOrUsedItem?.Invoke(this, EventArgs.Empty);
        }
        public void OnAttackUsed(object sender, EventArgs e)
        {
            AttackedOrUsedItem?.Invoke(this, EventArgs.Empty);
            if (HeldItem == null)
            {
                Sounds.PlayIfEmpty(SoundsProvider.GetClip(BotSounds.MeleeAttack), 0);
            }
        }


        private Quaternion movementConfusion = Quaternion.identity;
        public bool Hijacked { get; private set; }
        public void Hijack(Quaternion rotationToMovement)
        {
            Hijacked = true;
            movementConfusion = rotationToMovement;
        }
        public void ReleaseControlsHijack()
        {
            Hijacked = false;
            movementConfusion = Quaternion.identity;
        }

        public void OnReleaseControlBlock(object sender, EventArgs args)
        {
            SetAIBehaviour(new ConfusedBehaviour(this));
            ControlBlocked = false;
        }
        #region Bots AI utils
        private List<AttackToken> attackTokens;
        public List<AttackToken> GetAttackTokens()
        {
            if (attackTokens == null)
            {
                attackTokens = AttackToken.GenerateTokensAroundCollider(this.transform, GetComponent<Collider>(), .2f, ArenaData.AgentRadius);
            }
            return attackTokens;
        }


        private void OnDrawGizmos()
        {
            var tokens = GetAttackTokens();
            foreach (var token in tokens)
            {
                if (token.State == TokenState.Open)
                {
                    Gizmos.DrawWireSphere(token.Position, .1f);
                }
                else
                {
                    Gizmos.DrawSphere(token.Position, .1f);
                }
            }
        }

        #endregion

        private CoroutineCancellationToken LALAToken;
        private void PlayLALAPanic(object sender, EventArgs args)
        {
            if (Alive)
            {
                LALAToken = new CoroutineCancellationToken();
                StartCoroutine(LALAPanicScreams(LALAToken));
            }
        }

        private IEnumerator LALAPanicScreams(CoroutineCancellationToken LALAToken)
        {
            while (!LALAToken.CancellationRequested)
            {
                Sounds.PlayIfEmpty(SoundsProvider.GetClip(BotSounds.LALAPaninc), 0);
                yield return new WaitForEndOfFrame();
            }
        }

        private void EndLALAPanic(object sender, EventArgs args)
        {
            LALAToken?.Cancel();
        }
    }
}
