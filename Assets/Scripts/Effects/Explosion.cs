﻿using System;
using System.Linq;
using AIM.Sound;
using UnityEngine;

namespace AIM.Effects
{

    public class Explosion : MonoBehaviour
    {
        [SerializeField]
        private GameObject explosionEffect;
        
        [SerializeField]
        private float explosionRadius = 5.0f;
        [SerializeField]
        private float maxDamage = 50.0f;
        [SerializeField]
        private float explosionForce = 60.0f;

        [SerializeField] private SoundsPlayer soundsPlayer;

        public GameObject cause { get; set; }

        public void DoExplosion()
        {
            // Show explosion effect
            ParticleSystem ps = Instantiate(explosionEffect, transform, false).GetComponent<ParticleSystem>();
            if (ps == null)
            {
                throw new InvalidOperationException($"{nameof(explosionEffect)} does not have a {nameof(ParticleSystem)} component");
            }
            var explosionSound = SharedSoundsMaster.Instance.Weapons.GetClip(WeaponSounds.Explosion);
            soundsPlayer.PlayOneShot(explosionSound);
            Destroy(gameObject, Mathf.Max(ps.main.duration, explosionSound.length));

            // Find nearby bots
            var nearbyBots = Gamemaster.Instance.GetAIMBots().Where(bot => Vector3.Distance(bot.transform.position, transform.position) <= explosionRadius && bot.Alive);

            foreach (var bot in nearbyBots)
            {
                Vector3 direction = bot.transform.position - transform.position;
                direction.y = 0;
                float distance = direction.magnitude;
                direction.Normalize();

                // Damage them
                float damage = maxDamage * (1.0f - distance / explosionRadius);
                bot.ChangeHP(-damage, cause);

                // Play knockback animation
                bot.KnockBack(explosionForce * direction * (explosionRadius - distance) / explosionRadius);
            }

            // Damage boss if nearby
            var boss = Gamemaster.Instance.GetBoss();
            if (boss != null)
            {
                float distance = Vector3.Distance(boss.transform.position, transform.position);
                if (distance <= explosionRadius)
                {
                    float damage = maxDamage * (1.0f - distance / explosionRadius);
                    boss.ChangeHP(-damage);
                }
                var weakpointcontroller = boss.GetWeakPointController();
                foreach (var weakpoint in weakpointcontroller.GetWeakPoints())
                {
                    if (weakpoint.IsActive())
                    {
                        float distance2 = Vector3.Distance(weakpoint.transform.position, transform.position);
                        if (distance2 <= explosionRadius)
                        {
                            float damage = maxDamage * (1.0f - distance2 / explosionRadius);
                            weakpoint.DealBonusDamage(damage);
                        }
                    }
                }
                var eyeweakpoint = weakpointcontroller.GetEyeWeakPoint();
                if (eyeweakpoint.IsActive())
                {
                    float distance3 = Vector3.Distance(eyeweakpoint.transform.position, transform.position);
                    if (distance3 <= explosionRadius)
                    {
                        float damage = maxDamage * (1.0f - distance3 / explosionRadius);
                        eyeweakpoint.DealBonusDamage(damage);
                    }
                }
            }
        }
    }

}
