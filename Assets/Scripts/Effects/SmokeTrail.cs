﻿using System.Collections;
using UnityEngine;

namespace AIM.Effects
{
    [RequireComponent(typeof(ParticleSystem))]
    public class SmokeTrail : MonoBehaviour
    {
        [SerializeField] float killPlaneOffset;
        ParticleSystem[] particleSystems;

        private void Awake()
        {
            particleSystems = GetComponentsInChildren<ParticleSystem>();
        }

        public float StartSpeed
        {
            set
            {
                var main = particleSystems[0].main;
                main.startSpeed = value;
            }
        }

        public void Kill(Vector3 contactPoint) => StartCoroutine(FadeAndDestroy(contactPoint));

        private IEnumerator FadeAndDestroy(Vector3 contactPoint)
        {
            var killPlane = new GameObject("SmokeTrail_KillPlane").transform;
            killPlane.position = contactPoint + transform.forward * killPlaneOffset;
            killPlane.rotation = transform.rotation * Quaternion.Euler(-90, 0, 0);

            foreach (var ps in particleSystems)
            {
                var emission = ps.emission;
                emission.enabled = false;

                var collision = ps.collision;
                collision.enabled = true;
                collision.SetPlane(0, killPlane);
                collision.lifetimeLoss = 1;
            }

            yield return new WaitUntil(() => CheckAllParticlesDead());

            Destroy(gameObject);
            Destroy(killPlane.gameObject);
        }

        private bool CheckAllParticlesDead()
        {
            foreach (var ps in particleSystems)
            {
                if (ps.particleCount > 0)
                {
                    return false;
                }
            }
            return true;
        }
    }
}

