﻿using UnityEngine;
using UnityEngine.SceneManagement;

using System;
using System.Collections;

using AIM.Boss;
using AIM.Bots;
using AIM.Bots.AI;
using System.Text;

namespace AIM.Tutorial
{

    public class Tutorial_Scene_3 : Tutorial
    {

        protected override string GetNextScene()
        {
            return "Tutorial_3a";
        }
        protected override string GetSceneText()
        {
            var sb = new StringBuilder();
            sb.AppendLine("Bots have several different moods:");
            sb.AppendLine("Aggressive (red head)");
            sb.AppendLine("Confused (blue head)");
            sb.AppendLine("Coward (yellow head)");
            sb.AppendLine("Digger (dark green head)");
            sb.AppendLine("Evil (black head)");
            sb.AppendLine("Staring (green head)");
            sb.AppendLine();
            sb.AppendLine("Aggressive bots may attack each other!");
            sb.AppendLine();
            sb.AppendLine("Get all bots aggressive by performing attacks!");
            sb.AppendLine("Let go of the control after all the bots are attacking the boss!");

            return sb.ToString();
        }

        protected override void ShortlyAfterStart()
        {
            base.ShortlyAfterStart();

            foreach (var bot in Gamemaster.Instance.GetAIMBots())
            {
                bot.BotDied += SceneFailed;
            }
        }

        protected override void SceneFailed(object sender, EventArgs e)
        {
            if (sender is AIMBotController)
                ReloadScene("One of your bots died!");
            else if (sender is BossController)
                ReloadScene("The boss died!");
        }

        private void Update()
        {
            TestForGoalCondition();
        }

        private void TestForGoalCondition()
        {
            bool conditionMet = true;
            foreach (var bot in Gamemaster.Instance.GetAIMBots())
            {
                conditionMet &= bot.CurrentBehaviour is AggressiveBehaviour && (bot.CurrentBehaviour as AggressiveBehaviour).GetAggressionTarget() == Gamemaster.Instance.GetBoss().gameObject;
            }

            if (conditionMet)
            {
                LoadNextScene("Nice!");
            }
        }
    }

}
