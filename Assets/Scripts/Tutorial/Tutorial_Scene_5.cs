﻿using UnityEngine;
using UnityEngine.SceneManagement;

using System;
using System.Collections;

using AIM.Bots;
using System.Text;

namespace AIM.Tutorial
{

    public class Tutorial_Scene_5 : Tutorial
    {
        protected override string GetNextScene()
        {
            return "Tutorial_6";
        }
        protected override string GetSceneText()
        {
            var sb = new StringBuilder();
            sb.AppendLine("Dang! The boss is creating an electric field on the floor!");
            sb.AppendLine();
            sb.AppendLine("Better make some bots hit all of those pillars simultaneously!");
            return sb.ToString();
        }

        protected override void Start()
        {
            base.Start();

            var boss = Gamemaster.Instance.GetBoss();
            boss.SetToTut5Behaviour();
            boss.ChangeHP(99000);
            StartCoroutine(GetElectricField());
        }


        private IEnumerator GetElectricField()
        {
            var boss = Gamemaster.Instance.GetBoss();
            var field = boss.GetElectricField();
            while (field == null)
            {
                yield return null;
                field = boss.GetElectricField();
            }
            field.FieldDestroyed += SceneCompleted;
            Debug.Log("field found");
        }
    }

}
