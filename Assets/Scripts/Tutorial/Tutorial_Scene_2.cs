﻿using UnityEngine;

using System;
using System.Collections;
using System.Text;

using AIM.Boss;
using AIM.Bots;
using AIM.Environment;
using AIM.Items;

namespace AIM.Tutorial
{

    public class Tutorial_Scene_2 : Tutorial
    {
        [SerializeField]
        private BossController boss;
        [SerializeField]
        private AIMBotController bot;
        [SerializeField]
        private DiggingSpot diggingSpotPrefab;
        [SerializeField]
        private Item itemToSpawnPrefab;

        protected override string GetSceneText()
        {
            var sb = new StringBuilder();
            sb.AppendLine("Hold <b>Right-Click / X</b> (3 secs) = uncover hidden item in closest digging spot.");
            sb.AppendLine();
            sb.AppendLine("<b>F / Left trigger</b> = throw item");
            sb.AppendLine();
            sb.AppendLine("Bomb has 10 second timer.");
            sb.AppendLine();
            sb.AppendLine();
            sb.AppendLine("Damage the boss!");
            return sb.ToString();
        }

        protected override string GetSceneCompletedText() { return "Good job!"; }
        protected override string GetSceneFailedText() { return "Try not to get killed in the process!"; }

        protected override void Start()
        {
            NewDiggingSpot();
            
            bot.BotDied += SceneFailed;
            boss.BossHpChanged += TestForDamage;

            base.Start();
        }

        private void TestForDamage(object sender, EventArgs e)
        {
            var diff = ((BossHpChangedEventArgs)e).Difference;
            if (diff < 0)
                SceneCompleted(null, null);
        }

        private void StartNewDiggingSpotCoroutine(object sender, EventArgs e)
        {
            StartCoroutine(NewDiggingSpotAfterSeconds(3.0f));
        }

        private IEnumerator NewDiggingSpotAfterSeconds(float seconds)
        {
            yield return new WaitForSeconds(seconds);
            NewDiggingSpot();
        }

        private void NewDiggingSpot()
        {
            float x = UnityEngine.Random.Range(-4.0f, 4.0f);
            float z = UnityEngine.Random.Range(-15.0f, -11.0f);

            var ds = Instantiate(diggingSpotPrefab, new Vector3(x, 0.1f, z), Quaternion.identity);
            ds.SetItemToSpawnPrefab(itemToSpawnPrefab);
            ds.DugUp += StartNewDiggingSpotCoroutine;
        }

        protected override string GetNextScene()
        {
            return "Tutorial_3";
        }
    }
}
