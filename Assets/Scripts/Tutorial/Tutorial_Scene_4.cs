﻿using UnityEngine;
using UnityEngine.SceneManagement;

using System.Collections;
using System;

using AIM.Environment;
using AIM.Bots;
using AIM.Bots.AI;
using AIM.PlayerInput;
using System.Text;

namespace AIM.Tutorial
{

    public class Tutorial_Scene_4 : Tutorial
    {
        [SerializeField]
        RecalibrationTerminal recalibrationTerminal;
        [SerializeField]
        AIMBotController playersBot;
        [SerializeField]
        AIMBotController botPrefab1;
        [SerializeField]
        AIMBotController botPrefab2;
        [SerializeField]
        AIMBotController botPrefab3;

        protected override string GetNextScene()
        {
            return "Tutorial_5";
        }
        protected override string GetSceneText()
        {
            var sb = new StringBuilder();
            sb.AppendLine("Oh no! The boss is possessing your bot! Get it to the recalibration terminal on the bottom left before it kills the other bots!");
            sb.AppendLine("The controls of corrupt bots are funny!");
            sb.AppendLine();
            sb.AppendLine("Green Head = Can't move on its own.");
            sb.AppendLine("Black Head = Malevolent. Damages other bots.");
            sb.AppendLine("(Control of bot will be unblocked after the attack!)");
            return sb.ToString();
        }

        protected override string GetSceneFailedText() { return "One of the bots died!"; }

        protected override void Start()
        {
            base.Start();

            var boss = Gamemaster.Instance.GetBoss();
            //boss.SetToTut4Behaviour();
            boss.ActivateBoss();
            boss.BossDied += SceneFailed;

            recalibrationTerminal.RecalibrationPerformed += SceneCompleted;

            StartCoroutine(SpawnOtherBots());
        }

        private IEnumerator SpawnOtherBots()
        {
            yield return new WaitForSeconds(7.0f);

            var bot1 = Instantiate(botPrefab1, new Vector3(-11, 1, 5), Quaternion.identity);
            bot1.ControlBlocked = true;
            bot1.BotDied += SceneFailed;
            bot1.gameObject.name = "bot1";

            var bot2 = Instantiate(botPrefab2, new Vector3(0, 1, 5), Quaternion.identity);
            bot2.ControlBlocked = true;
            bot2.BotDied += SceneFailed;
            bot2.gameObject.name = "bot2";
            var bot3 = Instantiate(botPrefab3, new Vector3(-5, 1, -5), Quaternion.identity);
            bot3.ControlBlocked = true;
            bot3.BotDied += SceneFailed;
            bot3.gameObject.name = "bot3";
        }
    }

}
