﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

using System;
using System.Collections;
using TMPro;

namespace AIM.Tutorial
{

    public abstract class Tutorial : MonoBehaviour
    {
        [SerializeField]
        private GameObject panelTutExpl;
        [SerializeField]
        private GameObject panelCompFail;

        protected abstract string GetSceneText();


        protected virtual string GetSceneCompletedText() { return "Congrats! You beat the level."; }
        protected virtual string GetSceneFailedText() { return "You failed the level. Try again!"; }

        protected virtual void SceneCompleted(object sender, EventArgs e) { LoadNextScene(GetSceneCompletedText()); }
        protected virtual void SceneFailed(object sender, EventArgs e) { ReloadScene(GetSceneFailedText()); }

        private bool compFailSet = false;

        protected abstract string GetNextScene();

        // Start

        protected virtual void Start()
        {
            StartCoroutine(CallShortlyAfterStart());
            panelTutExpl.GetComponentInChildren<TMP_Text>().text = "";
        }

        private IEnumerator CallShortlyAfterStart()
        {
            yield return new WaitForSeconds(0.5f);
            StartCoroutine(ShowText(panelTutExpl.GetComponentInChildren<TMP_Text>(), GetSceneText(), 3));
            ShortlyAfterStart();
        }

        protected virtual void ShortlyAfterStart()
        {
            var boss = Gamemaster.Instance.GetBoss();
            if (boss != null)
            {
                boss.ChangeHP(99000);
                boss.BossDied += SceneFailed;
            }
        }

        // End

        protected void LoadNextScene(string text)
        {
            StartCoroutine(LoadScene(text, GetNextScene()));
        }

        protected void ReloadScene(string text)
        {
            StartCoroutine(LoadScene(text, SceneManager.GetActiveScene().name));
        }

        private IEnumerator LoadScene(string text, string sceneToLoad)
        {
            panelTutExpl.SetActive(false);
            if (!compFailSet)
            {
                panelCompFail.SetActive(true);
                StartCoroutine(ShowText(panelCompFail.GetComponentInChildren<TMP_Text>(), text, 1));
                compFailSet = true;
            }

            yield return new WaitForSeconds(5.0f);
            SceneManager.LoadScene(sceneToLoad);
        }

        private IEnumerator ShowText(TMP_Text tmp, string text, float timeToShow)
        {
            tmp.text = "";
            var timeElapsed = 0f;
            while (timeElapsed < timeToShow)
            {
                yield return null;
                timeElapsed += Time.deltaTime;
                tmp.text = text.Substring(0, Mathf.Min(Mathf.Max(1, Mathf.FloorToInt(timeElapsed / timeToShow * text.Length)), text.Length - 1));
            }
            tmp.text = text;
        }
    }

}
