﻿using UnityEngine;

using System;
using System.Collections;
using System.Text;

using AIM.Bots;
using AIM.Boss;
using AIM.Environment;
using AIM.Items;

namespace AIM.Tutorial
{

    public class Tutorial_Scene_3a : Tutorial
    {
        [SerializeField]
        private DiggingSpot diggingSpotPrefab;
        [SerializeField]
        private Item itemToSpawnPrefab;
        [SerializeField]
        private Bomb bombPrefab;

        protected override string GetNextScene()
        {
            return "Tutorial_4";
        }
        protected override string GetSceneText()
        {
            var sb = new StringBuilder();
            sb.AppendLine("Bots get ...");
            sb.AppendLine();
            sb.AppendLine();
            sb.AppendLine("... aggressive, when performing attacks.");
            sb.AppendLine();
            sb.AppendLine("... confused, when nothing noteworthy happens.");
            sb.AppendLine();
            sb.AppendLine("... cowardly, when they take lots of damage.");
            sb.AppendLine();
            sb.AppendLine("... a digger, when they dig for a while.");
            sb.AppendLine();
            sb.AppendLine();
            sb.AppendLine("Get all bots into different moods!");

            return sb.ToString();
        }

        protected override void Start()
        {
            StartCoroutine(DiggingSpotSpawnCycle());
            StartCoroutine(BombSpawnCycle());

            SpawnBomb();

            foreach (var bot in Gamemaster.Instance.GetAIMBots())
                bot.AIdriver.BehaviourChanged += TestForGoalCondition;

            base.Start();
        }

        // Digging Spots (top left)

        private IEnumerator DiggingSpotSpawnCycle()
        {
            yield return new WaitForSeconds(3.0f);

            if (Gamemaster.Instance.GetDiggingSpots().Count <= 3)
                NewDiggingSpot();

            StartCoroutine(DiggingSpotSpawnCycle());
        }

        private void NewDiggingSpot()
        {
            float x = UnityEngine.Random.Range(-8.0f, 1.0f);
            float z = UnityEngine.Random.Range(-4.0f, 6.0f);

            var ds = Instantiate(diggingSpotPrefab, new Vector3(x, 0.1f, z), Quaternion.identity);
            ds.SetItemToSpawnPrefab(itemToSpawnPrefab);
        }

        // Bombs (top right)

        private IEnumerator BombSpawnCycle()
        {
            yield return new WaitForSeconds(13.0f);
            SpawnBomb();
            StartCoroutine(BombSpawnCycle());
        }

        private void SpawnBomb()
        {
            Instantiate(bombPrefab, new Vector3(19.0f, 1.0f, 6.0f), Quaternion.identity);
        }

        // Goal Condition

        private void TestForGoalCondition(object sender, EventArgs e)
        {
            bool allDifferent = true;
            var botList = Gamemaster.Instance.GetAIMBots();
            for (int i = 0; i < botList.Count - 1; i++)
            {
                for (int j = i + 1; j < botList.Count; j++)
                {
                    allDifferent &= botList[i].CurrentBehaviour.GetType() != botList[j].CurrentBehaviour.GetType();
                }
            }

            if (allDifferent)
                SceneCompleted(null, null);
        }
    }

}
