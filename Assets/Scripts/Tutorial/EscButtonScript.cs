﻿using UnityEngine;
using UnityEngine.SceneManagement;

using System.Collections;

namespace AIM.Tutorial
{

    public class EscButtonScript : MonoBehaviour
    {
        public void BackToMainMenu()
        {
            SceneManager.LoadScene("Game");
        }
    }

}
