﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AIM.Tutorial
{
    [RequireComponent(typeof(Collider))]
    public class CubeTrigger : MonoBehaviour
    {
        public event EventHandler Triggered;

        private void OnTriggerEnter(Collider other)
        {
            Triggered?.Invoke(this, new TriggerEventArgs(other.gameObject));
        }
    }
}
