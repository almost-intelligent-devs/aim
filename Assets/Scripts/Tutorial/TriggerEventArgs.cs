﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AIM.Tutorial
{
    public class TriggerEventArgs : EventArgs
    {
        public GameObject Triggerer
        {
            get; private set;
        }

        public TriggerEventArgs(GameObject obj)
        {
            Triggerer = obj;
        }
    }
}
