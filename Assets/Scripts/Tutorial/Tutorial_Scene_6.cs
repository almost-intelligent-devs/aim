﻿using UnityEngine;
using UnityEngine.SceneManagement;

using System;
using System.Collections;
using System.Collections.Generic;

using AIM.Boss.Weakpoints;
using System.Text;

namespace AIM.Tutorial
{

    public class Tutorial_Scene_6 : Tutorial
    {
        protected override string GetNextScene()
        {
            return "Game";
        }
        protected override string GetSceneText()
        {
            var sb = new StringBuilder();
            sb.AppendLine("Each button reveals one of the bosses' weakpoints for a short period of time!");
            sb.AppendLine("Deal bonus damage to a weakpoint of the boss!");
            return sb.ToString();
        }

        protected override string GetSceneCompletedText() { return "Congrats! You beat the tutorial. Now off to a real challenge!"; }

        private Dictionary<BossWeakPoint, bool> weakpointHitStatus;

        protected override void ShortlyAfterStart()
        {
            var boss = Gamemaster.Instance.GetBoss();
            boss.SetToTut6Behaviour();

            var weakpoints = boss.GetWeakPointController().GetWeakPoints();
            weakpointHitStatus = new Dictionary<BossWeakPoint, bool>();
            foreach (var weakpoint in weakpoints)
            {
                weakpoint.WeakpointHit += OnWeakpointHit;
                weakpointHitStatus.Add(weakpoint, false);
            }
        }

        private void OnWeakpointHit(object sender, EventArgs e)
        {
            weakpointHitStatus[(BossWeakPoint)sender] = true;

            SceneCompleted(null, null);
        }

    }

}
