﻿using UnityEngine;
using UnityEngine.SceneManagement;

using System.Collections;

using AIM.Controllables;
using System;
using System.Text;

namespace AIM.Tutorial
{

    public class Tutorial_Scene_1 : Tutorial
    {
        [SerializeField]
        private Tut1Controllable[] buttons;

        [SerializeField]
        private Door door;

        protected override string GetSceneText()
        {
            var sb = new StringBuilder();
            sb.AppendLine("Welcome to A.I.M. tutorial.");
            sb.AppendLine();
            sb.AppendLine("Use <b>WASD / Left stick</b> to move");
            sb.AppendLine();
            sb.AppendLine("Use <b>SPACE / A</b> to take control of the bot and to roll");
            sb.AppendLine();
            sb.AppendLine("<b>TAB / B</b> to stop controlling the bot");
            sb.AppendLine();
            sb.AppendLine("Use <b>Left Mouse Button / Right trigger</b> to attack");
            sb.AppendLine();
            sb.AppendLine();
            sb.AppendLine("Activate all buttons to leave the facility");
            return sb.ToString();
        }

        public void NotifyChangeInButtonStates()
        {
            bool b = true;
            foreach (var button in buttons)
                b &= button.IsActive();

            if (b)
                door.Activate(null);
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.tag == TagDictionary.Bot)
                LoadNextScene("Well done!");
        }

        protected override string GetNextScene()
        {
            return "Tutorial_2";
        }
    }

}
