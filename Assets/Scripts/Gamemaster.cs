﻿using AIM.Boss;
using AIM.Bots;
using AIM.Environment;
using AIM.Interactables;
using AIM.Items;
using AIM.PlayerInput;
using System;
using System.Collections.Generic;

namespace AIM
{
    /// <summary>
    /// The connecting point for all the game objects. Overload <see cref="Register"/> method to add game objects
    /// that should be available to other game objects and add a corresponding GetX method for other classes to use.
    /// Call <see cref="Register"/> in the Start section of your game object script.
    /// </summary>
    public class Gamemaster : Singleton<Gamemaster>
    {
        public event EventHandler OnAllBotsDied;
        public event EventHandler OnGameFinished;

        private List<AIMBotController> registeredBots = new List<AIMBotController>();
        private BossController boss;
        private List<Interactable> registeredInteractables = new List<Interactable>();
        private List<DiggingSpot> diggingSpots = new List<DiggingSpot>();
        private InputHandler input;
        public CameraController CameraController { get; private set; }

        public bool ApplicationQuit { get => IsApplicationQuitting; }

        private int aliveBots = 0;

        #region Bots

        /// <summary>
        /// Register <see cref="AIMBotController"/>. Will throw <see cref="InvalidOperationException"/>on double registration attempt.
        /// </summary>
        /// <param name="bot"><see cref="AIMBotController"/>to be registered</param>
        public void Register(AIMBotController bot)
        {
            if (registeredBots.Contains(bot))
            {
                throw new InvalidOperationException($"Trying to register the same {nameof(AIMBotController)} twice!");
            }
            else
            {
                registeredBots.Add(bot);
                aliveBots++;
                bot.BotDied += (sender, args) =>
                {
                    aliveBots--;
                    if (aliveBots == 0)
                    {
                        OnAllBotsDied?.Invoke(this, EventArgs.Empty);
                        OnGameFinished?.Invoke(this, EventArgs.Empty);
                    }
                };
            }
        }

        /// <summary>
        /// Get list of all the registered <see cref="AIMBotController"/>. Will return an empty list if no bots were registered.
        /// </summary>
        /// <returns></returns>
        public List<AIMBotController> GetAIMBots()
        {
            return registeredBots;
        }

        #endregion

        #region Boss

        public bool HardMode { get; set; }

        /// <summary>
        /// Register <see cref="BossController"/>. Will throw <see cref="InvalidOperationException"/>on double registration attempt.
        /// </summary>
        /// <param name="boss"><see cref="BossController"/>to be registered</param>
        public void Register(BossController boss)
        {
            if (this.boss != null)
            {
                throw new InvalidOperationException($"Trying to register second boss!");
            }
            else
            {
                this.boss = boss;
                boss.BossDeathAnimationFinished += (sender, args) => { OnGameFinished?.Invoke(this, EventArgs.Empty); };
            }
        }

        /// <summary>
        /// Get registered <see cref="BossController"/>. Will return null if boss has not been registered;
        /// </summary>
        public BossController GetBoss()
        {
            return boss;
        }

        #endregion

        #region Interactables
        /// <summary>
        /// Register <see cref="Interactable"/>. Will throw <see cref="InvalidOperationException"/>on double registration attempt.
        /// </summary>
        /// <param name="interactable"><see cref="Interactable"/>to be registered</param>
        public void Register(Interactable interactable)
        {
            if (registeredInteractables.Contains(interactable))
            {
                throw new InvalidOperationException($"Trying to register the same {nameof(Interactable)} twice!");
            }
            else
            {
                registeredInteractables.Add(interactable);
            }
        }

        /// <summary>
        /// Get list of all the registered <see cref="Interactable"/>. Will return an empty list if no interactables were registered.
        /// </summary>
        /// <returns></returns>
        public List<Interactable> GetInteractables()
        {
            return registeredInteractables;
        }

        /// <summary>
        /// Remove the <see cref="Interactable"/> from the registered interactables list.
        /// </summary>
        /// <param name="interactable"></param>
        public void Unregister(Interactable interactable)
        {

            registeredInteractables.Remove(interactable);
        }

        #endregion

        #region Items

        private List<Item> registeredItems = new List<Item>();

        /// <summary>
        /// Register <see cref="Item"/>. Will throw <see cref="InvalidOperationException"/>on double registration attempt.
        /// </summary>
        /// <param name="item"><see cref="Item"/>to be registered</param>
        public void Register(Item item)
        {
            if (registeredItems.Contains(item))
            {
                throw new InvalidOperationException($"Trying to register the same {nameof(item)} twice!");
            }
            else
            {
                registeredItems.Add(item);
            }
        }

        /// <summary>
        /// Get list of all the registered <see cref="Item"/>. Will return an empty list if no interactables were registered.
        /// </summary>
        /// <returns></returns>
        public List<Item> GetItems()
        {
            return registeredItems;
        }

        /// <summary>
        /// Remove the <see cref="Item"/> from the registered items list.
        /// </summary>
        /// <param name="item"></param>
        public void Unregister(Item item)
        {

            registeredItems.Remove(item);
        }

        #endregion

        #region Digging Spots

        /// <summary>
        /// Register <see cref="DiggingSpot"/>. Will throw <see cref="InvalidOperationException"/>on double registration attempt.
        /// </summary>
        /// <param name="ds"><see cref="DiggingSpot"/>to be registered</param>
        public void Register(DiggingSpot ds)
        {
            if (diggingSpots.Contains(ds))
            {
                throw new InvalidOperationException($"Trying to register the same {nameof(ds)} twice!");
            }
            else
            {
                diggingSpots.Add(ds);
            }
        }

        /// <summary>
        /// Get list of all the registered <see cref="DiggingSpot"/>. Will return an empty list if no digging spots were registered.
        /// </summary>
        /// <returns></returns>
        public List<DiggingSpot> GetDiggingSpots()
        {
            return diggingSpots;
        }

        /// <summary>
        /// Remove the <see cref="DiggingSpot"/> from the registered digging spots list.
        /// </summary>
        /// <param name="ds"></param>
        public void Unregister(DiggingSpot ds)
        {

            diggingSpots.Remove(ds);
        }
        #endregion

        #region Camera Controller

        /// <summary>
        /// Register <see cref="PlayerInput.CameraController"/>. Will throw <see cref="InvalidOperationException"/>on double registration attempt.
        /// </summary>
        /// <param name="cameraController"><see cref="CameraController"/>to be registered</param>
        public void Register(CameraController cameraController)
        {
            if (CameraController != null)
            {
                throw new InvalidOperationException($"Trying to register second camera controller!");
            }
            else
            {
                CameraController = cameraController;
            }
        }

        #endregion

        #region Input
        public void Register(InputHandler input)
        {
            if (this.input != null)
            {
                throw new InvalidOperationException($"Trying to register second inputHandler!");
            }
            else
            {
                this.input = input;
            }
        }

        public InputHandler GetInputHandler()
        {
            return this.input;
        }
        #endregion
    }
}
