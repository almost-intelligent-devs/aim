﻿using UnityEngine;
using UnityEngine.AI;

using System.Collections;
using System.Collections.Generic;

using AIM.Items;
using System;
using AIM.Boss;

namespace AIM.Environment
{
    [Serializable]
    public class ItemFloatPair
    {
        public Item item;
        public float weight;
    }

    public class DiggingSpotGenerator : MonoBehaviour
    {
        [SerializeField]
        private ItemFloatPair[] spawnPool;
        [SerializeField]
        private DiggingSpot diggingSpotPrefab;

        [SerializeField]
        private float minSpawnDelay = 10.0f;
        [SerializeField]
        private float maxSpawnDelay = 30.0f;
        [SerializeField]
        private int maxNoDiggingSpots = 5;

        private Item[] spawnableItems;
        private float[] weightThresholds;
        private float sumOfWeights;

        private void Start()
        {
            var n = spawnPool.Length;
            spawnableItems = new Item[n];
            weightThresholds = new float[n];

            float currentWeight = 0;
            for (int i = 0; i < n; i++)
            {
                var pair = spawnPool[i];
                currentWeight += pair.weight;
                spawnableItems[i] = pair.item;
                weightThresholds[i] = currentWeight;
            }
            sumOfWeights = currentWeight;
            var boss = Gamemaster.Instance.GetBoss();
            if (boss == null)
            {
                StartCoroutine(SpawnCycle());
            }
            else
            {
                boss.BossPhaseChanged += BossPhaseChanged;
            }
        }

        private void BossPhaseChanged(object sender, EventArgs e)
        {
            var args = (BossPhaseChangedEventArgs) e;
            if(args.Phase == 1)
            {
                StartCoroutine(SpawnCycle());
            }
        }

        private IEnumerator SpawnCycle()
        {
            yield return new WaitForSeconds(UnityEngine.Random.Range(minSpawnDelay, maxSpawnDelay));
            if (Gamemaster.Instance.GetDiggingSpots().Count < maxNoDiggingSpots)
                SpawnDiggingSpotWithRandomContent(NavMeshHelper.RandomPositionOnNavMesh());

            StartCoroutine(SpawnCycle());
        }

        private void SpawnDiggingSpotWithRandomContent(Vector3 position)
        {
            float f = (float)((new System.Random()).NextDouble() * sumOfWeights);
            int n = Array.BinarySearch(weightThresholds, f);
            if (n < 0)
                n = ~n;

            var ds = Instantiate(diggingSpotPrefab, position, Quaternion.identity);
            ds.SetItemToSpawnPrefab(spawnableItems[n]);
        }

    }

}
