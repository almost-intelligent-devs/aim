﻿using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace AIM.Environment
{
    public class BossActivator : MonoBehaviour
    {
        private void OnTriggerEnter(Collider other)
        {
            if (other.tag == TagDictionary.Bot)
            {
                Gamemaster.Instance.GetBoss().ActivateBoss();
                Destroy(this.gameObject);
            }
        }

#if UNITY_EDITOR

        private void OnDrawGizmos()
        {

            var collider = GetComponent<BoxCollider>();
            if (collider)
            {
                Vector3 center = transform.position + collider.center;
                Gizmos.DrawWireCube(center, Vector3.Scale(collider.size, transform.localScale));
                var style = new GUIStyle() { alignment = TextAnchor.MiddleCenter };
                style.normal.textColor = Color.white;
                Handles.Label(center, nameof(BossActivator), style);
            }
        }
#endif
    }
}