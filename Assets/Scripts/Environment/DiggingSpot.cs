﻿using UnityEngine;

using System;
using System.Collections.Generic;

using AIM.Items;

namespace AIM.Environment
{
    public class DiggingSpot : MonoBehaviour
    {
        [SerializeField]
        private Item itemToSpawnPrefab;

        private float leftOverDiggingTime;
        // Stores the frames where hits were perceived.
        private HashSet<int> recentHits;

        public event EventHandler DugUp;

        private void Start()
        {
            leftOverDiggingTime = ArenaData.DiggingTime;
            recentHits = new HashSet<int>();
            Gamemaster.Instance.Register(this);
        }

        private void Update()
        {
            int lastFrame = Time.frameCount - 1;
            if (!recentHits.Contains(lastFrame))
                ProcessCancellation();
            else
                recentHits.Remove(lastFrame);
        }

        public void SetItemToSpawnPrefab(Item itemToSpawnPrefab)
        {
            this.itemToSpawnPrefab = itemToSpawnPrefab;
        }

        public void ProcessHit()
        {
            leftOverDiggingTime -= Time.deltaTime;
            if (leftOverDiggingTime <= 0)
                SpawnItem();

            recentHits.Add(Time.frameCount);
        }

        private void ProcessCancellation()
        {
            leftOverDiggingTime = ArenaData.DiggingTime;
        }

        private void SpawnItem()
        {
            Instantiate(itemToSpawnPrefab, transform.position + Vector3.up * 0.25f, Quaternion.identity);
            Gamemaster.Instance.Unregister(this);
            DugUp?.Invoke(this, EventArgs.Empty);
            Destroy(gameObject);
        }
    }
}
