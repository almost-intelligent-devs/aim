﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AIM.Bots;
using AIM.Bots.AI;
using AIM.Sound;
using System;

namespace AIM.Environment
{
    /// <summary>
    /// When a controlled bot steps on the platform of the Recalibration Terminal
    /// it will be locked in place at the terminal for the recalibration duration and then cleansed from the controll effect
    /// </summary>
    [RequireComponent(typeof(MeshRenderer))]
    public class RecalibrationTerminal : MonoBehaviour
    {
        [SerializeField]
        private float recalibrationDuration;
        [SerializeField]
        private Transform monitor;
        private bool recalibrating = false;

        private Material platformMat;
        private Color standardColor;
        private float intensity = 5;

        public event EventHandler RecalibrationPerformed;
        protected SoundsPlayer soundsPlayer;

        private void Awake()
        {
            platformMat = this.GetComponent<MeshRenderer>().material;
            standardColor = platformMat.GetColor("_EmissionColor");

            soundsPlayer = GetComponentInChildren<SoundsPlayer>();
            if (soundsPlayer == null)
            {
                throw new InvalidOperationException($"{nameof(soundsPlayer)} is not specified!");
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.tag == TagDictionary.Bot && !recalibrating)
            {
                AIMBotController bot = other.GetComponent<AIMBotController>();
                if(bot.Corrupted)
                {
                    if (!bot.ControlBlocked)
                    {
                        StartCoroutine(Recalibration(bot));
                    }
                    else
                    {
                        Debug.Log("nope");
                    }
                }
            }
        }

        /// <summary>
        /// While recalibrating bot cannot do anything and platform lights up
        /// </summary>
        /// <param name="bot"></param>
        private IEnumerator Recalibration(AIMBotController bot)
        {
            recalibrating = true;
            platformMat.SetColor("_EmissionColor", standardColor * intensity);

            bot.SetAIBehaviour(new StareAtPointBehaviour(bot, monitor));
            soundsPlayer.PlayOneShot(SharedSoundsMaster.Instance.Environment.GetClip(EnvironmentSounds.RecalibrationStarted));
            float timer = 0;
            //maybe replace the teleportation with moveToPosition from Bot
            while (timer < recalibrationDuration)
            {
                bot.transform.position = this.transform.position;
                timer += Time.deltaTime;
                yield return new WaitForEndOfFrame();
            }
            bot.SetAIBehaviour(new ConfusedBehaviour(bot));
            platformMat.SetColor("_EmissionColor", standardColor);
            soundsPlayer.PlayOneShot(SharedSoundsMaster.Instance.Environment.GetClip(EnvironmentSounds.RecalibrationComplete));
            recalibrating = false;
            RecalibrationPerformed?.Invoke(this, EventArgs.Empty);
        }

    }
}
