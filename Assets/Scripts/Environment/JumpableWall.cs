﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AIM.Boss;

namespace AIM.Environment {
    /// <summary>
    /// One of the 4 inner walls - Boss can jump to this position and wall will vanish/emerge
    /// </summary>
    public class JumpableWall : MonoBehaviour
    {
        private const float vanishTime = 0.1f;
        private float height;
        private Vector3 originPosition;
        private void Start()
        {
            height = this.GetComponent<BoxCollider>().size.y;
            originPosition = this.transform.position;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.tag == TagDictionary.BossJumpTrigger)
            {
                other.transform.GetComponentInParent<BossController>().SetPositionPoint(this);
                StartCoroutine(VanishAnimation());
            }
        }

        private IEnumerator VanishAnimation()
        {
            float timer = 0;
            while(timer < vanishTime)
            {
                yield return new WaitForEndOfFrame();
                timer += Time.deltaTime;
                this.transform.Translate(Vector3.down * Time.deltaTime * height / vanishTime);
            }
            this.gameObject.SetActive(false);
        }

        public void Reappear()
        {
            this.gameObject.SetActive(true);
            StartCoroutine(ReappearAnimation());
        }

        private IEnumerator ReappearAnimation()
        {
            float timer = 0;
            while (timer < vanishTime)
            {
                yield return new WaitForEndOfFrame();
                timer += Time.deltaTime;
                this.transform.Translate(Vector3.up * Time.deltaTime * height / vanishTime);
            }
            this.transform.position = originPosition;
        }
    }
}