﻿using System;

using UnityEngine;

using AIM.Boss;
using AIM.Interactables;

namespace AIM.Environment
{

    public class ButtonActivator : MonoBehaviour
    {
        [SerializeField]
        private GameObject[] objectsToActivate;

        private void Start()
        {
            Gamemaster.Instance.GetBoss().BossPhaseChanged += OnEnterNewPhase;
        }

        private void OnEnterNewPhase(object sender, EventArgs e)
        {
            int phase = ((BossPhaseChangedEventArgs)e).Phase;
            if (phase == 2)
            {
                foreach (var obj in objectsToActivate)
                {
                    obj.SetActive(true);
                    var button = obj.GetComponentInChildren<Button>();
                    button?.SetInteractable(true);
                }
            }
            else
            {
                foreach (var obj in objectsToActivate)
                {
                    obj.SetActive(false);
                    var button = obj.GetComponentInChildren<Button>();
                    button?.SetInteractable(false);
                }
            }
        }
    }

}
