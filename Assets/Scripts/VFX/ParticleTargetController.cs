﻿using System.Collections.Generic;
using UnityEngine;

namespace AIM.VFX
{

    public class ParticleTargetController : MonoBehaviour
    {
        [SerializeField] private List<ParticleSystem> particleSystems;
        [SerializeField] private float killPlaneOffset;
        [SerializeField] private GameObject plane;

        public Transform Target { get; set; }

        void Update()
        {
            if (Target)
            {
                Vector3 direction = Target.position - transform.position;
                transform.rotation = Quaternion.LookRotation(direction);

                plane.transform.position = Target.position + direction.normalized * killPlaneOffset;
                plane.transform.rotation = transform.rotation * Quaternion.Euler(-90, 0, 0);
            }
        }
    }
}