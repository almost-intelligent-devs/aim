﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.ParticleSystem;

namespace AIM.VFX
{
    public class ParticleColorChanger : MonoBehaviour
    {
        [SerializeField] private List<ParticleSystem> particleSystems;
        [SerializeField] private float blendingTime = 1f;

        private CoroutineCancellationToken cancelLightChange;

        private void Awake()
        {
            if (particleSystems == null || particleSystems.Count == 0)
            {
                throw new InvalidOperationException($"{nameof(particleSystems)} is not specified!");
            }
            ApplyColorAndIntensityChange(particleSystems[0].colorOverLifetime.color.gradient.colorKeys[0].color, 0);
        }

        public void ChangeColor(Color newColor)
        {
            cancelLightChange?.Cancel();
            cancelLightChange = new CoroutineCancellationToken();
            StartCoroutine(AnimateColorAndIntensityChange(newColor, 0, cancelLightChange));
        }

        public void ChangeIntensity(float intensityChange)
        {
            Color color = particleSystems[0].colorOverLifetime.color.gradient.colorKeys[0].color;
            cancelLightChange?.Cancel();
            cancelLightChange = new CoroutineCancellationToken();
            StartCoroutine(AnimateColorAndIntensityChange(color, intensityChange, cancelLightChange));
        }

        public void ChangeColorAndIntensity(Color newColor, float intensityChange)
        {
            cancelLightChange?.Cancel();
            cancelLightChange = new CoroutineCancellationToken();
            StartCoroutine(AnimateColorAndIntensityChange(newColor, intensityChange, cancelLightChange));
        }

        private IEnumerator AnimateColorAndIntensityChange(Color targetColor, float intensityChange, CoroutineCancellationToken ct)
        {
            float time = 0f;
            Color currentColor = particleSystems[0].main.startColor.color;

            while (!ct.CancellationRequested && time < blendingTime)
            {
                yield return new WaitForEndOfFrame();
                time += Time.deltaTime;
                ApplyColorAndIntensityChange(Color.Lerp(currentColor, targetColor, time / blendingTime), intensityChange * Time.deltaTime / blendingTime);
            }
            cancelLightChange = null;
        }

        private void ApplyColorAndIntensityChange(Color color, float intensityChange)
        {
            foreach (ParticleSystem particleSystem in particleSystems)
            {
                ColorOverLifetimeModule module = particleSystem.colorOverLifetime;
                GradientAlphaKey[] alphas = module.color.gradient.alphaKeys;
                GradientColorKey[] colors = module.color.gradient.colorKeys;
                for (int i = 0; i < colors.Length; i++)
                {
                    colors[i].color = color;
                }
                for (int i = 0; i < alphas.Length; i++)
                {
                    alphas[i].alpha += intensityChange;
                }
                Gradient gradient = new Gradient();
                gradient.SetKeys(colors, alphas);
                module.color = new MinMaxGradient(gradient);
            }
        }

        public void TurnOff()
        {
            cancelLightChange?.Cancel();
            foreach (var ps in particleSystems)
            {
                ps.Clear();
                ps.Stop(true);
            }
        }

    }
}
