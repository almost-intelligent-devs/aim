﻿
using AIM.Interactables;
using UnityEngine;

namespace AIM.Controllables
{
    /// <summary>
    /// Base class for objects in the game world that 
    /// can be controlled e.g. via a button
    /// </summary>
    public abstract class Controllable: MonoBehaviour
    {
        public virtual void Init(bool activate)
        {
            if (activate) { Activate(null); }
            else { Deactivate(null); }
        }
        public abstract void Activate(Button button);
        public abstract void Deactivate(Button button);
    }

}
