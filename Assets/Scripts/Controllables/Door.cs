﻿using UnityEngine;
using AIM.Controllables;
using System.Collections;
using System.Collections.Generic;
using AIM.Interactables;
using AIM.Sound;
using System;

namespace AIM.Controllables
{
    [RequireComponent(typeof(Animator), typeof(Renderer))]
    public class Door : Controllable
    {
        private readonly int BOpen = Animator.StringToHash("Open");
        private readonly int EmissionId = Shader.PropertyToID("_EmissionColor");
        private readonly Color ColorOpen = new Color(0f, 2f, 0f);
        private readonly Color ColorClosed = new Color(2f, 0f, 0f);

        private Animator animator;
        private Material material;

        protected SoundsPlayer soundsPlayer;

        private void Awake()
        {
            animator = GetComponent<Animator>();
            material = GetComponent<Renderer>().material;

            soundsPlayer = GetComponentInChildren<SoundsPlayer>();
            if (soundsPlayer == null)
            {
                throw new InvalidOperationException($"{nameof(soundsPlayer)} is not specified!");
            }
        }

        public override void Activate(Button button)
        {
            animator.SetBool(BOpen, true);
            material.SetColor(EmissionId, ColorOpen);
            soundsPlayer.PlayOneShot(SharedSoundsMaster.Instance.Environment.GetClip(EnvironmentSounds.DoorOpened));
        }

        public override void Deactivate(Button button)
        {
            animator.SetBool(BOpen, false);
            material.SetColor(EmissionId, ColorClosed);
            soundsPlayer.PlayOneShot(SharedSoundsMaster.Instance.Environment.GetClip(EnvironmentSounds.DoorClosed));
        }
    }
}
