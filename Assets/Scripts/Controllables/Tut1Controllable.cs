﻿using UnityEngine;
using System.Collections;
using AIM.Interactables;
using AIM.Tutorial;

namespace AIM.Controllables
{

    public class Tut1Controllable : Controllable
    {
        [SerializeField]
        private Tutorial_Scene_1 scene;

        private bool isActive = false;

        public bool IsActive()
        {
            return isActive;
        }

        public override void Activate(Button button)
        {
            isActive = true;
            scene.NotifyChangeInButtonStates();
        }

        public override void Deactivate(Button button)
        {
            isActive = false;
            scene.NotifyChangeInButtonStates();
        }
    }

}
